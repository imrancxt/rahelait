
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link href="../css/login.css" rel="stylesheet">
        <script src="../js/jquery.js"></script>
        <script src="../js/login.js"></script>
    </head>
    <body>
        <div class="login">
            <form id='login_form' action="../login/check_admin" method='POST' enctype='multipart/form-data'> 
                <fieldset>
                    <legend> <h1><strong>Welcome </strong>Admin</h1></legend>
                    <p><input name="admin" type="text" required placeholder="Admin"></p>
                    <p><input name="password" type="password" required placeholder="Password"></p>
                    <p><input type="submit" value="Login"></p>
                    <p><a href="../login/user_login" style="color: green">Login as a user?</a></p>
                </fieldset>
            </form>
        </div>    
    </body>
</html>