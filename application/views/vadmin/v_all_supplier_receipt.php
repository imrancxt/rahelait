
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<? echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url() ?>admin/all_supplier_receipt">ALL SUPPLIER RECEIPT</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                All supplier Receipt
            </div>
            <div class="panel-body">
                <div class="table-responsive">
   
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>INVOICE NO</th>
                        <th>GET RECEIPT</th>
                        <th>DATE</th>
                        <th>SUPPLIER NAME</th>
                        <th>MOBILE NO</th>
                        <th>TOTAL DEBIT</th>
                        <th>TOTAL CREDIT</th>
                    </tr>
                </thead>
                <?php
                if (isset($invoice_no)) {
                    for ($i = 0; $i < count($invoice_no); $i++) {
                        $receipt = "$supplier_id[$i]-$invoice_no[$i]";
                        echo"<tr class='warning'><td>$invoice_no[$i]</td><td><a class='get_supplier_receipt' data-toggle='modal' data-target='#myModal2' serial='$receipt'>RECEIPT</a></td><td>$date[$i]</td><td>$supplier_name[$i]</td><td>$mobile_no[$i]</td><td>$dr[$i]</td><td>$cr[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">REPORTS</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive" id="receipt_content">
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" id="print_this">PRINT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".get_supplier_receipt").click(function(){
            serial=$(this).attr('serial');
            page="admin/get_supplier_invoice_data/"+serial;
            // alert(page);
            change_content("#receipt_content",page);
            
        }); 
        $("#print_this").click(function(){
            $("#receipt_content").print();
        });
    });
</script>
<style>
    .table-hover{
        width: 100%;
        border-spacing: 0;
        border-collapse: collapse;
        margin-bottom: 15px;

    } 
    .table-hover td{
        border: 1px #CCC solid;
        min-height: 25px;
        background-color:#faffe9;

    }
    .table-hover th{
        border: 1px #CCC solid;
        background-color:#f4f2f2;
    }
    .table-hover tr{
        height: 35px;
    }
    </style>