<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/addproducttype">ADD PRODUCT TYPE</a>
            </li>
        </ol>
    </div>
</div>

<br><br>
<div class="row">
    <h3 style="width:100%; text-align: center">ADD NEW PRODUCT CATEGORY</h3><hr>
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div id="add_new_product_type">

        </div>
        <form class='change_content_by_form' content="#add_new_product_type" action='<?php echo base_url(); ?>admin/add_new_product_type' method='POST' enctype='multipart/form-data'>

            <div class="table-responsive">

                <table class="table table-bordered table-hover">
                    <tr class="info">
                        <td>PRODUCT TYPE</td><td><input name="product_type" required class="form-control"/></td>
                    </tr>
                    <tr><td></td><td><button class="btn btn-primary" style="width:100%">SUBMIT</button></td></tr>
                </table>
            </div>
        </form>
    </div>
    <div class="col-lg-3"></div>
</div>