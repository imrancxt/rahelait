<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/sales_report">SALES REPORT</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Sales Report
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 form-inline">
        <div class="form-group">
            <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date1"/>
        </div>
        <div class="form-group">
            <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date2"/>
        </div>
        <div class="form-group">
            <button class="btn btn-primary" style="width: 99%" id="filter">FILTER</button>
        </div>
    </div>
                </div>
                <div class="row" id="sale_report_content">
                    <div class="col-lg-6">
                        <div class="table-responsive">
                            <br> <table class="table table-bordered table-hover" border="1" id="data_table">
                                <thead>
                                    <tr class="active">
                                        <th>PRODUCT TYPE</th>
                                        <th>TOTAL QUANTITY</th>
                                        <th>TOTAL AMOUNT</th>
                                    </tr>
                                </thead>
                                <?php
                                if (isset($o_product_type)) {
                                    for ($i = 0; $i < count($o_product_type); $i++) {
                                        echo"<tr class='warning'><td>$o_product_type[$i]</td><td>$o_quantity[$i]</td><td>$o_amount[$i]</td></tr>";
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" border="1" id="data_table">
                                <thead>
                                    <tr class="active">
                                        <th>SERIAL</th>
                                        <th>CUSTOMER NAME</th>
                                        <th>CUSTOMER TYPE</th>
                                        <th>AREA</th>
                                        <th>PRODUCT TYPE</th>
                                        <th>PRODUCT NAME</th>
                                        <th>QUANTITY</th>
                                        <th>AMOUNT</th>
                                        <th>DATE</th>
                                    </tr>
                                </thead>
                                <?php
                                if (isset($product_name)) {
                                    for ($i = 0; $i < count($product_name); $i++) {
                                        echo"<tr class='warning'>
                               <td>$i</td><td>$customer_name[$i]</td><td>$type[$i]</td><td>$area[$i]</td><td>$product_type[$i]</td>
                               <td>$product_name[$i]</td><td>$quantity[$i]</td><td>$total_amount[$i]</td><td>$sales_date[$i]</td>
                               </tr>";
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#filter").click(function(){
            date1=$("#date1").val();date2=$("#date2").val();
            page="admin/filter_sales_report/"+date1+"/"+date2;
            change_content("#sale_report_content",page);
        })
    })
</script>