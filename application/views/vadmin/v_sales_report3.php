<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/sales_report2">SALES REPORT2</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <input class="form-control" id="search" placeholder="TYPE TO SEARCH"/>
            <table class="table table-bordered table-hover" border="1" id="data_table">
                <thead>
                    <tr class="info">
                        <th>CUSTOMER NAME</th>
                        <th>PHONE NUMBER</th>
                        <th>SHOP NAME</th>
                        <th>PRODUCT TYPE</th>
                        <th>AMOUNT</th>
                    </tr>
                </thead> 
                <?php
                for($i=0;$i<count($customer_id);$i++){
                    echo"<tr class='warning'><td>$customer_name[$i]</td><td>$mobile_no[$i]</td><td>$shop_name[$i]</td><td>$product_type[$i]</td><td>$amount[$i]</td></tr>";
                }
                ?>
            </table>
        </div>
    </div>
</div>