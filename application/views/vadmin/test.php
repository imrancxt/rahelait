
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/add_product_name">ADD PRODUCT NAME</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <h3 style="width:100%; text-align: center">ADD NEW PRODUCT CATEGORY</h3><hr>
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div id="add_new_product_type">

        </div>
        <form class='change_content_by_form' content="#add_new_product_type" action='<?php echo base_url(); ?>admin/add_new_product_type' method='POST' enctype='multipart/form-data'>

            <div class="table-responsive">

                <table class="table table-bordered table-hover">
                    <tr class="info">
                        <td>PRODUCT TYPE</td><td><input name="product_type" required class="form-control"/></td>
                    </tr>
                    <tr class="info">
                        <td>SCALE</td><td><input name="scale" required class="form-control"/></td>
                    </tr>
                    <tr class="info">
                        <td>MEASUREMENT</td><td><input name="measurement" required class="form-control"/></td>
                    </tr>
                    <tr><td></td><td><button class="btn btn-primary" style="width:100%">SUBMIT</button></td></tr>
                </table>
            </div>
        </form>
    </div>
    <div class="col-lg-3"></div>
</div>

<div class="row">
    <div class="col-lg-12" id="get_product_name">

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h3 style="width:100%; text-align: center">PRODUCT CATEGORY LIST<?
if (isset($type_serial)) {
    $total_pc = count($type_serial);
    echo"(TOTAL:$total_pc)";
}
?></h3><hr>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="warning">
                        <th>SERIAL</th>
                        <th>PRODUCT TYPE</th>
                        <th>SCALE</th>
                        <th>MEASUREMENT</th>
                        <th>ADD NEW PRODUCT</th>
                        <th>EDIT</th>
                        <th>DELETE</th>
                        <th>VIEW</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($product_type)) {
                        for ($i = 0; $i < count($product_type); $i++) {
                            echo"<tr class='info' >";
                            echo"<td>$i</td><td>$product_type[$i]</td><td>$scale[$i]</td><td>$measurement[$i]</td><td><button serial='$type_serial[$i]' class='btn add_new_product_name'  data-toggle='modal' data-target='#myModal1'>ADD NEW</button></td>
                            <td><button class='btn product_type_edit' serial='$type_serial[$i]' data-toggle='modal' data-target='#myModal2'>EDIT</button></td>
                            <td><button class='btn delete' serial='$type_serial[$i]'>DELETE</button></td><td><button class='btn product_name_list' serial='$type_serial[$i]'>LIST</button></td>";
                            echo"</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="row">
    <form class='change_content_by_form' content="#message1" action='<?php echo base_url(); ?>admin/add_new_product_name' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">ADD NEW PRODUCT NAME</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr class="warning"><td>PRODUCT NAME</td><td><input required name="product_name" class=" form-control" placeholder="PRODUCT NAME"/></td></tr>
                                <tr class="warning">
                                    <td>BRAND</td>
                                    <td>
                                        <select class="form-control" name="product_brand">
                                            <?php
                                            include_once("common_function.php");
                                            $cf = new common_function();
                                            $cf->print_brand_list();
                                            ?>
                                        </select>
                                    </td>
                                </tr> 
                                <tr class="warning"><td>DATE</td><td><input name="date" class=" form-control" type="date" value="<? echo date('Y-m-d') ?>"/></td></tr>
                                <tr class="warning"><td>DESCRIPTION</td><td><textarea required name="product_description" class=" form-control" placeholder="DESCRIPTION"></textarea></td></tr>

                            </table>
                            <input type="hidden" name="type_serial" id="type_serial">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">ADD</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message1">

                    </div>
                </div>

            </div>
        </div>
    </form>

    <form class='change_content_by_form' content="#message2" action='<?php echo base_url(); ?>admin/update_product_type' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT PRODUCT CATEGORY</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="edit_product_type_table">

                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message2">

                    </div>
                </div>

            </div>
        </div>
    </form>


    <form class='change_content_by_form' content="#message3" action='<?php echo base_url(); ?>admin/update_product_name' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT PRODUCT NAME</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="edit_product_name_table">
                                
                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message3">

                    </div>
                </div>

            </div>
        </div>
    </form>

</div>
<script src="<?php echo $asset_url; ?>js/customize.js"></script>
<script>
    $(document).ready(function(){
        $(".add_new_product_name").click(function(){
            type_serial=$(this).attr("serial");
            $("#message1").html("");
            $("#type_serial").val(type_serial);
        });
        $(".product_type_edit").click(function(){
            $("#message2").html("");
            type_serial=$(this).attr("serial");
            page="admin/ajax_update_product_type/"+type_serial;
            change_content("#edit_product_type_table",page);
        });
        $(".product_name_list").click(function(){
            $("#get_product_name").slideUp('slow');
            type_serial=$(this).attr("serial");
            page="admin/ajax_get_product_name/"+type_serial;//"#edit_product_type_table"
            change_content("#get_product_name",page);
            $("#get_product_name").slideDown('slow');
        });
        $(document).on('click','.edit_product_name',function(){
            product_name_serial=$(this).attr("serial");
            page="admin/get_product_name_to_edit/"+product_name_serial;
            change_content("#edit_product_name_table",page);
        });
    })
</script>