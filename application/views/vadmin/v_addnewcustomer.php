<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/addnewcustomer">ADD NEW CUSTOMER</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Add New Customer
            </div>
            <div class="panel-body">
                <div id="message1">
            
        </div>
         <form class='change_content_by_form' content="#message1" action='<?php echo base_url(); ?>admin/add_new_customer' method='POST' enctype='multipart/form-data'>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr class="warning">
                        <td>
                            CUSTOMER TYPE
                        </td>
                        <td>
                            <select class="form-control" name="type">
                                <?php
                                if(isset($type)){
                                    foreach($type as $option){
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                           
                        </td>
                    </tr>
                    <tr class="warning">
                        <td>
                            AREA
                        </td>
                        <td>
                            <select class="form-control" name="area">
                                <?php
                                if(isset($area)){
                                    foreach($area as $option){
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                           
                        </td>
                    </tr>
                    <tr class="warning">
                        <td>
                            SHOP NAME
                        </td>
                        <td>
                            <textarea required class="form-control" name="shop_name">

                            </textarea>   
                        </td>
                    </tr>
                    <tr class="warning">
                        <td>
                            CUSTOMER NAME
                        </td>
                        <td>
                            <textarea required class="form-control" name="customer_name">

                            </textarea>   
                        </td>
                        
                    </tr>
                    <tr class="warning">
                        <td>
                            ADDRESS
                        </td>
                        <td>
                            <textarea required class="form-control" name="customer_address">

                            </textarea>   
                        </td>
                        
                    </tr>
                    <tr class="warning">
                        <td>
                            MOBILE NO
                        </td>
                        <td>
                            <input required class="form-control" name="mobile_no"/>  
                        </td>
                        
                    </tr>
                    <tr>
                        <td></td>
                        <td><button class="btn btn-danger">SUBMIT</button></td>
                    </tr>
                </table>
            </div>
         </form>
            </div>
        </div>
        
    </div>
</div>