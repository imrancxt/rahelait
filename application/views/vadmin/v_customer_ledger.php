

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="#">CUSTOMER LEDGER</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">REPORTS</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive" id="checking_content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class='col-lg-6'>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Add New Transaction
            </div>
            <div class="panel-body">
                <?php
                if ($_SESSION['type'] == "Admin" && isset($customer_id)) {
                    echo"
        <div id='message1'></div>
        <form class='change_content_by_form' content='#message1' action='<?php echo base_url(); ?>admin/post_customer_ledger_2' method='POST' enctype='multipart/form-data'>
            <div class='table-responsive'>
                <table class='table table-bordered table-hover'>
                    <tr class='warning'>
                        <td>DATE</td><td><input name='date' required type='date' class='form-control'/></td>
                    </tr>
                    <tr class='warning'>
                        <td>DESCRIPTION</td><td><textarea name='description'  class='form-control'></textarea></td>
                    </tr>
                    <tr class='warning'>
                        <td>DEBIT(-)</td><td><input required name='dr' class='form-control'/></td>
                    </tr>
                    <tr class='warning'>
                        <td>CREDIT(+)</td><td><input required name='cr' class='form-control'/></td>
                    </tr>
                    <tr>
                        <td>
    
                                <input name='customer_id' type='hidden' value='$customer_id'/>
                        </td>
                        <td> <button class='btn btn-danger' type='submit'  style='width: 100%;' >SUBMIT</button></td>
                    </tr>
                </table>
            </div>
        </form>";
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Customer Information
            </div>
            <div class="panel-body">
                <div class="table-responsive" style="height:205px">
                    <table class="table table-bordered table-hover">
                        <?php
                        if (isset($customer_name)) {
                            echo"<tr class='info'><td>CUSTOMER NAME</td><td>$customer_name[0]</td></tr>";
                            echo"<tr class='info'><td>PHONE NUMBER</td><td>$mobile_no[0]</td></tr>";
                            echo"<tr class='info'><td>ADDRESS</td><td>$address[0]</td></tr>";
                        }
                        ?>
                    </table>
                </div>
                <strong>DISCUSSION:</strong>
        <p class="text-info" id="disscussion"></p>
            </div>
        </div>

    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                All Transactions
            </div>
            <div class="panel-body">
                <div class="table-responsive">
           
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>INVOICE NO</th>
                        <th>RECEIPT</th>
                        <th>DESCRIPTION</th>
                        <th>DATE</th>
                        <th>DEBIT(-)</th>
                        <th>CREDIT(+)</th>
                        <th>BALANCE</th>
                    </tr>
                </thead>
                <?php
                $balance = 0;
                if (isset($invoice_no)) {
                    for ($i = 0; $i < count($invoice_no); $i++) {
                        $serial = "$customer_id-$invoice_no[$i]";
                        $balance = number_format($cr[$i], 2, '.', '') - number_format($dr[$i], 2, '.', '') + $balance;
                        $balance = number_format($balance, 2, '.', '');
                        $dr1 = number_format($dr[$i], 2, '.', '');
                        $cr1 = number_format($cr[$i], 2, '.', '');
                        $url = base_url() . "admin/get_customer_invoice_data/$customer_id-$invoice_no[$i]";

                        echo"<tr class='warning'><td>$invoice_no[$i]</td><td><a class='check_receipt' data-toggle='modal' data-target='#myModal2' serial='$serial'>GET</a></td><td>$description[$i]</td><td>$date[$i]</td><td>$dr1</td><td>$cr1</td><td>$balance</td></tr>";
                    }
                }
                if (isset($customer_ledger2_serial)) {
                    for ($i = 0; $i < count($customer_ledger2_serial); $i++) {
                        $balance = $cr2[$i] - $dr2[$i] + $balance;
                        $balance = number_format($balance, 2, '.', '');
                        echo"<tr class='warning'><td>INV</td><td></td><td>$description2[$i]</td><td>$date2[$i]</td><td>$dr2[$i]</td><td>$cr2[$i]</td><td>$balance</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>
<script>
    $(document).ready(function(){
        var balance=<?php echo "$balance" ?>;
        $("#disscussion").html(get_balance_report(balance));
        //alert(get_balance_report(balance));
        $("#filter").click(function(){
            serial=$("#product_name_id").val();
            date1=$("#date1").val();
            date2=$("#date2").val();
            page="admin/filter_product_ledger/"+serial+"/"+date1+"/"+date2;
            change_content("#data_table",page);
        });
        function get_balance_report(balance){
            if(balance<0){
                balance=-(balance);
                return "YOU WILL GET "+balance+" TAKA FROM THIS CUSTOMER";
            }
            else{
                if(balance>0){
                    return "THIS CUSTOMER WILL GET "+balance+" TAKA FROM YOU";
                }
                else{
                    return "NO HISTOEY";
                }
            }
        };
        $(".check_receipt").click(function(){
            serial=$(this).attr('serial');
            page="admin/get_customer_invoice_data/"+serial;
            change_content("#checking_content",page);
            
        });
    })
</script>
<style>
    .check_receipt{
        cursor: pointer;
    }
</style>