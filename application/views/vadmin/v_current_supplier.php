

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/currentsupplier">CURRENT SUPPLIER</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                CURRENT SUPPLIER
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <table class="table table-bordered table-hover" id="data_table">
                        <thead>
                            <tr class="info">
                                <th>SERIAL</th>
                                <th>IMAGE</th>
                                <th>NAME</th>
                                <th>SALES PERSON</th>
                                <th>ADDRESS</th>
                                <th>PHONE NUMBER</th>
                                <th>LEDGER ACCOUNT</th>
                                <th>EDIT</th>
                            </tr>
                        </thead>
                        <?php
                        if (isset($serial)) {
                            for ($i = 0; $i < count($serial); $i++) {
                                $img = "../img/noimage.jpg";
                                $ledger ="../admin/supplier_ledger_account?id=$serial[$i]";
                                $biodata ="../admin/supplier_id?id=$serial[$i]";
                                echo"<tr class='warning'><td>$i.</td><td><a href='$biodata'><img src='$img' class='img-circle' height='45px' width='45px'/></a></td><td><a href='$biodata'>$name[$i]</a></td><td>$sales_person[$i]</td><td>$address[$i]</td><td>
                       $phone[$i]</td><td><a href='$ledger'>ACCOUNT</a></td><td><button  class='update_supplier_btn' data-toggle='modal' data-target='#myModal2' serial='$serial[$i]'>EDIT</button></td></tr>";
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="row">
    <form class='change_content_by_form' content="#message2" action='../admin2/f_update_supplier_info' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT SUPPLIER INFO</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="edit_supplier_info">

                            </table>
                        </div>
                    </div>
                    <input type="hidden" name="serial" id="supplier_serial"/>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message2">

                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $(".update_supplier_btn").click(function(){
            serial=$(this).attr("serial");
            $("#supplier_serial").val(serial);
            page="admin/update_data_of_supplier_info/"+serial;
            change_content("#edit_supplier_info",page);
        })
    })
</script>