<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/return_product_list">ALL RETURNED PRODUCT</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                All Returned Product
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>PRODUCT TYPE</th>
                        <th>PRODUCT NAME</th>
                        <th>QUANTITY</th>
                        <th>SCALE</th>
                        <th>AMOUNT</th>
                        <th>DATE</th>
                    </tr>
                    </thead>
                    <?php
                    if(isset($product_name)){
                        for($i=0;$i<count($product_name);$i++){
                            echo"<tr class='warning'><td>$product_type[$i]</td><td>$product_name[$i]</td><td>$quantity[$i]</td><td>$scale[$i]</td><td>$total_amount[$i]</td><td>$date[$i]</td></tr>";
                        }
                    }
                    ?>
                
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>