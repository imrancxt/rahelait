<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../account/add_head_data">ADD ACCOUNT HEAD DATA</a>
            </li>
        </ol>
    </div>
</div>
    <div class="row">
        <div class="col-lg-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Others Account
                </div>
                <div class="panel-body">
                     <form style="border: none"  class='change_content_by_alert' action='../account/others_account_ledger' method='POST' enctype='multipart/form-data'>
            <div class="form-group">
                Date:<input class="form-control" name="date" type="date" value="<?php echo date("Y-m-d") ?>" required/>
            </div>
            <div class="form-group">
                Account Head
                <select class="form-control" id="supplementary_name" name="acc_head">
                    <?php
                    $supllementary_name=$acc_head;
                    foreach ($supllementary_name as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                Account Sub-Head
                <select required class="form-control" name="acc_subhead">
                    <?php
                    if(isset($acc_subhead)){
                        foreach($acc_subhead as $option){
                            echo"<option>$option</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                Transaction Type:
                <select class="form-control" name="drcr">
                    <?php
                    $supllementary_type = array('dr', 'cr');
                    foreach ($supllementary_type as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                Amount
                <input type="number" class="form-control" name="amount" placeholder="Amount" value="0"/>
            </div>
       
            <div class="form-group">
                Comment
                <input class="form-control" name="comment" placeholder="Comment"/>
            </div>
            <div class="form-group">
                
                <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#alert_modal">Submit</button>
            </div>
        </form>
        
                </div>
            </div>
        </div>
       
    </div>

<script>
    $(document).ready(function(){
        $("#check_supplementary_account").click(function(){
            supplementary_name=$("#supplementary_name").val();
            page="../member/get_account_info/"+supplementary_name;
            change_content("#supplementary_account",page);
        })
        $("#supplementary_name").on('change',function(){
            val=$(this).val();
            page="account/get_subhead?head="+val;
            change_content("select[name='acc_subhead']",page);
        })
    })
</script>