<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/measuring_scale">MEASURING SCALE</a>
            </li>
        </ol>
    </div>
</div>
<div class="row"><div class="col-lg-2"></div>
    <div class="col-lg-8" id="message1"></div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                ADD NEW MEASUREMENT SCALE
            </div>
            <div class="panel-body">
                <form class='change_content_by_form' content="#message1" action='../admin/add_measuring_scale' method='POST' enctype='multipart/form-data'>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr class='warning'><td>SCALE NAME</td><td><input required class='form-control' name='scale'/></td></tr>
                            <tr class='warning'><td>MEASUREMENT</td><td><input required class='form-control' name='measurement'/></td></tr>
                            <tr><td></td><td><button class='btn btn-warning'>ADD </button></td></tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                MEASUREMENT SCALE LIST
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
                    <table class="table table-bordered table-hover" id="data_table">
                        <thead>
                            <tr class="success">
                                <th>
                                    SERIAL 
                                </th>
                                <th>
                                    SCALE NAME
                                </th>
                                <th>
                                    MEASUREMENT
                                </th>
                                <th>UPDATE</th>
                            </tr>
                        </thead>
                        <?php
                        if (isset($serial)) {
                            for ($i = 0; $i < count($serial); $i++) {
                                echo"<tr class='warning'><td>$i</td><td>$scale[$i]</td><td>$measurement[$i]</td><td><button  class='btn btn-default update_measuring_scale' serial='$serial[$i]' data-toggle='modal' data-target='#myModal2'>UPDATE</button></td></tr>";
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="row">
    <form class='change_content_by_form' content="#message2" action='../admin/f_update_measuring_scale' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT MEASUREMENT SCALE</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="edit_measuring_scale_table">

                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message2">

                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
<script src="../js/customize.js"></script>
<script>
    $(document).ready(function(){
        var $rows = $('#data_table .warning');
        $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        }); 
        $(".update_measuring_scale").click(function(){
            serial=$(this).attr("serial");
            page="admin/update_measuring_scale/"+serial;
            change_content("#edit_measuring_scale_table",page);
        })
    });
</script>
<style>
    .update_measuring_scale{
        background-color:wheat;
        border: 1px wheat;
    }
</style>