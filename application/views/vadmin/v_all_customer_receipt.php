
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url() ?>admin/all_customer_receipt">ALL CUSTOMER RECEIPT</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                All customer Invoice
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>INVOICE NO</th>
                        <th>GET RECEIPT</th>
                        <th>DATE</th>
                        <th>CUSTOMER NAME</th>
                        <th>MOBILE NO</th>
                        <th>TOTAL DEBIT</th>
                        <th>TOTAL CREDIT</th>
                    </tr>
                </thead>
                <?php
                if (isset($invoice_no)) {
                    for ($i = 0; $i < count($invoice_no); $i++) {
                        $receipt = "$customer_id[$i]-$invoice_no[$i]";
                        echo"<tr class='warning'><td>$invoice_no[$i]</td><td><a class='get_customer_receipt' data-toggle='modal' data-target='#myModal2' serial='$receipt'>RECEIPT</a></td><td>$date[$i]</td><td>$customer_name[$i]</td><td>$mobile_no[$i]</td><td>$dr[$i]</td><td>$cr[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">REPORTS</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive" id="receipt_content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".get_customer_receipt").click(function(){
            serial=$(this).attr('serial');
            page="admin/get_customer_invoice_data/"+serial;
           // alert(page);
            change_content("#receipt_content",page);
            
        }); 
    });
</script>