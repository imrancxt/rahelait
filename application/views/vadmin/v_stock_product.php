<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/product_in_stock">STOCK PRODUCT</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Filter Stock
            </div>
            <div class="panel-body form-inline">
                <div class="form-group">
        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date1"/>
    </div>
    <div class="form-group">
        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date2"/>
    </div>
    <div class="form-group">
        <button class="btn btn-primary" style="width: 99%" id="filter">FILTER</button>
    </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row" id="stock_report_content">
    <div class="col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Product Available
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <br> <table class="table table-bordered table-hover" border="1" id="data_table">
                <thead>
                    <tr class="active">
                        <th>PRODUCT TYPE</th>
                        <th>TOTAL QUANTITY</th>
                        
                    </tr>
                </thead>
                <?php
                if(isset($o_product_type)){
                    for($i=0;$i<count($o_product_type);$i++){
                        echo"<tr class='warning'><td>$o_product_type[$i]</td><td>$o_quantity[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
    <div class="col-lg-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Available Product Quantity
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>SERIAL</th>
                        <th>IMAGE</th>
                        <th>PRODUCT TYPE</th>
                        <th>PRODUCT NAME</th>
                      
                        <th>AVAILABLE QUANTITY</th>
                        
                    </tr>
                </thead>  
                <?php
                if(isset($product_name)){
                    for($i=0;$i<count($product_name);$i++){
                        $img="../img/noimage.jpg";
                        echo"<tr class='warning'><td>$i</td><td><a href='#'><img src='$img' class='img-circle' height='45px' width='45px'/></a></td>
                        <td>$product_type[$i]</td><td>$product_name[$i]</td><td>$quantity[$i]</td>
                        </tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
       
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#filter").click(function(){
            date1=$("#date1").val();date2=$("#date2").val();
            page="admin/filter_product_in_stock/"+date1+"/"+date2;
            change_content("#stock_report_content",page);
        });
        $(document).on('keyup','#search',function(){
            
        })
    })
</script>