
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<? echo base_url() ?>admin/currentcustomer">CURRENT CUSTOMER</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                All Current Customer
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
            
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>SERIAL</th>
                        <th>CUSTOMER TYPE</th>
                        <th>AREA</th>
                        <th>SHOP NAME</th>
                        <th>CUSTOMER NAME</th>
                        <th>ADDRESS</th>
                        <th>LEDGER ACCOUNT</th>
                        <th>EDIT</th>
                        <th>SALE PRODUCT</th>
                    </tr>
                </thead>
                <?php
                if (isset($serial)) {
                    for ($i = 0; $i < count($serial); $i++) {
                        $url= "../admin/sale_product_to_customer?id=$serial[$i]";
                        $pro_url= "../admin/customer_product?id=$serial[$i]";
                        $acc_url="../admin/customer_ledger_account?id=$serial[$i]";
                        echo"<tr class='warning'><td>$i</td><td>$type[$i]</td><td><pre>$area[$i]</pre></td><td>$shop_name[$i]</td><td><a href='$pro_url'>$customer_name[$i]</a></td>
                        <td><pre>$address[$i]</pre></td><td><a href='$acc_url'>ACCOUNT</a></td><td><button class='update_customer_btn' data-toggle='modal' data-target='#myModal2' serial='$serial[$i]'> EDIT</button></td>
                        <td><a href='$url'>SALE</a></td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
       
    </div>
</div>


<div class="row">
    <form class='change_content_by_form' content="#message2" action='<?php echo base_url(); ?>admin/f_update_customer_info' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT CUSTOMER INFO</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="edit_customer_info">
                               
                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message2">

                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $(".update_customer_btn").click(function(){
            serial=$(this).attr('serial');
            page="admin/get_customer_info/"+serial;
            change_content("#edit_customer_info",page);
        })
    })
</script>