  <div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="#">SUPPLIER DETAILS</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
  
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Basic Information
            </div>
            <div class="panel-body">
                <form class='change_content_by_form' content="#message1" action='<?php echo base_url(); ?>admin/post_supplier_ledger_2' method='POST' enctype='multipart/form-data'>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <?php
                    if (isset($name)) {
                        echo"<tr class='warning'><td>NAME</td><td>$name[0]</td></tr>";
                        echo"<tr class='warning'><td>ADDRESS</td><td>$address[0]</td></tr>";
                        echo"<tr class='warning'><td>PNONE NUMBER</td><td>$phone[0]</td></tr>";
                        echo"<tr class='warning'><td>SALES PERSON</td><td>$sales_person[0]</td></tr>";
                    }
                    ?>
                </table>
            </div>
        </form>
            </div>
        </div>
        
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Supplied Product
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>SERIAL</th>
                        <th>PRODUCT NAME</th>
                        <th>PRODUCT TYPE</th>
                        <th>QUANTITY</th>
                        <th>PURCHASE PRICE</th>
                        <th>TOTAL</th>
                        <th>DATE</th>
                    </tr>
                </thead>
                <?php
                if (isset($invoice_no)) {
                    for ($i = 0; $i < count($invoice_no); $i++) {
                        echo"<tr class='warning'><td>$i</td><td>$product_name[$i]</td><td>$product_type[$i]</td><td>$quantity[$i]</td><td>$purchase_price[$i]</td><td>$amount[$i]</td><td>$insertion_date[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>