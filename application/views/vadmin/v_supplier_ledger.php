
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="#">SUPPLIER LEDGER</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
<div class="col-lg-6">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            Add Transaction
        </div>
        <div class="panel-body">
            <div id="message1"></div>
    <form class='change_content_by_form' content="#message1" action='<?php echo base_url(); ?>admin/post_supplier_ledger_2' method='POST' enctype='multipart/form-data'>
        <div class="table-responsive">
            <table class="table table-bordered table-hover" >
                <tr class="warning">
                    <td>DATE</td><td><input name="date" required type="date" class="form-control"/></td>
                </tr>
                <tr class="warning">
                    <td>DESCRIPTION</td><td><textarea name="description"  class="form-control"></textarea></td>
                </tr>
                <tr class="warning">
                    <td>DEBIT(-)</td><td><input required name="dr" class="form-control"/></td>
                </tr>
                <tr class="warning">
                    <td>CREDIT(+)</td><td><input required name="cr" class="form-control"/></td>
                </tr>
                <tr>
                    <td>
                        <?php
                        if (isset($supplier_id)) {
                            echo"<input id='supplier_id' name='supplier_id' type='hidden' value='$supplier_id'/>";
                        }
                        ?>
                    </td>
                    <td> <button class="btn btn-danger" type="submit"  style="width: 100%;" >SUBMIT</button></td>
                </tr>
            </table>

        </div>
    </form>
        </div>
    </div>
    
</div>
<div class="col-lg-6">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            Basic Information
        </div>
        <div class="panel-body">
                <div class="table-responsive" style="min-height:200px">
        <table class="table table-bordered table-hover" >
            <?php
            if (isset($name)) {
                echo"<tr class='info'><td>NAME</td><td>$name[0]</td></tr>";
                echo"<tr class='info'><td>PHONE NUMBER</td><td>$phone[0]</td></tr>";
                echo"<tr class='info'><td>ADDRESS</td><td>$address[0]</td></tr>";
            }
            ?>
        </table>
    </div>
        <strong>DISCUSSION:</strong>
        <p class="text-info" id="disscussion"></p>
        </div>
    </div>

</div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Transaction History
            </div>
            <div class="panel-body">
                <div class='form-inline'>Date (From): <input class='form-control' type='date' id='date1'/>
               Date (To): <input class='form-control' type='date' id='date2'/> <button class='btn btn-info' id='filter'>Filter</button></div>
                <br>
                <div class="table-responsive" id="ledger_content">
                    <?php
                    $this->load->view("ajax_code/v_ajax_supplier_ledger");
                    ?>
        </div>
            </div>
        </div>
        
    </div>
</div>
<div class="row">
    <div class="modal fade bs-example-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div id="print_content">

                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" id="print_this">PRINT</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var balance=<?php echo "$balance" ?>;
        $("#disscussion").html(get_balance_report(balance));
        function get_balance_report(balance){
            if(balance<0){
                balance=-(balance);
                return "YOU WILL GET "+balance+" TAKA FROM THIS SUPPLIER";
            }
            else{
                if(balance>0){
                    return "THIS SUPPLIER WILL GET "+balance+" TAKA FROM YOU";
                }
                else{
                    return "NO HISTOEY";
                }
            }
        };
        $(document).on("click",".invoice_data",function(){
            serial=$(this).attr('serial');
            page="admin/get_supplier_invoice_data/"+serial;
            change_content("#print_content",page);
        })

        $(document).on('click','#print_this',function(){
            $("#print_content").print(); 
        });

        $(document).on('click','#filter',function(){

            var date1=$("#date1").val();var date2=$("#date2").val();
            var supplier_id=$("#supplier_id").val();
            page="admin/filter_supplier_trans_history?date1="+date1+"&date2="+date2+"&supplier_id="+supplier_id;
            change_content("#ledger_content",page);
        })


    })
</script>
<style>
    .table-hover{
        width: 100%;
        border-spacing: 0;
        border-collapse: collapse;
        margin-bottom: 15px;

    } 
    .table-hover td{
        border: 1px #CCC solid;
        min-height: 25px;
        background-color:#faffe9;

    }
    .table-hover th{
        border: 1px #CCC solid;
        background-color:#f4f2f2;
    }
    .table-hover tr{
        height: 35px;
    }
</style>