
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url() ?>admin/area">AREA</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
      <div class="col-lg-4">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            Add Area
        </div>
        <div class="panel-body">
          
                <div id="message1">

                </div>
                <form class='change_content_by_form' content="#message1"  action='<?php echo base_url() ?>admin/add_customer_area' method='POST' enctype='multipart/form-data'>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr class="info">
                                <td>AREA</td>
                                <td><textarea required class="form-control" name="customer_area"></textarea></td>
                            </tr>
                            <tr>
                                <td></td><td><button class="btn btn-danger">ADD</button></td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                All Customer Area
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>SERIAL</th>
                        <th>CUSTOMER AREA</th>
                        <th>EDIT</th>
                    </tr>
                </thead>
                <?php
                if (isset($serial)) {
                    for ($i = 0; $i < count($serial); $i++) {
                        echo"<tr class='warning'><td>$i</td><td>$area[$i]</td><td><button class='update_area_btn' serial='$serial[$i]' data-toggle='modal' data-target='#myModal2'>EDIT</button></td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
       
    </div>
</div>


<div class="row">
    <form class='change_content_by_form' content="#message2" action='../admin/f_update_area' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT AREA</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr class="warning">
                                    <td>AREA</td>
                                    <td id="edit_customer_area">

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message2">

                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $(".update_area_btn").click(function(){
            serial=$(this).attr('serial');
            page="admin/get_specific_area/"+serial;
            change_content("#edit_customer_area",page);
        })
    })
</script>
<style>
    .update_area_btn{
        background-color:#9999ff;
        border: 1px #9999ff solid;
        border-radius: 2px;
        color: white;
        height: 30px;
    }
</style>