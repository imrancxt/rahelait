<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/branch_wise_stock">BRANCH WISE STOCK</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">

    <div class="col-lg-4">
        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date1"/>
    </div>
    <div class="col-lg-4">
        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date2"/>
    </div>
    <div class="col-lg-4">
        <button class="btn btn-primary" style="width: 99%" id="filter">FILTER</button>
    </div>
</div>
<div class="row" id="stock_report_content">
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Total Product Quantity
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" border="1" id="data_table">
                        <thead>
                            <tr class="active">
                                <th>PRODUCT TYPE</th>
                                <th>TOTAL QUANTITY</th>
                                <th>TOTAL AMOUNT</th>
                            </tr>
                        </thead>
                        <?php
                        if (isset($o_product_type)) {
                            for ($i = 0; $i < count($o_product_type); $i++) {
                                echo"<tr class='warning'><td>$o_product_type[$i]</td><td>$o_quantity[$i]</td><td>$o_amount[$i]</td></tr>";
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Branch Product Amount
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
            <table class="table table-bordered table-hover" border="1" id="data_table">
                <thead>
                    <tr class="active">
                        <th>BRANCH NAME</th>
                        <th>TOTAL AMOUNT</th>
                    </tr>
                </thead>
                <?php
                if (isset($o2_branch_id)) {
                    for ($i = 0; $i < count($o2_branch_id); $i++) {
                        echo"<tr class='warning'><td>$o2_branch_name[$i]</td><td>$o2_total_amount[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
       
    </div>
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Branch-product-quantity-amount
            </div>
            <div class="panel-body">
                <div class="table-responsive" style="min-height: 410px">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="active">
                        <th>BRANCH NAME</th>
                        <th>PRODUCT TYPE</th>
                        <th>TOTAL QUANTITY</th>
                        <th>TOTAL AMOUNT</th>
                    </tr>
                </thead>
                <?php
                if (isset($o1_branch_id)) {
                    for ($i = 0; $i < count($o1_branch_id); $i++) {
                        echo"<tr class='warning'><td>$o1_branch_name[$i]</td><td>$o1_product_type[$i]</td><td>$o1_total_quantity[$i]</td><td>$o1_total_amount[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Available Product
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="active">
                        <th>
                            PRODUCT NAME
                        </th>
                        <th>PRODUCT TYPE</th>
                        <th>BRANCH</th>
                        <th>
                            AVAILABLE QUANTITY
                        </th>
                        <th>
                            SCALE
                        </th>
                        <th>
                            PURCHASE PRICE
                        </th>
                        <th>
                            AMOUNT
                        </th>

                    </tr>
                </thead>
                <?php
                if (isset($serial)) {
                    for ($i = 0; $i < count($serial); $i++) {
                        echo"<tr class='warning'><td>$product_name[$i]</td><td>$product_type[$i]</td><td>$branch[$i]</td>
                        <td>$quantity[$i]</td><td>$scale[$i]</td><td>$purchase_price[$i]</td><td>$amount[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#filter").click(function(){
            date1=$("#date1").val();date2=$("#date2").val();
            page="admin/filter_branch_wise_stock/"+date1+"/"+date2;
            change_content("#stock_report_content",page);
        });
    })
</script>