
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="#">STOCK HISTORY</a>
            </li>
        </ol>
    </div>
</div>






<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                STOCK PRODUCT HISTORY of <strong><?php echo $product_type; ?></strong>
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="success">
                        <th>
                            SERIAL 
                        </th>
                        <th>
                            DATE
                        </th>
                        <th>
                            INVOICE NO
                        </th>
                       

                        <th>RECEIPTS</th>
                        <TH>QUANTITY SOLD</TH>
                        <th>BALANCE</th>
                        <th>REMARKS</th>

                    </tr>
                </thead>
                <?php
                  if(isset($date)){
                    $serial=1;
                    $balance=0;
                    for($i=0;$i<count($date);$i++){
                        $balance=$stock[$i]-$sold[$i]+$balance;
                        echo"<tr class='warning'><td>$serial</td><td>{$date[$i]}</td><td>{$invoice_no[$i]}</td><td>{$stock[$i]}</td>
                        <td>{$sold[$i]}</td><td>$balance</td><td>-</td>
                        </tr>";
                        $serial++;
                    }
                  }
                ?>
            </table>
        </div>
            </div>
        </div>
       
    </div>
</div>