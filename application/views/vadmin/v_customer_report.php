<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/customer_report">CUSTOMER REPORT</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-3"><input class="form-control" placeholder="SEARCH" id="search"/></div>
    <div class="col-lg-9"><strong>(-)BALANCE MEANS YOU GET TAKA FROM THIS PERSON AND (+)BALANCE MEANS THIS PERSON GET TAKA FROM YOU</strong></div>
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="active">
                        <th>SERIAL</th>
                        <th>CUSTOMER NAME</th>
                        <th>CUSTOMER TYPE</th>
                        <th>AREA</th>
                        <th>DEBIT</th>
                        <th>CREDIT</th>
                        <th>BALANCE</th>
                    </tr>
                </thead>
                <?php
                if(isset($customer_name)){
                    for($i=0;$i<count($customer_name);$i++){
                        echo"<tr class='warning'><td>$i</td><td>$customer_name[$i]</td><td>$type[$i]</td><td><pre>$area[$i]</pre></td><td>$dr[$i]</td><td>$cr[$i]</td><td>$balance[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
    </div>
</div>