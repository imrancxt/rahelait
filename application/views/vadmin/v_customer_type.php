
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url() ?>admin/customer_type">CUSTOMER TYPE</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Add Customer Type
            </div>
            <div class="panel-body">
                <div id="message1">
                </div>
                <form class='change_content_by_form' content="#message1"  action='<?php echo base_url() ?>admin/add_customer_type' method='POST' enctype='multipart/form-data'>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr class="info">
                                <td>CUSTOMER TYPE</td>
                                <td><input required class="form-control" name="customer_type_post"/></td>
                            </tr>
                            <tr>
                                <td></td><td><button class="btn btn-danger" type="submit">ADD</button></td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                All Customer Type
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="info">
                        <th>SERIAL</th>
                        <th>CUSTOMER TYPE</th>
                        <th>EDIT</th>
                    </tr>
                </thead>
                <?php
                if (isset($serial)) {
                    for ($i = 0; $i < count($serial); $i++) {
                        echo"<tr class='warning'><td>$i</td><td>$customer_type[$i]</td><td><a href='#'>EDIT</a></td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>