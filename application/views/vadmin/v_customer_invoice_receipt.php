
     <div class="col-lg-12">
        <a class="btn btn-primary" id="print_this" style="width: 100%">PRINT MAIN RECEIPT</a>
    </div>
           
    <div>
        <div class="col-lg-12" id="print_content">
            <div style="border: 1px #000 solid; padding:5px">
            <h3 style="text-align: center">MAIN RECEIPT OF INVOICE NO # <?php echo $invoice_no ?></h3><hr>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <?php
                    if (isset($customer_name)) {
                        echo"<tr class='info'><td>CUSTOMER NAME</td><td>$customer_name[0]</td></tr>";
                        echo"<tr class='info'><td>ADDRESS</td><td>$address[0]</td></tr>";
                        echo"<tr class='info'><td>PHONE NO</td><td>$mobile_no[0]</td></tr>";
                    }
                    ?>
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr class="active">
                            <th>SERIAL</th>
                            <th>PRODUCT NAME</th>
                            <th>QUANTITY</th>
                            <th>SALES PRICE</th>
                            <th>AMOUNT</th>
                        </tr>
                    </thead>
                    <?php
                    if (isset($product_name)) {
                        $total_product_price=0;
                        for ($i = 0; $i < count($product_name); $i++) {
                            $ammount = number_format($amount[$i], 2,".","");
                             $total_product_price+=$ammount;
                            echo"<tr class='warning'><td>$i</td><td>$product_name[$i]</td><td>$quantity[$i]</td><td>$sales_price[$i]</td><td>$ammount TAKA</td></tr>";
                        }
                    }
                    ?>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <?php
                    $total = 0.00;
                    if (isset($description)) {
                        for ($i = 0; $i < count($description); $i++) {
                            $amount = number_format($total_amount[$i], 2,".","");
                            if ($description[$i] == "SOLD PRODUCT" || $description[$i] == "LABER BILL") {
                                echo"<tr class='success'><td>$description[$i]</td><td>+$amount TAKA</td></tr>";
                                $total = $total + $total_amount[$i];
                            } else {
                                if ($description[$i] == "DISSCOUNT") {
                                    echo"<tr class='success'><td>$description[$i]</td><td>-$amount TAKA</td></tr>";
                                    $total = $total - $total_amount[$i];
                                }
                                else{
                                    if($description[$i]=="PAID"){
                                        $paid=$total_amount[$i];
                                    }
                                }
                            }
                        }
                        $total=  number_format($total,2,".","");
                         echo"<tr class=''><td>TOTAL</td><td>$total TAKA</td></tr>";
                         $paid=  number_format($paid,2,".","");
                         echo"<tr class='primary'><td>PAID</td><td>-$paid TAKA</td></tr>";
                         $due=$total-$paid;
                         $due=number_format($due, 2,".","");
                         echo"<tr class='success'><td>DUE</td><td>$due TAKA</td></tr>";
                    }
                    ?>  
                </table>
            </div>
            </div>
        </div><br>
         <div class="col-lg-12"><a style="width:100%" id="print_do_slip" class="btn btn-primary">PRINT DO SLIP</a></div>
        <div class="col-lg-12" id="print_do">
            <div style="border: 1px #000 solid; padding:5px">
            <h3 style="text-align: center">DO SLIP OF INVOICE NO # <?php echo $invoice_no ?></h3><hr>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <?php
                    if (isset($customer_name)) {
                        echo"<tr class='info'><td>CUSTOMER NAME</td><td>$customer_name[0]</td></tr>";
                        echo"<tr class='info'><td>ADDRESS</td><td>$address[0]</td></tr>";
                        echo"<tr class='info'><td>PHONE NO</td><td>$mobile_no[0]</td></tr>";
                    }
                    ?>
                </table>
            </div>
             <div class="table-responsive">
                <table class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr class="active">
                            <th>PRODUCT NAME</th>
                            <th>QUANTITY</th>
                            
                        </tr>
                    </thead>
                    <?php
                    if (isset($product_name)) {
                        for ($i = 0; $i < count($product_name); $i++) {
                            echo"<tr class='warning'><td>$product_name[$i]</td><td>$quantity[$i]</td></tr>";
                        }
                        echo"<tr class='success'><td>TOTAL AMOUNT</td><td>$total_product_price</td></tr>";
                    }
                    ?>
                </table>
            </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        $("#print_this").click(function(){
            $("#print_content").print();
        });
         $("#print_do_slip").click(function(){
            $("#print_do").print();
        })
    })
</script>
<style>
    .table-hover{
        width: 100%;
        border-spacing: 0;
        border-collapse: collapse;
        margin-bottom: 15px;

    } 
    .table-hover td{
        border: 1px #CCC solid;
        min-height: 25px;
        background-color:#faffe9;

    }
    .table-hover th{
        border: 1px #CCC solid;
        background-color:#f4f2f2;
    }
    .table-hover tr{
        height: 35px;
    }
    .exam_term_btn{
        margin: 5px;
        background-color:#77d4cd;
        color: white;
    }
</style>
