<?php
include_once("common_function.php");
$cf = new common_function();
?>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/addproductname">ADD NEW PRODUCT NAME</a>
            </li>
        </ol>
    </div>
</div>
<h3 style="width:100%; text-align: center">ADD NEW PRODUCT NAME</h3><hr>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div id="message1"></div>
        <form class='change_content_by_form' content="#message1" action='<?php echo base_url(); ?>admin/p_addnewproductname' method='POST' enctype='multipart/form-data'>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr class="warning">
                        <td>PRODUCT TYPE</td>
                        <td><select class="form-control" name="product_type"><?php
if (isset($product_type)) {
    foreach ($product_type as $type) {
        echo"<option>$type</option>";
    }
}
?></select></td>

                    </tr>
                    <tr class="warning"><td>PRODUCT NAME</td><td><input required name="product_name" class=" form-control" placeholder="PRODUCT NAME"/></td></tr>

                    <tr class="warning">
                        <td>BRAND</td>
                        <td>
                            <select class="form-control" name="brand">
                                <?php
                                $cf->print_brand_list();
                                ?>
                            </select>
                        </td>
                    </tr> 
                    <tr class="warning"><td>DATE</td><td><input name="date" class=" form-control" type="date" value="<?php echo date('Y-m-d') ?>"/></td></tr>
                    <tr class="warning"><td>DESCRIPTION</td><td><textarea required name="description" class=" form-control" placeholder="DESCRIPTION"></textarea></td></tr>
                    <tr><td></td><td><button class="btn btn-danger" style="width: 100%">ADD</button></td></tr>
                </table>
            </div>
        </form>
    </div>
</div>