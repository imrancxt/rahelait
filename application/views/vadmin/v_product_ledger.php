<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="#">PRODUCT LEDGER</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Product Info
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <?php
                        if (isset($pn_serial)) {
                            echo"<tr class='info'><td>PRODUCT NAME</td><td>$pn_product_name[0]</td></tr>";
                            echo"<tr class='info'><td>PRODUCT BRAND</td><td>$pn_brand[0]</td></tr>";
                            echo"<tr class='info'><td>SCALE</td><td>$pn_scale[0]</td></tr>";
                            echo"<tr class='info'><td>MEASUREMENT</td><td>$pn_measurement[0]</td></tr>";
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-12"><h2 style="text-align: center"></h2><hr></div>

</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                LEDGER ACCOUNT
            </div>
            <div class="panel-body">
                <div class="form-inline">
                    <div class="form-group"><strong>DISSCUSSION:</strong><p class="text-info" id="disscussion"></p></div>
                    <div class="form-group">
                        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date1"/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date2"/>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" style="width: 99%" id="filter">FILTER</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="data_table">
                        <thead>
                            <tr class="active">
                                <th>SERIAL</th>
                                <th>DESCRIPTION</th>
                                <th>DATE</th>
                                <th>DEBIT</th>
                                <th>CREDIT</th>
                                <th>BALANCE</th>
                            </tr>
                        </thead>
                        <?php
                        $balance = 0;
                        if (isset($date)) {
                            for ($i = 0; $i < count($date); $i++) {
                                $balance = $cr[$i] - $dr[$i] + $balance;
                                echo"<tr class='warning'><td>$i</td><td>$description[$i]</td><td>$date[$i]</td><td>$dr[$i]</td><td>$cr[$i]</td><td>$balance</td></tr>";
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <?php
    if (isset($product_name_id)) {
        echo"<input id='product_name_id' value='$product_name_id' type='hidden'/>";
    }
    ?>
</div>
<script>
    $(document).ready(function(){
        var balance=<?php echo "$balance" ?>;
        $("#disscussion").html(get_balance_report(balance));
        //alert(get_balance_report(balance));
        $("#filter").click(function(){
            serial=$("#product_name_id").val();
            date1=$("#date1").val();
            date2=$("#date2").val();
            page="admin/filter_product_ledger/"+serial+"/"+date1+"/"+date2;
            change_content("#data_table",page);
        });
        function get_balance_report(balance){
            if(balance<0){
                balance=-(balance);
                return "THIS PRODUCT IS STILL IN LOSS STATE AND TOTAL LOSS FROM THIS PRODUCT IS:"+balance+" TAKA";
            }
            else{
                if(balance>0){
                    return "THIS PRODUCT IS NOW PROFIT STATE AND TOTAL PROFIT FROM THIS PRODUCT IS:"+balance+" TAKA";
                }
                else{
                    return "THIS PRODUCT IS NOT IN STOCK";
                }
            }
        }
    })
</script>