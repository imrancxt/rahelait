</div>

</div>
</div>
</div>
<script src="<?php echo $asset_url ?>js/fileinput2.js"></script>
<script>
    $(document).ready(function(){
        $('.toggle-one').bootstrapToggle();
        $(document).on("click","#mark_unmark",function(){
            status=$(this).html();
            if(status=="MARK ALL"){
                $('.toggle-one').bootstrapToggle('on');
                $(this).html('UNMARK ALL');
                $(this).css({"background-color":"#ec971f"});
            }
            else{
                $('.toggle-one').bootstrapToggle('off');
                $(this).html('MARK ALL');
                $(this).css({"background-color":"#5cb85c"});
            }
        });
        /*var $rows = $('#data_table .warning');
        $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });*/
        $(document).on('keyup','#search',function(){
            var $rows = $('#data_table .warning');
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });
        
    })
</script>
</body>

</html>
