<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/add_new_product_to_stock">ADD PRODUCT TO STOCK</a>
            </li>
        </ol>
    </div>
</div>



<div class="row"> 
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Add Product To Stock
            </div>
            <div class="panel-body">
                 <form class='alert_ajax_form' action='../admin/readynewproducttostock' method='POST' enctype='multipart/form-data'>
      
        <div class="col-lg-6">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr class="info">
                        <td>BRANCH NAME</td>
                        <td>
                            <select class="form-control" name="branch_name">
                                <?php
                                if (isset($branch_name)) {
                                    foreach ($branch_name as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>    
                        </td>
                    </tr>
                    <tr class="info">
                        <td>SUPPLIER</td><td>
                            <select name="supplier"class="form-control">
                                <?php
                                if (isset($name)) {
                                    foreach ($name as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="info">
                        <td>DATE</td><td><input name="date" required class="form-control" type="date" value="<?php echo date("Y-m-d") ?>"/></td>
                    </tr>
                    <tr class="info">
                        <td>INVOICE NO</td>
                        <td><input class="form-control" name="invoice_no" required value="<?php if(isset($invoice_no)){echo $invoice_no;}else{echo"1";}?>"/></td>
                    </tr>
                    <tr class="info">
                        <td>COMMENT</td>
                        <td>
                            <textarea class="form-control" name="comment">
                             COMMENT
                            </textarea>
                        </td>
                    </tr>
                    <tr class="info">
                        <td>PRODUCT TYPE</td>
                        <td>
                            <select class="form-control" name="product_type" id="product_type">
                                <?php
                                if (isset($product_type)) {
                                    foreach ($product_type as $option) {

                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                    if(!isset($stock_type)){
                        echo"<tr>
                        <td></td>
                        <td><button type='button' class='btn btn-info'  id='check_product_type'>Check</button></td>
                    </tr>";
                    }
                    ?>
                    
                    <!--<tr class="info">
                        <td>TOTAL PAID</td>
                        <td>
                            <input  name="total_paid" class="form-control" value="0"/>
                        </td>
                    </tr>-->

                </table>
            </div>
        </div>

        <div class="col-lg-12" id="product_list">
            <?php
            if(isset($stock_type)){
                $str=strtolower($stock_type);
                $this->load->view("ajax_code/v_stock_$str");
            }
            ?>
        </div>

    </form>
            </div>
        </div>
    </div>
   
</div>
<script>
    $(document).ready(function(){
        $("#check_product_type").click(function(){
            val=$("#product_type").val();
            page="admin/get_product_list_to_stock/"+val;
            change_content("#product_list",page);
        
        
        })
    })
</script>




