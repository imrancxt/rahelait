<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="#">CUSTOMER PRODUCT</a>
            </li>
        </ol>
    </div>
    
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Customer Information
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <?php
                if (isset($customer_name)) {
                    echo"<tr class='info'><td>CUSTOMER NAME</td><td>$customer_name[0]</td></tr>";
                    echo"<tr class='info'><td>PHONE NUMBER</td><td>$mobile_no[0]</td></tr>";
                    echo"<tr class='info'><td>ADDRESS</td><td>$address[0]</td></tr>";
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
      <div class="col-lg-12">
          <div class="panel panel-inverse">
              <div class="panel-heading">
                  Sold Product
              </div>
              <div class="panel-body">
                      <div class="table-responsive">
            
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="success">
                        <th>
                            SERIAL 
                        </th>
                        <th>
                            PRODUCT TYPE
                        </th>
                        <th>
                            PRODUCT NAME
                        </th>
                        <th>
                            SALES PRICE
                        </th>
                        <th>
                            QUANTITY
                        </th>
                        <th>
                            TOTAL AMOUNT
                        </th>
                        <th>
                            DATE
                        </th>
                    </tr>
                </thead>
                <?php
                if(isset($product_type)){
                    for($i=0;$i<count($product_type);$i++){
                        echo"<tr class='warning'><td>$i</td><td>$product_type[$i]</td><td>$product_name[$i]</td><td>$sales_price[$i]</td>
                        <td>$quantity[$i]</td><td>$total_amount[$i]</td><td>$date[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
              </div>
          </div>
    
    </div>
</div>