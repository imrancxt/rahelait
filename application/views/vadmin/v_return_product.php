
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/return_product">RETURN PRODUCT</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    
    <div class="col-lg-4">
        <select class="form-control" id="select_invoice">
            <option>Select An Invoice</option>
            <?php
            if(isset($invoice_no)){
                foreach($invoice_no as $invoice){
                    echo"<option>$invoice</option>";
                }
            }
            ?>
        </select>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12"><div id="message1"></div></div>
    
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Add Return Product
            </div>
            <div class="panel-body">
                 <form class='change_content_by_form' content="#message1" action='<?php echo base_url(); ?>admin/update_sales_product' method='POST' enctype='multipart/form-data'>
            <div class="table-responsive">
             <table class="table table-bordered table-hover" id="data_table">
                 
             </table>
         </div>
            
        </form>
            </div>
        </div>
       
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#select_invoice").on("change",function(){
            invoice=$(this).val();
            page="admin/return_product2/"+invoice;
            change_content("#data_table",page);
        })
    })
    </script>