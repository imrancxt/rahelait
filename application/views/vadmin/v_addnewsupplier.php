
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/addnewsupplier">ADD NEW SUPPLIER</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    
    <div class="col-lg-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Add New Product
            </div>
            <div class="panel-body">
                <div id="add_new_product_type"></div>
        <form class='change_content_by_form' content="#add_new_product_type" action='<?php echo base_url(); ?>admin/p_add_new_supplier' method='POST' enctype='multipart/form-data'>
            <div class="table-responsive">

                <table class="table table-bordered table-hover">
                    <tr class="info">
                        <td>NAME</td><td><input name="name" required class="form-control"/></td>
                    </tr>
                    <tr class="info">
                        <td>PHONE</td><td><input name="phone" required class="form-control"/></td>
                    </tr>
                    <tr class="info">
                        <td>SALES PERSON</td><td><input name="sales_person" required class="form-control"/></td>
                    </tr>
                    <tr class="info">
                        <td>ADDRESS</td><td><textarea name="address" class="form-control"></textarea></td>
                    </tr>
                    <tr class="info">
                        <td>DATE</td><td><input name="date" class="form-control" type="date" value="<?php echo date("Y-m-d")?>"/></td>
                    </tr>
                    <tr><td></td><td><button class="btn btn-primary" style="width:100%">SUBMIT</button></td></tr>
                </table>
            </div>
        </form>
            </div>
        </div>
        
    </div>
</div>