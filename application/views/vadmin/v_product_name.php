

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/productname">PRODUCT NAME</a>
            </li>
        </ol>
    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <div id="add_new_product_name">
        </div>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                ADD NEW PRODUCT NAME
            </div>
            <div class="panel-body">
                <form class='change_content_by_form' content="#add_new_product_name" action='../admin/add_new_product_name' method='POST' enctype='multipart/form-data'>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr class="warning">
                                <td>PRODUCT TYPE</td>
                                <td>
                                    <select class="form-control" name="product_type">
                                        <?php
                                        if (isset($product_type_serial)) {
                                            foreach ($product_type as $option) {
                                                echo"<option>$option</option>";
                                            }
                                        }
                                        ?>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="warning">
                                <td>PRODUCT BRAND</td>
                                <td>
                                    <select class="form-control" name="product_brand">
                                        <?php
                                        if (isset($brand_serial)) {
                                            foreach ($brand as $option) {
                                                echo"<option>$option</option>";
                                            }
                                        }
                                        ?>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="warning">
                                <td>SCALE</td>
                                <td>
                                    <select class="form-control" name="scale">
                                        <?php
                                        if (isset($scale)) {
                                            foreach ($scale as $option) {
                                                echo"<option>$option</option>";
                                            }
                                        }
                                        ?>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="warning">
                                <td>MEASUREMENT</td>
                                <td>
                                    <select class="form-control" name="measurement">
                                        <?php
                                        if (isset($measurement)) {
                                            foreach ($measurement as $option) {
                                                echo"<option>$option</option>";
                                            }
                                        }
                                        ?>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="warning">
                                <td>PRODUCT NAME</td>
                                <td><input required class="form-control" name="product_name"/></td>
                            </tr>

                            <tr><td></td><td><button class="btn btn-primary" style="width:100%">SUBMIT</button></td></tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                PRODUCT NAME LIST
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="success">
                        <th>
                            SERIAL 
                        </th>
                        <th>
                            PRODUCT TYPE
                        </th>
                        <th>
                            BRAND
                        </th>
                        <th>PRODUCT NAME</th>
                        <th>SCALE</th>
                        <th>MEASUREMENT</th>
                        <th>INSERTION DATE</th>
                        <th>EDIT</th>
                        <th>LEDGER</th>

                    </tr>
                </thead>
                <?php
                if (isset($pn_serial)) {
                    for ($i = 0; $i < count($pn_serial); $i++) {
                        $url ="../admin/product_ledger?id=$pn_serial[$i]";
                        echo"<tr class='warning'><td>$i</td><td>$pn_type[$i]</td><td>$pn_brand[$i]</td><td>$pn_product_name[$i]</td>
                            <td>$pn_scale[$i]</td><td>$pn_measurement[$i]</td><td>$insertion_date[$i]</td>
                            <td><button class='btn btn-default update_product_name' serial='$pn_serial[$i]' data-toggle='modal' data-target='#myModal2'>EDIT</button></td>
                             <td><a href='$url'>ACCOUNT</a></td>
                             </tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
       
    </div>
</div>


<div class="row">
    <form class='change_content_by_form' content="#message2" action='<?php echo base_url(); ?>admin/f_update_product_name' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT PRODUCT BRANCH</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="edit_product_name_table">

                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message2">

                    </div>
                </div>

            </div>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){
        var $rows = $('#data_table .warning');
        $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        }); 
        $(".update_product_name").click(function(){
            serial=$(this).attr("serial");
            page="admin/update_product_name/"+serial;
            change_content("#edit_product_name_table",page);
        })
    });
</script>