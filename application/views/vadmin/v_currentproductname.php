
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url(); ?>admin/currentproductname">CURRENT PRODUCT NAME</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="info">
                        <th>SERIAL</th>
                        <th>PRODUCT TYPE</th>
                        <th>PRODUCT NAME</th>
                        <th>PRODUCT BRAND</th>
                        <th>DESCRIPTION</th>
                        <th>EDIT</th>
                    </tr>
                </thead>
                <?php
                if (isset($product_name_serial)) {
                    for ($i = 0; $i < count($product_name_serial); $i++) {
                        echo"<tr class=warning><td>$i</td><td>$product_type[$i]</td><td>$product_name[$i]</td><td>$product_brand[$i]</td><td><pre>$description[$i]</pre></td>
                <td><button class='btn edit_product_name' serial='$product_name_serial[$i]' data-toggle='modal' data-target='#myModal3'>EDIT</button></td></tr>";
                    }
                }
                ?>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <form class='change_content_by_form' content="#message3" action='<?php echo base_url(); ?>admin/update_product_name' method='POST' enctype='multipart/form-data'>
        <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">EDIT PRODUCT NAME</h4>
                    </div>
                    <div class="modal-body" id="change_modal">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="edit_product_name_table">
                                
                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                    <div id="message3">

                    </div>
                </div>

            </div>
        </div>
    </form>
</div>



<script src="../js/customize.js"></script>
<script>
    $(document).ready(function(){
        $(document).on('click','.edit_product_name',function(){
            product_name_serial=$(this).attr("serial");
            page="admin/get_product_name_to_edit/"+product_name_serial;
            change_content("#edit_product_name_table",page);
        });
    })
</script>