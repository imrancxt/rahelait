
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/profile">PROFILE</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                Update Admin Info
            </div>
            <div class="panel-body">
                <div id="message1" style="text-align: center">

                </div>
                <form class='change_content_by_form' content="#message1" action='<?php echo base_url(); ?>admin/update_admin' method='POST' enctype='multipart/form-data'>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <?php
                            if (isset($admin_name)) {
                                echo"<tr class='warning'><td>ADMIN NAME</td><td><input required name='admin_name' class='form-control' value='$admin_name[0]'/></td></tr>";
                                echo"<tr class='warning'><td>SHOP NAME</td><td><input required name='admin_shop_name' class='form-control' value='$admin_shop_name[0]'/></td></tr>";
                                echo"<tr class='warning'><td>ADMIN PASSWORD(old)</td><td><input required type='password' name='admin_old_password' class='form-control'/></td></tr>";
                                echo"<tr class='warning'><td>ADMIN PASSWORD(new)</td><td><input required type='password' name='admin_new_password' class='form-control'/></td></tr>";
                                echo"<tr><td></td><td><button type='submit' class='btn btn-primary'>UPDATE</button></td></tr>";
                            }
                            ?>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>



</div>
