<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li class="active">
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li>
                <a href="../admin/personal_dr_cr">PERSONAL DEBIT CREDIT</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                ADD PERSONAL DEBIT-CREDIT
            </div>
            <div class="panel panel-body">
                <div id="message1"></div>
                <form class='change_content_by_form' content="#message1" action='../admin/post_personal_dr_cr' method='POST' enctype='multipart/form-data'>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" >
                            <tr class="warning">
                                <td>DATE</td><td><input name="date" required type="date" class="form-control"/></td>
                            </tr>
                            <tr class="warning">
                                <td>DESCRIPTION</td><td><textarea name="description"  class="form-control"></textarea></td>
                            </tr>
                            <tr class="warning">
                                <td>DEBIT(-)</td><td><input required name="dr" class="form-control"/></td>
                            </tr>
                            <tr class="warning">
                                <td>CREDIT(+)</td><td><input required name="cr" class="form-control"/></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td> <button class="btn btn-danger" type="submit"  style="width: 100%;" >SUBMIT</button></td>
                            </tr>
                        </table>

                    </div>
                </form>
            </div>
        </div>


    </div>
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="panel panel-inverse">
            <div class="panel-heading">
                ALL PERSONAL DEBIT-CREDIT
            </div>
            <div class="panel-body">
                <div class="form-inline">
                    <div class="form-group">
                        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date1"/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="date" value='<?php echo date("Y-m-d"); ?>' id="date2"/>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" style="width: 99%" id="filter">FILTER</button>
                    </div>
                    <div class="form-group">
                        <strong>(-)BALANCE MEANS YOU ARE STILL LOSS STATE IN THIS TRANSACTION AND (+)BALANCE MEANS YOU ARE NOW PROFIT STATE IN THIS TRANSACTION</strong>
                    </div>
                </div>
                <div class="table-responsive">
                    <br><table class="table table-bordered table-hover" id="data_table">
                        <thead>
                            <tr class="active">
                                <th>SERIAL</th>
                                <th>DESCRIPTION</th>
                                <th>DATE</th>
                                <th>DEBIT</th>
                                <th>CREDIT</th>
                                <th>BALANCE</th>
                            </tr>
                        </thead>
                        <?php
                        $balance = 0;
                        if (isset($date)) {
                            for ($i = 0; $i < count($date); $i++) {
                                $balance = $cr[$i] - $dr[$i] + $balance;
                                echo"<tr class='warning'><td>$i</td><td>$description[$i]</td><td>$date[$i]</td><td>$dr[$i]</td><td>$cr[$i]</td><td>$balance</td></tr>";
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $("#filter").click(function(){
            date1=$("#date1").val();
            date2=$("#date2").val();
            page="admin/filter_personal_dr_cr/"+date1+"/"+date2;
            change_content("#data_table",page);
        })
    })
</script>