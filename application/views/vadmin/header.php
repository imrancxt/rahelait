<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin Panel</title>

        <link href="<?php echo $asset_url; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $asset_url; ?>css/tooltip.css" rel="stylesheet">
        <link href="<?php echo $asset_url; ?>css/sb-admin.css" rel="stylesheet">

        <link href="<?php echo $asset_url; ?>css/plugins/morris.css" rel="stylesheet">

        <link href="<?php echo $asset_url; ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $asset_url; ?>css/fileinput.css" rel="stylesheet">
        <link href="<?php echo $asset_url; ?>css/customize.css" rel="stylesheet">
        <link href="<?php echo $asset_url; ?>css/toggle.css" rel="stylesheet">
        <link href="<?php echo $asset_url; ?>css/plugins/morris.css" rel="stylesheet">

        <script src="<?php echo $asset_url; ?>js/jquery.js"></script>
        <script src="<?php echo $asset_url; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $asset_url; ?>js/admin.js"></script>
        <script src="<?php echo $asset_url; ?>js/fileinput.js"></script>
        <script src='<?php echo $asset_url; ?>js/plugins/morris/raphael.min.js'></script>
        <script src='<?php echo $asset_url; ?>js/plugins/morris/morris.js'></script>
        <script src='<?php echo $asset_url; ?>js/print.js'></script>
        <script src="<?php echo $asset_url; ?>js/customize.js"></script>
        <script src='<?php echo $asset_url; ?>js/toggle.js'></script>


    </head>

    <body style="background-color: <?php echo $_SESSION['top_side_color']; ?>">

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: <?php echo $_SESSION['top_side_color']; ?>">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>admin/dashboard"><?php echo $_SESSION['shop_name'] ?></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="http://localhost/phpmyadmin/server_export.php?db=&table=&server=1&target=&token=448be83169a9a4315722c799c2bfbde1#PMAURL-0:server_export.php?db=&table=&server=1&target=&token=448be83169a9a4315722c799c2bfbde1">Export database</a>
                            </li>

                            <li>
                                <a href="http://localhost/phpmyadmin/server_export.php?db=&table=&server=1&target=&token=448be83169a9a4315722c799c2bfbde1#PMAURL-1:server_import.php?db=&table=&server=1&target=&token=772c14893158307e7b4962807017cd59">Import database</a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url(); ?>admin/sales_report">Sales report</a>
                            </li>
                             <li>
                                <a href="<?php echo base_url(); ?>admin/sales_report2">Sales report2</a>
                            </li>
                            <li class="divider">

                            </li>
                            <li>
                                <a href="<?php echo base_url() ?>admin/customer_report">Customer report</a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?php echo $_SESSION['type'] ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php
                            if ($_SESSION['type'] == "Admin") {
                                $url = base_url() . "admin/profile";
                                echo"<li><a href='$url'><i class='fa fa-fw fa-user'></i> Profile</a></li><li class='divider'></li>";
                            }
                            ?>
                            <li>
                                <a href="<?php
                            if ($_SESSION['type'] == "Admin") {
                                $log_out = "login/admin_log_out";
                            } else {
                                $log_out = "login/user_log_out";
                            } echo base_url() . "$log_out"
                            ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav" style="background-color: <?php echo $_SESSION['top_side_color']; ?>">
                        <li class="ctive">
                            <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-fw fa-dashboard"></i>DASHBOARD</a>
                        </li>

                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i>SUPPLIER<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/addnewsupplier">ADD NEW SUPPLIER</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/currentsupplier">CURRENT SUPPLIER</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i>CUSTOMER<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo3" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/customer_type">CUSTOMER TYPE</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/area">AREA</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/addnewcustomer">ADD NEW CUSTOMER</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/currentcustomer">CURRENT CUSTOMER</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i>STOCK<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo2" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/add_new_product_to_stock">ADD NEW PRODUCT</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/product_in_stock">CURRENT STOCK</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/return_product">ADD RETURN PRODUCT</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/return_product_list">ALL RETURNED PRODUCT</a>
                                </li>
                                <li class="ctive">
                                    <a href="<?php echo base_url(); ?>admin/branch">BRANCH</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/branch_wise_stock">BRACH WISE STOCK</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-arrows-v"></i>PRODUCTS<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo4" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/producttype">PRODUCT TYPE</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/brand">BRAND</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/measuring_scale">MEASURING SCALE</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/productname">PRODUCT NAME</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-arrows-v"></i>ALL INVOICE RECEIPT<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo5" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/all_customer_receipt/">CUSTOMER RECEIPT</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/all_supplier_receipt">SUPPLIER RECEIPT</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/personal_dr_cr"><i class="fa fa-fw fa-phone"></i>PERSONAL DEBIT-CREDIT</a>
                        </li>
                        <li class="ctive">
                            <a href="<?php echo base_url(); ?>admin/cashbook"><i class="fa fa-fw fa-phone"></i>CASH BOOK</a>
                        </li>
                    </ul>

                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <div id="core_container">
                <div id="page-wrapper" style="min-height:613px;background-color:<?php echo $_SESSION['body_color']; ?>">

                    <div class="container-fluid" >


