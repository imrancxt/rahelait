<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="#">SALE PRODUCT</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
            <div id="message1"></div>
            <form id="sales_product" name="form"  action='../admin/check_product' method='POST' enctype='multipart/form-data'>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                Customer Information
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive" style="height: 120px">
                                    <table class="table table-bordered table-hover">
                                        <?php
                                        if (isset($customer_name)) {
                                            echo"<tr class='success'><td>CUSTOMER NAME</td>
                    <td>$customer_name[0]</td></tr>
                    <tr class='success'><td>SHOP NAME</td><td>$shop_name[0]</td></tr>
                    <tr class='success'><td>ADDRESS</td><td>$address[0]</td></tr>";
                                            echo"<input type='hidden' name='customer_id' value='$customer_serial'/>";
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">REPORTS</h4>
                                    </div>
                                    <div class="modal-body" id="change_modal">
                                        <div class="table-responsive" id="checking_content">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="table-responsiv">
                            <table class="table table-bordered table-hover">
                                <tr class="info"><td>INVOICE NO</td><td><input  required name="invoice_no" class="form-control" value="<?php if (isset($invoice_no)) {
                                            echo"$invoice_no";
                                        } else {
                                            echo 1;
                                        } ?>"/></td></tr>
                                <tr class="info"><td>DATE</td><td><input required type="date" class="form-control" name="date" value="<?php echo date('Y-m-d') ?>"/></td></tr>
                                <tr class="info"><td>PAID</td><td><input required class="form-control" name="paid" value="0"/></td></tr>
                                <tr class="info"><td>DISCOUNT</td><td><input required  class="form-control" name="disscount" value="0"/></td></tr>
                                <tr class="info"><td>LABER BILL</td><td><input required  class="form-control" name="laber_bill" value="0"/></td></tr>
                            </table>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <button class="btn btn-default" type="submit" name="action" value="submit" data-toggle='modal' data-target='#myModal2'>SUBMIT</button>
                        <button class="btn btn-default" type="submit" name="action" value="check_amount" data-toggle='modal' data-target='#myModal2'>CHECK</button>
                        <a class="btn btn-danger" id="refresh">RESET</a>
                        <a class="btn btn-danger" id="check_receipt" data-toggle='modal' data-target='#myModal2'>CHECK RECEIPT</a>
                    </div>

                    <div class="col-lg-12">
                        <div class="table-responsiv">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="active">
                                        <th>
                                            CHECK
                                        </th>
                                        <th>
                                            PRODUCT  NAME
                                        </th>
                                        <th>BRANCH</th>
                                        <th>
                                            AVAILABLE QUANTITY
                                        </th>
                                        <th>
                                            PURCHASE PRICE(BUNDLE)
                                        </th>
                                        <th>
                                            SALES PRICE(BUNDLE)
                                        </th>
                                        <th>
                                            SALES(BUNDLE)
                                        </th>
                                        <th>
                                            SALES(PICES)
                                        </th>
                                    </tr>
                                </thead>
                                <?php
                                if (isset($bundle_tin_stock_product_serial)) {
                                    for ($i = 0; $i < count($bundle_tin_stock_product_serial); $i++) {
                                        $product_name_id = $bundle_tin_product_name_id[$i];
                                        $pices = $bundle_tin_quantity[$i] % $bundle_tin_measurement[$i];
                                        $avl_bundle = ($bundle_tin_quantity[$i] - $pices) / $bundle_tin_measurement[$i];
                                        $sales_price_in_bundle = ($bundle_tin_sales_price[$i] * $bundle_tin_measurement[$i]);
                                        $purchase_price_in_bundle = $bundle_tin_purchase_price[$i] * $bundle_tin_measurement[$i];

                                        echo"<tr class='warning'><td><input type='checkbox' value='$bundle_tin_stock_product_serial[$i]' name='bundle_tin_id[]'/></td>
                          <td>$bundle_tin_product_name[$i]<td>$bundle_tin_branch[$i]</td><td>$avl_bundle bundle & $pices pices</td><td>$purchase_price_in_bundle</td>
                           <td><input class='form-control' name='bundle_tin_sales_price$bundle_tin_stock_product_serial[$i]' value='$sales_price_in_bundle'/></td>
                          ";
                                        /*  echo"<td><select name='bundle_sales_tin_bundle$bundle_tin_stock_product_serial[$i]' class='form-control'>";
                                          for ($j = 0; $j <= $avl_bundle; $j++) {
                                          echo"<option>$j</option>";
                                          }
                                          echo"</select></td>"; */
                                        echo"<td><input class='form-control' type='number' name='bundle_sales_tin_bundle$bundle_tin_stock_product_serial[$i]' min='0' max='$avl_bundle' value='0'/></td>";
                                        echo"<td><input type='number' class='form-control' name='bundle_sales_tin_pices$bundle_tin_stock_product_serial[$i]' min='0' max='$bundle_tin_quantity[$i]' value='1'/></td>";
                                        echo"<input type='hidden' name='bundle_tin_measurement$bundle_tin_stock_product_serial[$i]' value='$bundle_tin_measurement[$i]'/>";
                                        echo"<input type='hidden' name='bundle_tin_name$bundle_tin_stock_product_serial[$i]' value='$bundle_tin_product_name[$i]'/>";
                                        echo"<input type='hidden' name='bundle_tin_name_id$bundle_tin_stock_product_serial[$i]' value='$product_name_id'/>";
                                        echo"</tr>";
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>


                    <div class="col-lg-12">
                        <div class="table-responsiv">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="active">
                                        <th>
                                            CHECK
                                        </th>
                                        <th>
                                            PRODUCT NAME
                                        </th>
                                        <th>BRANCH</th>
                                        <th>
                                            AVAILABLE QUANTITY
                                        </th>

                                        <th>
                                            PURCHASE PRICE(FEET)
                                        </th>
                                        <th>
                                            SALES PRICE(FEET)
                                        </th>
                                        <th>SALES-QUANTITY</th>
                                    </tr>
                                </thead>
                                <?php
                                if (isset($fiber_tin_stock_product_serial)) {
                                    for ($i = 0; $i < count($fiber_tin_stock_product_serial); $i++) {
                                        $product_name_id = $fiber_tin_product_name_id[$i];
                                        $avl_bundle = $fiber_tin_quantity[$i] / $fiber_tin_measurement[$i];
                                        $pices = $fiber_tin_quantity[$i] % $fiber_tin_measurement[$i];
                                        //$sales_price_in_bundle = ($bundle_tin_sales_price[$i] * $bundle_tin_measurement[$i]);
                                        //$purchase_price_in_bundle = $bundle_tin_purchase_price[$i] * $bundle_tin_measurement[$i];

                                        echo"<tr class='warning'><td><input type='checkbox' value='$fiber_tin_stock_product_serial[$i]' name='fiber_tin_id[]'/></td>
                          <td>$fiber_tin_product_name[$i]<td>$fiber_tin_branch[$i]</td><td>$fiber_tin_quantity[$i]</td><td>$fiber_tin_purchase_price[$i]</td>
                           <td><input class='form-control' name='fiber_tin_sales_price$fiber_tin_stock_product_serial[$i]' value='$fiber_tin_sales_price[$i]'/></td>
                          ";

                                        echo"<td><input type='number' class='form-control'  name='fiber_sales_tin_pices$fiber_tin_stock_product_serial[$i]' min='1' max='$fiber_tin_quantity[$i]' value='1'/></td>";
                                        echo"<input type='hidden' name='fiber_tin_measurement$fiber_tin_stock_product_serial[$i]' value='$fiber_tin_measurement[$i]'/>";
                                        echo"<input type='hidden' name='fiber_tin_name$fiber_tin_stock_product_serial[$i]' value='$fiber_tin_product_name[$i]'/>";
                                        echo"<input type='hidden' name='fiber_tin_name_id$fiber_tin_stock_product_serial[$i]' value='$product_name_id'/>";
                                        echo"</tr>";
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="table-responsiv">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="active">
                                        <th>
                                            CHECK
                                        </th>
                                        <th>
                                            PRODUCT NAME
                                        </th>
                                        <th>BRANCH</th>
                                        <th>
                                            AVAILABLE QUANTITY
                                        </th>
                                        <th>
                                            MEASURING-SCALE
                                        </th>
                                        <th>

                                            PURCHASE PRICE
                                        </th>
                                        <th>
                                            SALES PRICE
                                        </th>
                                        <th>SALES-QUANTITY</th>
                                    </tr>
                                </thead>
                                <?php
                                if (isset($other_stock_product_serial)) {
                                    for ($i = 0; $i < count($other_stock_product_serial); $i++) {
                                        $product_name_id = $other_product_name_id[$i];
                                        $measuring_scale = "$other_measurement[$i]$other_scale[$i]";
                                        echo"<tr class='warning'><td><input type='checkbox' value='$other_stock_product_serial[$i]' name='other_id[]'/></td>
                          <td>$other_product_name[$i]<td>$other_branch[$i]</td><td>$other_quantity[$i]</td><td>$measuring_scale</td><td>$other_purchase_price[$i]</td>
                           <td><input class='form-control' name='other_sales_price$other_stock_product_serial[$i]' value='$other_sales_price[$i]'/></td>
                          ";

                                        echo"<td><input type='number' class='form-control'  name='other_sales_quantity$other_stock_product_serial[$i]' min='1' max='$other_quantity[$i]' value='1'/></td>";
                                        echo"<input type='hidden' name='other_measurement$other_stock_product_serial[$i]' value='$other_measurement[$i]'/>";
                                        echo"<input type='hidden' name='other_name$other_stock_product_serial[$i]' value='$other_product_name[$i]'/>";
                                        echo"<input type='hidden' name='other_name_id$other_stock_product_serial[$i]' value='$product_name_id'/>";
                                        echo"<input type='hidden' name='other_name_id$other_stock_product_serial[$i]' value='$product_name_id'/>";
                                        echo"</tr>";
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        
    </div>

</div>
<script>
    $(document).ready(function(){
        var $rows = $('.table-hover .warning');
        $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });
        $("#check_amount").click(function(){
            document.form.submit();
        });
        $(document).on('submit','#sales_product',function(e)
        {
            var formObj =$(this);
            var formURL =formObj.attr("action");
            var formData = new FormData(this);
            $.ajax({
                url: formURL,
                type: 'POST',
                data:formData,
                mimeType:"multipart/form-data",
                contentType: false,
                cache: false,
                processData:false,
                success: function(data, textStatus, jqXHR)
                {
                    if(data!=""){
                        $("#checking_content").html(data);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    alert(errorThrown);
                }
                        
            });
            e.preventDefault();
        });
        $("#refresh").click(function(){
            window.location= $(location).attr('href'); 
        });
        $("#check_receipt").click(function(){
            cust_id=$("input[name=customer_id]").val();
            invoice_no=$("input[name='invoice_no']").val();
            page="admin/get_customer_invoice_data/"+cust_id+"-"+invoice_no;
            change_content("#checking_content",page);
            
        });
       
    });
</script>