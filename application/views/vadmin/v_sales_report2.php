<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/sales_report2">SALES REPORT2</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Sales Report2
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover" border="1" id="data_table">
                <thead>
                    <tr class="info">
                        <th>CUSTOMER NAME</th>
                        <th>SHOP NAME</th>
                        <th>ADDRESS</th>
                        <th>ROD</th>
                        <th>TIN</th>
                        <th>CEMENT</th>
                        <th>TOTAL</th>
                    </tr>
                </thead> 
                <?php
                $total_rod_amount=0; $total_tin_amount=0; $total_cement_amount=0;$total_amount=0;
                for ($i = 0; $i < count($data); $i++) {
                    $total_rod_amount+=$data[$i]['rod_amount'];
                    $total_tin_amount+=$data[$i]['tin_amount'];
                    $total_cement_amount+=$data[$i]['cement_amount'];
                    $total_amount=$total_amount+$data[$i]['total'];
                    echo"<tr class='warning'><td>{$data[$i]['customer_name']}</td><td>{$data[$i]['shop_name']}</td><td>{$data[$i]['address']}</td><td>{$data[$i]['rod_amount']}</td><td>{$data[$i]['tin_amount']}</td><td>{$data[$i]['cement_amount']}</td><td>{$data[$i]['total']}</td></tr>";
                }
                echo"<tr class='success'><td></td><td></td><td></td><td>$total_rod_amount(TOTAL)</td><td>$total_tin_amount(TOTAL)</td><td>$total_cement_amount(TOTAL)</td><td>$total_amount(TOTAL)</td></tr>";
                ?>
            </table>
        </div> 
            </div>
        </div>
         
    </div>
</div>


