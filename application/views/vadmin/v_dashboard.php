<link rel="stylesheet" href="../plugins/morris/morris.css">
<script src="../js/raphael.js"></script>
<script src="../plugins/morris/morris.min.js"></script>
<style>
    .carousel,
    .item,
    .active {
        height: 100%;
    }

    .carousel-inner {
        height: 100%;
    }
    .fill {
        width: 100%;
        height:100%;
        background-position: center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #myCarousel{

    }
</style>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
              Available Product Vs Quantity Chart
            </div>
            <div class="panel-body">
                <div id="morris-donut-chart1"></div>
            </div>
        </div>
    </div>
      <div class="col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
              Quantity Vs Product Name
            </div>
            <div class="panel-body">
                <div id="morris-donut-chart3"></div>
            </div>
        </div>
    </div>
     <div class="col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
               Month Vs Sold Product Amount
            </div>
            <div class="panel-body">
                <div id="morris-donut-chart2"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
              Month Vs Stock Product Amount
            </div>
            <div class="panel-body">
                <div id="morris-bar-chart1"></div>
            </div>
        </div>
    </div>
      <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Date Vs Sold Product Amount
            </div>
            <div class="panel-body">
                <div id="morris-bar-chart3"></div>
            </div>
        </div>
    </div>
</div>


<script>
$(function() {
Morris.Donut({
        element: 'morris-donut-chart1',
        data:<?php echo $avl_product?>,
        resize: true
    });
Morris.Bar({
        element: 'morris-bar-chart1',
        data:<?php echo $stock_product ?>,
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['TOTAL'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });
Morris.Donut({
        element: 'morris-donut-chart2',
        data:<?php echo $sold_product ?>,
        resize: true
    });
Morris.Donut({
        element: 'morris-donut-chart3',
        data:<?php echo $avl_product_product_name?>,
        resize: true
    });
Morris.Bar({
        element: 'morris-bar-chart3',
        data:<?php echo $sold_product_by_date?>,
        xkey: 'd',
        ykeys: ['visits'],
        labels: ['Amount'],
        smooth: false,
        resize: true
    });

    });
</script>;

