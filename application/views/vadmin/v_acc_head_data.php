<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb" class=" btn-danger">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../account/acc_head_general_ledger">ACCOUNT HEAD LEDGER</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Filter Account Head Data
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>
                        Select Account Head
                    </label>
                    <select class="form-control" id="acc_head">
                        <?php
                        if (isset($acc_head)) {
                            foreach ($acc_head as $option) {
                                echo"<option>$option</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Select Date</label>
                    <input class="form-control" type="date" id="date" value="<?php echo date("Y-m-d") ?>"/>
                </div>
                <div class="form-group">
                    <button class="btn btn-default" id="filter">Filter</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Account Head Data
            </div>
            <div class="panel-body">
                <div class="table-responsive" id="data_table">
            <?php
            $this->load->view('ajax_code/v_filter_acc_head_data');
            ?>
        </div>
            </div>
        </div>
        
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#filter").click(function(){
            acc_head=$("#acc_head").val();
            date=$("#date").val();
            page="account/filter_acc_head_data?acc_head="+acc_head+"&date="+date;
            change_content("#data_table",page);
        })
    })
    </script>