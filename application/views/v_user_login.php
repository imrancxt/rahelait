
<html>
    <head>
        <meta charset="utf-8">
        <link href="<?php echo $asset_url?>css/login.css" rel="stylesheet">
        <script src='<?php echo $asset_url?>js/jquery.js'></script>
         <script src='<?php echo $asset_url?>js/login.js'></script>
    </head>
    <body>
        <div class="login">
            <form id='login_form' action='<?php echo base_url(); ?>login/check_user' method='POST' enctype='multipart/form-data'> 
                <fieldset>
                    <legend> <h1><strong>Welcome </strong>User</h1></legend>
                    <p><input name="user" type="text" required placeholder="User" onBlur="if(this.placeholer=='')this.placeholder='User'" onFocus="if(this.placeholder=='User')this.placeholder='' "></p>
                    <p><input name="password" type="password" required placeholder="Password" onBlur="if(this.placeholder=='')this.placeholder='Password'" onFocus="if(this.placeholder=='Password')this.placeholder='' "></p>
                    <p><input type="submit" value="Login"></p>
                     <p><a href="<?php echo base_url()?>login/admin_login.php" style="color: green">Login as a admin?</a></p>
                </fieldset>
            </form>
        </div>    </body>
</html>