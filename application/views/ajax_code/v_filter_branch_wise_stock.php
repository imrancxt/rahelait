<div class="col-lg-6">
        <div class="table-responsive">
             <table class="table table-bordered table-hover" border="1" id="data_table">
                <thead>
                    <tr class="active">
                        <th>PRODUCT TYPE</th>
                        <th>TOTAL QUANTITY</th>
                        <th>TOTAL AMOUNT</th>
                    </tr>
                </thead>
                <?php
                if (isset($o_product_type)) {
                    for ($i = 0; $i < count($o_product_type); $i++) {
                        echo"<tr class='warning'><td>$o_product_type[$i]</td><td>$o_quantity[$i]</td><td>$o_amount[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover" border="1" id="data_table">
                <thead>
                    <tr class="active">
                        <th>BRANCH NAME</th>
                        <th>TOTAL AMOUNT</th>
                    </tr>
                </thead>
                <?php
                if (isset($o2_branch_id)) {
                    for ($i = 0; $i < count($o2_branch_id); $i++) {
                        echo"<tr class='warning'><td>$o2_branch_name[$i]</td><td>$o2_total_amount[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="table-responsive">
            <br> <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="active">
                        <th>BRANCH NAME</th>
                        <th>PRODUCT TYPE</th>
                        <th>TOTAL QUANTITY</th>
                        <th>TOTAL AMOUNT</th>
                    </tr>
                </thead>
                <?php
                if (isset($o1_branch_id)) {
                    for ($i = 0; $i < count($o1_branch_id); $i++) {
                        echo"<tr class='warning'><td>$o1_branch_name[$i]</td><td>$o1_product_type[$i]</td><td>$o1_total_quantity[$i]</td><td>$o1_total_amount[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="active">
                        <th>
                            PRODUCT NAME
                        </th>
                        <th>PRODUCT TYPE</th>
                        <th>BRANCH</th>
                        <th>
                            AVAILABLE QUANTITY
                        </th>
                        <th>
                            SCALE
                        </th>
                        <th>
                            PURCHASE PRICE
                        </th>
                        <th>
                            AMOUNT
                        </th>

                    </tr>
                </thead>
                <?php
                if (isset($serial)) {
                    for ($i = 0; $i < count($serial); $i++) {
                        echo"<tr class='warning'><td>$product_name[$i]</td><td>$product_type[$i]</td><td>$branch[$i]</td>
                        <td>$quantity[$i]</td><td>$scale[$i]</td><td>$purchase_price[$i]</td><td>$amount[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
    </div>