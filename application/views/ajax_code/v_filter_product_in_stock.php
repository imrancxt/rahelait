<div class="col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Product Available
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <br> <table class="table table-bordered table-hover" border="1" id="data_table">
                <thead>
                    <tr class="active">
                        <th>PRODUCT TYPE</th>
                        <th>TOTAL QUANTITY</th>
                        
                    </tr>
                </thead>
                <?php
                if(isset($o_product_type)){
                    for($i=0;$i<count($o_product_type);$i++){
                        echo"<tr class='warning'><td>$o_product_type[$i]</td><td>$o_quantity[$i]</td></tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Available Product Quantity
            </div>
            <div class="panel-body">
                 <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="info">
                        <th>SERIAL</th>
                        <th>IMAGE</th>
                        <th>PRODUCT TYPE</th>
                        <th>PRODUCT NAME</th>
                      
                        <th>AVAILABLE QUANTITY</th>
                        
                    </tr>
                </thead>  
                <?php
                if(isset($product_name)){
                    for($i=0;$i<count($product_name);$i++){
                        $img="../img/noimage.jpg";
                        echo"<tr class='warning'><td>$i</td><td><a href='#'><img src='$img' class='img-circle' height='45px' width='45px'/></a></td>
                        <td>$product_type[$i]</td><td>$product_name[$i]</td><td>$quantity[$i]</td>
                        </tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
       
    </div>