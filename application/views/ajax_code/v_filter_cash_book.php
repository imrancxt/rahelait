<div class="col-lg-3"><strong id="total_dr"></strong></div><div class="col-lg-3"><strong id="total_cr"></strong></div>
<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="data_table">
            <thead>
                <tr class="active">
                    <th>SERIAL</th>
                    <th>DESCRIPTION</th>
                    <th>DEBIT</th>
                    <th>CREDIT</th>
                </tr>
            </thead>
            <?php
            static $serial = 0;
            $total_dr = 0;
            $total_cr = 0; //$previous_day_redit;
            if ($previous_day_credit < 0) {
                $total_cr -=$previous_day_credit;
                
            } else {
                $total_dr = $previous_day_credit;
            }
            echo"<tr class='warning'><td>$serial</td><td>TRANSACTION BEFORE TODAY</td><td>$total_dr</td><td>$total_cr</td></tr>";
            $serial++;
            if (isset($s1_dr)) {
                for ($i = 0; $i < count($s1_dr); $i++) {
                    $total_dr+=$s1_dr[$i];
                    $total_cr+=$s1_cr[$i];
                    echo"<tr class='warning'><td>$serial</td><td>$s1_description[$i](supplier)</td><td>$s1_dr[$i]</td><td>$s1_cr[$i]</td></tr>";
                    $serial++;
                }
            }
            if (isset($s2_dr)) {
                for ($i = 0; $i < count($s2_dr); $i++) {
                    $total_dr+=$s2_dr[$i];
                    $total_cr+=$s2_cr[$i];
                    echo"<tr class='warning'><td>$serial</td><td>$s2_description[$i](supplier)</td><td>$s2_dr[$i]</td><td>$s2_cr[$i]</td></tr>";
                    $serial++;
                }
            }
            if (isset($c1_dr)) {
                for ($i = 0; $i < count($c1_dr); $i++) {
                    $total_dr+=$c1_dr[$i];
                    $total_cr+=$c1_cr[$i];
                    echo"<tr class='warning'><td>$serial</td><td>$c1_description[$i](customer)</td><td>$c1_dr[$i]</td><td>$c1_cr[$i]</td></tr>";
                    $serial++;
                }
            }
            if (isset($c2_dr)) {
                for ($i = 0; $i < count($c2_dr); $i++) {
                    $total_dr+=$c2_dr[$i];
                    $total_cr+=$c2_cr[$i];
                    echo"<tr class='warning'><td>$serial</td><td>$c2_description[$i](customer)</td><td>$c2_dr[$i]</td><td>$c2_cr[$i]</td></tr>";
                    $serial++;
                }
            }
            if (isset($p1_dr)) {
                for ($i = 0; $i < count($p1_dr); $i++) {
                    $total_dr+=$p1_dr[$i];
                    $total_cr+=$p1_cr[$i];
                    echo"<tr class='warning'><td>$serial</td><td>$p1_description[$i](personal debit-credit)</td><td>$p1_dr[$i]</td><td>$p1_cr[$i]</td></tr>";
                    $serial++;
                }
            }
            ?>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        total_dr=<?php echo $total_dr ?>;
        total_cr=<?php echo $total_cr ?>;
        $("#total_dr").html("TOTAL DEBIT:"+total_dr+" TAKA");$("#total_cr").html("TOTAL CREDIT:"+total_cr+" TAKA");
    })
</script>