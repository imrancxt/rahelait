<div class="table-responsive">

    <table class="table table-bordered table-hover" id="data_table">
        <thead>
            <tr class="success">
                <th>CHECK</th>
                <th>PRODUCT NAME</th>
                <th>BRAND</th>
                <th>QUANTITY<? echo"(in $pn_scale[0])"; ?></th>
                <th>PURCHASE PRICE<? echo"($pn_measurement[0]$pn_scale[0])";?></th>
                <th>SALES PRICE<? echo"($pn_measurement[0]$pn_scale[0])";?></th>
                <th>SCALE</th>
                <th>MEASUREMENT</th>
            </tr>
        </thead>
        <?php
         $check="";
        if(isset($stock_type)){
            $check="checked";
        }

        if(isset($pn_serial)){
            for($i=0;$i<count($pn_serial);$i++){
                echo"<tr class='warning'><td><input type='checkbox' $check name='product_name_id[]' value='$pn_serial[$i]'/></td><td>$pn_product_name[$i]</td>
                <td>$pn_brand[$i]</td><td><input class='form-control' type='number' value='1' min='1' name='quantity$pn_serial[$i]'/></td><td><input class='form-control' name='purchase_price$pn_serial[$i]'/></td>
                <td><input class='form-control' name='sales_price$pn_serial[$i]'/></td><td>$pn_scale[0]</td><td>$pn_measurement[0]</td></tr>";
            }
           // echo"<input type='hidden' name='product_name_id'/>";
            echo"<input type='hidden' name='scale' value='$pn_scale[0]'/>";
            echo"<input type='hidden' name='measurement' value='$pn_measurement[0]'/>";
        }
        ?>
    </table>
</div>
 <button class="btn btn-danger">SUBMIT</button>
 <script>
    $(document).ready(function(){
        var $rows = $('#data_table .warning');
        $(document).on('keyup','#search',function(){
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        })
    })
</script>
