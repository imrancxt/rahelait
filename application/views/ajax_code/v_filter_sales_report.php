<div class="col-lg-6">
    <div class="table-responsive">
        <br> <table class="table table-bordered table-hover" border="1" id="data_table">
            <thead>
                <tr class="active">
                    <th>PRODUCT TYPE</th>
                    <th>TOTAL QUANTITY</th>
                    <th>TOTAL AMOUNT</th>
                </tr>
            </thead>
            <?php
            if (isset($o_product_type)) {
                for ($i = 0; $i < count($o_product_type); $i++) {
                    echo"<tr class='warning'><td>$o_product_type[$i]</td><td>$o_quantity[$i]</td><td>$o_amount[$i]</td></tr>";
                }
            }
            ?>
        </table>
    </div>
</div>
<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" border="1" id="data_table">
            <thead>
                <tr class="active">
                    <th>SERIAL</th>
                    <th>CUSTOMER NAME</th>
                    <th>CUSTOMER TYPE</th>
                    <th>AREA</th>
                    <th>PRODUCT TYPE</th>
                    <th>PRODUCT NAME</th>
                    <th>QUANTITY</th>
                    <th>AMOUNT</th>
                    <th>DATE</th>
                </tr>
            </thead>
            <?php
            if (isset($product_name)) {
                for ($i = 0; $i < count($product_name); $i++) {
                    echo"<tr class='warning'>
                               <td>$i</td><td>$customer_name[$i]</td><td>$type[$i]</td><td>$area[$i]</td><td>$product_type[$i]</td>
                               <td>$product_name[$i]</td><td>$quantity[$i]</td><td>$total_amount[$i]</td><td>$sales_date[$i]</td>
                               </tr>";
                }
            }
            ?>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        var $rows = $('#data_table .warning');
        $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });
    })
</script>