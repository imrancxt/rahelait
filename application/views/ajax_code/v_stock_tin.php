   <div class="row">
        <div class="col-lg-6">
            <strong>INSTRUCTION FOR WHEN NORMAL TIN STOCK IN BUNDLE:</strong>
            
                <li>UNIT IN BUNDLE</li>
                <li>PURCHASE PRICE IN PER-BUNDLE</li>
                <li>SALES PRICE IN PER-BUNDLE</li>
            
        </div>
        <div class="col-lg-6" style="float: right">
            <strong>INSTRUCTION FOR WHEN FIBER TIN STOCK IN FEET :</strong>
                <li>UNIT IN NUMBER OF TIN</li>
                <li>PURCHASE PRICE IN PER-FEET</li>
                <li>SALES PRICE IN PER-FEET</li>
        </div>
    </div>
<br>
<div class="table-responsive">
 
    <table class="table table-bordered table-hover"  id="data_table">
        <thead>
            <tr class="success">
                <th>CHECK</th>
                <th>PRODUCT NAME</th>
                <th>BRAND</th>
                <th>UNIT</th>
                <th>PURCHASE PRICE/UNIT</th>
                <th>SALES PRICE/UNIT</th>
                <th>SCALE</th>
                <th>MEASUREMENT</th>
            </tr>
        </thead>
        <?php
        if (isset($pn_serial)) {
             $check="";
        if(isset($stock_type)){
            $check="checked";
        }
            for ($i = 0; $i < count($pn_serial); $i++) {
                echo"<tr class='warning'><td><input type='checkbox' $check name='product_name_id[]' value='$pn_serial[$i]'/></td><td>$pn_product_name[$i]</td>
                <td>$pn_brand[$i]</td><td><input class='form-control' name='quantity$pn_serial[$i]' type='number' value='1' min='1'/></td><td><input class='form-control' name='purchase_price$pn_serial[$i]'/></td>
                <td><input class='form-control' name='sales_price$pn_serial[$i]'/></td><td>$pn_scale[$i]</td><td>$pn_measurement[$i]</td></tr>";
                echo"<input type='hidden' name='scale$pn_serial[$i]' value='$pn_scale[$i]'/>";
                echo"<input type='hidden' name='measurement$pn_serial[$i]' value='$pn_measurement[$i]'/>";
            }
            // echo"<input type='hidden' name='product_name_id'/>";
        }
        ?>
    </table>
</div>
<button class="btn btn-danger">SUBMIT</button>
<script>
    $(document).ready(function(){
        var $rows = $('#data_table .warning');
        $(document).on('keyup','#search',function(){
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        })
    })
</script>
