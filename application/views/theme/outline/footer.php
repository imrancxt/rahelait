



</div> <!-- .container-fluid -->
</div> <!-- #page-content -->
</div>
<footer>
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;">&copy; 2015 rahelait</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
    </div>
</footer>
</div>
</div>
</div>


<!-- Switcher -->
<div class="demo-options">
    <div class="demo-options-icon"><i class="ti ti-paint-bucket"></i></div>
    <div class="demo-heading" data-localize="switcher_demo_settings">Demo Settings</div>

    <div class="demo-body">
        <div class="tabular">
            <div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_fixed_header">Fixed Header</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" checked data-size="mini" data-on-color="success" data-off-color="default" name="demo-fixedheader" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
            </div>
            <div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_boxed_layout">Boxed Layout</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-boxedlayout" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
            </div>
            <div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_collapse_leftbar">Collapse Leftbar</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-collapseleftbar" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
            </div>
            <!--<div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_collapse_rightbar">Collapse Rightbar</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="danger" data-off-color="default" name="demo-alternative" data-on-text="&nbsp;" data-off-text="&nbsp;" checked="unchecked"></div>
            </div> -->               
        </div>
    </div>

    <div class="demo-body">
        <div class="option-title" data-localize="switcher_header_color">Header Colors</div>
        <ul id="demo-header-color" class="demo-color-list">
            <li><span class="demo-cyan"></span></li>
            <li><span class="demo-light-blue"></span></li>
            <li><span class="demo-blue"></span></li>
            <li><span class="demo-indigo"></span></li>
            <li><span class="demo-deep-purple"></span></li> 
            <li><span class="demo-purple"></span></li> 
            <li><span class="demo-pink"></span></li> 
            <li><span class="demo-red"></span></li>
            <li><span class="demo-teal"></span></li>
            <li><span class="demo-green"></span></li>
            <li><span class="demo-light-green"></span></li>
            <li><span class="demo-lime"></span></li>
            <li><span class="demo-yellow"></span></li>
            <li><span class="demo-amber"></span></li>
            <li><span class="demo-orange"></span></li>               
            <li><span class="demo-deep-orange"></span></li>
            <li><span class="demo-bluegray"></span></li>
            <li><span class="demo-gray"></span></li> 
            <li><span class="demo-brown"></span></li>
        </ul>
    </div>

    <div class="demo-body">
        <div class="option-title" data-localize="switcher_sidebar_color">Sidebar Colors</div>
        <ul id="demo-sidebar-color" class="demo-color-list">
            <li><span class="demo-cyan"></span></li>
            <li><span class="demo-light-blue"></span></li>
            <li><span class="demo-blue"></span></li>
            <li><span class="demo-indigo"></span></li>
            <li><span class="demo-deep-purple"></span></li> 
            <li><span class="demo-purple"></span></li> 
            <li><span class="demo-pink"></span></li> 
            <li><span class="demo-red"></span></li>
            <li><span class="demo-teal"></span></li>
            <li><span class="demo-green"></span></li>
            <li><span class="demo-light-green"></span></li>
            <li><span class="demo-lime"></span></li>
            <li><span class="demo-yellow"></span></li>
            <li><span class="demo-amber"></span></li>
            <li><span class="demo-orange"></span></li>               
            <li><span class="demo-deep-orange"></span></li>
            <li><span class="demo-bluegray"></span></li>
            <li><span class="demo-gray"></span></li> 
            <li><span class="demo-brown"></span></li>
        </ul>
    </div>

</div>
<!-- /Switcher -->
<!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->


<!-- End loading site level scripts -->

<!-- Load page level scripts-->


<!-- End loading page level scripts-->
<script type="text/javascript" src="../theme-outline/assets/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->
<script type="text/javascript" src="../theme-outline/assets/js/enquire.min.js"></script> 									<!-- Load Enquire -->

<script type="text/javascript" src="../theme-outline/assets/plugins/velocityjs/velocity.min.js"></script>					<!-- Load Velocity for Animated Content -->
<script type="text/javascript" src="../theme-outline/assets/plugins/velocityjs/velocity.ui.min.js"></script>

<script type="text/javascript" src="../theme-outline/assets/plugins/wijets/wijets.js"></script>     						<!-- Wijet -->

<script type="text/javascript" src="../theme-outline/assets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
<script type="text/javascript" src="../theme-outline/assets/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->

<script type="text/javascript" src="../theme-outline/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="../theme-outline/assets/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->

<script type="text/javascript" src="../theme-outline/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->
<script type="text/javascript" src="../theme-outline/assets/plugins/jquery-mousewheel/jquery.mousewheel.min.js"></script> <!-- Mousewheel support needed for Mapael -->

<script type="text/javascript" src="../theme-outline/assets/plugins/sparklines/jquery.sparklines.min.js"></script> 			 <!-- Sparkline -->

<script type="text/javascript" src="../theme-outline/assets/js/application.js"></script>
<script type="text/javascript" src="../theme-outline/assets/demo/demo.js"></script>
<script type="text/javascript" src="../theme-outline/assets/demo/demo-switcher.js"></script>
<script type="text/javascript" src="../js/footer_script.js"></script>
<script type="text/javascript" src="../theme-avenger/assets/plugins/localisation/js/jquery.localize.min.js"></script>
<script type="text/javascript" src="../theme-avenger/assets/plugins/localisation/js/demo-localisation.js"></script>

</body>
</html>