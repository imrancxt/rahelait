
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Outline Admin Theme</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="Outline Admin Theme">
        <meta name="author" content="KaijuThemes">

        <link type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600' rel='stylesheet'>

        <link type="text/css" href="../theme-outline/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">        <!-- Font Awesome -->
        <link type="text/css" href="../theme-outline/assets/fonts/themify-icons/themify-icons.css" rel="stylesheet">              <!-- Themify Icons -->
        <link type="text/css" href="../theme-outline/assets/fonts/weather-icons/css/weather-icons.min.css" rel="stylesheet">      <!-- Weather Icons -->

        <link rel="stylesheet" href="../theme-outline/assets/css/styles-alternative.css" id="theme">             <!-- Default CSS: Altenate Style -->
        <link rel="prefetch alternate stylesheet" href="../theme-outline/assets/css/styles.css">                 <!-- Prefetched Secondary Style -->

        <link type="text/css" href="../theme-outline/assets/plugins/codeprettifier/prettify.css" rel="stylesheet">                <!-- Code Prettifier -->
        <link type="text/css" href="../theme-outline/assets/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">              <!-- iCheck -->

        <!--[if lt IE 10]>
            <script type="text/javascript" src="../theme-outline/assets/js/media.match.min.js"></script>
            <script type="text/javascript" src="../theme-outline/assets/js/respond.min.js"></script>
            <script type="text/javascript" src="../theme-outline/assets/js/placeholder.min.js"></script>
        <![endif]-->
        <!-- The following CSS are included as plugins and can be removed if unused-->
        <script type="text/javascript" src="../theme-outline/assets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
        <script type="text/javascript" src="../theme-outline/assets/js/jqueryui-1.10.3.min.js"></script> 							<!-- Load jQueryUI -->
        <script src="../js/admin.js"></script>
        <script src="../js/customize.js"></script>
        <script src="../js/print.js"></script>

    </head>

    <body class="infobar-overlay sidebar-hideon-collpase sidebar-scroll animated-content">

        <div class="extrabar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">
                        <div class="info-tile info-tile-alt tile-warning">
                            <div class="tile-icon"><i class="ti ti-eye"></i></div>
                            <div class="tile-heading"><span>Page Views</span></div>
                            <div class="tile-body"><span>1,600</span></div>
                            <div class="tile-footer"><span class="text-danger">-7.6% <i class="ti ti-arrow-down"></i></span></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-tile info-tile-alt tile-success">
                            <div class="tile-icon"><i class="ti ti-thumb-up"></i></div>
                            <div class="tile-heading"><span>Likes</span></div>
                            <div class="tile-body"><span>345</span></div>
                            <div class="tile-footer"><span class="text-success">+15.4% <i class="ti ti-arrow-up"></i></span></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-tile info-tile-alt tile-danger">
                            <div class="tile-icon"><i class="ti ti-check-box"></i></div>
                            <div class="tile-heading"><span>Bugs Fixed</span></div>
                            <div class="tile-body"><span>21</span></div>
                            <div class="tile-footer"><span class="text-success">+10.4% <i class="ti ti-arrow-up"></i></span></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-tile info-tile-alt tile-info">
                            <div class="tile-icon"><i class="ti ti-user"></i></div>
                            <div class="tile-heading"><span>New Members</span></div>
                            <div class="tile-body"><span>124</span></div>
                            <div class="tile-footer"><span class="text-danger">-25.4% <i class="ti ti-arrow-down"></i></span></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-tile info-tile-alt tile-cyan">
                            <div class="tile-icon"><i class="ti ti-gift"></i></div>
                            <div class="tile-heading"><span>Gifts</span></div>
                            <div class="tile-body"><span>16</span></div>
                            <div class="tile-footer"><span class="text-danger">-7.6% <i class="ti ti-arrow-down"></i></span></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-tile info-tile-alt tile-indigo">
                            <div class="tile-icon"><i class="ti ti-menu-alt"></i></div>
                            <div class="tile-heading"><span>Tasks</span></div>
                            <div class="tile-body"><span>17</span></div>
                            <div class="tile-footer"><span class="text-danger">-26.4% <i class="ti ti-arrow-down"></i></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="extrabar-underlay"></div>

        <header id="topnav" class="navbar navbar-cyan navbar-fixed-top">

            <div class="logo-area">
                <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
                    <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                        <span class="icon-bg">
                            <i class="ti ti-shift-left"></i>
                        </span>
                    </a>
                </span>

                <a class="navbar-brand" href="index.html">Outline</a>

                <div class="toolbar-icon-bg hidden-xs" id="toolbar-search">
                    <div class="input-icon">
                        <i class="ti ti-search search"></i>
                        <input type="text" placeholder="Type to search..." class="form-control" tabindex="1" id="search">
                        <i class="ti ti-close remove"></i>
                    </div>
                </div>

            </div><!-- logo-area -->



            <div class="yamm navbar-left navbar-collapse collapse in">
                <ul class="nav navbar-nav">

                    <li class="dropdown" id="widget-classicmenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-localize="backup_database">Backup Database</span><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="http://localhost/phpmyadmin/#PMAURL-3:db_export.php?db=mess&table=&server=1&target=&token=3f5bdc4975ccdf009e6572c87188a69e">
                                    <i class="fa fa-fw fa-lock"></i><span data-localize="export_database">Export Database</span></a>
                            </li>
                            <li>
                                <a href="http://localhost/phpmyadmin/db_export.php?db=mess&table=&server=1&target=&token=3f5bdc4975ccdf009e6572c87188a69e#PMAURL-1:db_import.php?db=mess&table=&server=1&target=&token=4b812e06a5eec956e69985dea8203ea1" attr="_blank">
                                    <i class="fa fa-fw fa-unlock"></i><span data-localize="import_database">Import Database</span></a>
                            </li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-localize="change_theme">Change Theme</span><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a class="change_theme" theme="avenger">
                                    <i class="fa fa-fw fa-weibo"></i><span data-localize="theme_avenger">Avenger</span></a>
                            </li>
                            <li>
                                <a class="change_theme" theme="outline">
                                    <i class="fa fa-fw fa-circle-o"></i><span data-localize="theme_outline">Outline</span></a>
                            </li>

                        </ul>
                    </li>

                </ul>
            </div>



            <ul class="nav navbar-nav toolbar pull-right">


                <li class="dropdown toolbar-icon-bg">
                    <a href="#" id="navbar-links-toggle" data-toggle="collapse" data-target="header>.navbar-collapse">
                        <span class="icon-bg">
                            <i class="ti ti-more"></i>
                        </span>
                    </a>
                </li>

                <li class="toolbar-icon-bg" id="trigger-toolbar-search">
                    <a href="#"><span class="icon-bg"><i class="ti ti-search"></i></span></a>
                </li>

                <li class="dropdown toolbar-icon-bg">
                    <a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-globe"></i></span></a>
                    <ul class="dropdown-menu userinfo arrow">
                        <li class="en"><a href="#" class="select_local" lang="en"><span class="pull-left">English</span><i class="pull-right fa fa-cog"></i></a></li>
                        <li class="bn"><a href="#" class="select_local" lang="bn"><span class="pull-left">বাংলা</span><i class="pull-right fa fa-cog"></i></a></li>
                    </ul>
                </li>
                <li class="toolbar-icon-bg hidden-xs" id="trigger-extrabar">
                    <a href="#"><span class="icon-bg"><i class="ti ti-view-grid"></i></span></a>
                </li>

                <li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
                    <a href="#" class="toggle-fullscreen"><span class="icon-bg"><i class="ti ti-fullscreen"></i></span></i></a>
                </li>

            

                <li class="dropdown toolbar-icon-bg">
                    <a href="#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="ti ti-bell"></i></span><span class="badge badge-deeporange">2</span></a>
                    <div class="dropdown-menu notifications arrow">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="scroll-pane">
                            <ul class="media-list scroll-content">
                                <li class="media notification-success">
                                    <a href="#">
                                        <div class="media-left">
                                            <span class="notification-icon"><i class="ti ti-pencil"></i></span>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="notification-heading">Profile page has been updated</h4>
                                            <span class="notification-time">12 mins ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-info">
                                    <a href="#">
                                        <div class="media-left">
                                            <span class="notification-icon"><i class="ti ti-check"></i></span>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="notification-heading">Updates pushed successfully</h4>
                                            <span class="notification-time">35 mins ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-cyan">
                                    <a href="#">
                                        <div class="media-left">
                                            <span class="notification-icon"><i class="ti ti-user"></i></span>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="notification-heading">New users requested to join</h4>
                                            <span class="notification-time">11 hours ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-indigo">
                                    <a href="#">
                                        <div class="media-left">
                                            <span class="notification-icon"><i class="ti ti-shopping-cart"></i></span>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="notification-heading">More orders are pending</h4>
                                            <span class="notification-time">2 days ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-danger">
                                    <a href="#">
                                        <div class="media-left">
                                            <span class="notification-icon"><i class="ti ti-arrow-up"></i></span>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="notification-heading">Pending membership approval</h4>
                                            <span class="notification-time">4 days ago</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="#">See all notifications</a>
                        </div>
                    </div>
                </li>
                <li class="dropdown toolbar-icon-bg">
                    <a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-gamepad"></i></span></a>
                    <ul class="dropdown-menu userinfo arrow">
                        <li>
                            <a href="javascript:have_fun(1)">
                                <i class="fa fa-fw fa-smile-o" data-locali></i><span data-localize="nav_fun_snake">Snake</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:have_fun(6)"><i class="fa fa-fw fa-smile-o"></i><span data-localize="nav_fun_pyramid">Pyramid</span></a>
                        </li>
                        <li>
                            <a href="javascript:have_fun(7)"><i class="fa fa-fw fa-smile-o"></i><span data-localize="nav_fun_bricks">Break-Bricks</span></a>
                        </li>
                        <li>
                            <a href="javascript:have_fun(8)"><i class="fa fa-fw fa-smile-o"></i><span data-localize="nav_fun_ping_pong">Ping-Pong</span> </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown toolbar-icon-bg">
                    <a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-user"></i></span></a>
                    <ul class="dropdown-menu userinfo arrow">
                        <li><a href="#"><span class="pull-left" data-localize="nav_profile">Profile</span></a></li>
                        <li><a id="generate_fee" style="cursor:pointer"><span class="pull-left" data-localize="nav_generate_monthly_fee">Generate Monthly Fee</span> <i class="pull-right fa fa-cog"></i></a></li>
                        <li><a href="../login/signout"><span class="pull-left" data-localize="nav_sign_out">Sign Out</span> <i class="pull-right fa fa-sign-out"></i></a></li>
                    </ul>
                </li>

            </ul>

        </header>

        <div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-cyan">
                    <div class="static-sidebar">
                        <div class="sidebar">
                            <div class="widget">
                                <div class="widget-body">
                                    <div class="userinfo">
                                        <div class="avatar">
                                            <img src="../theme-outline/assets/demo/avatar/avatar_13.png" class="img-responsive img-circle"> 
                                        </div>
                                        <div class="info">
                                            <span class="welcome-text" data-localize="nav_welcome">Welcome</span><br>
                                            <a href="#" class="name">Admin</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget stay-on-collapse" id="widget-sidebar">
                                <nav class="widget-body">
                                    <ul class="acc-menu">
                                        <li class="nav-separator" data-localize="nav_explore">Explore</li>
                                        <li><a href="../account/dashboard"><i class="fa fa-home"></i><span data-localize="nav_dashboard">Dashboard</span></a></li>

                                        <li><a href="javascript:;"><i class="fa fa-columns"></i><span data-localize="nav_decoration">Decoration</span><span class="badge badge-primary">4</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/addroom" data-localize="nav_add_room">Add Room</a>
                                                </li>

                                                <li>
                                                    <a href="../admin/viewroomlist" data-localize="nav_view_room_list">View Room List</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/viewseatlist" data-localize="nav_view_seat_list">View Seat List</a>
                                                </li>
                                                <li>
                                                    <a href="../admin2/leaving_seat" data-localize="nav_leaving_seat_list">Leaving Seat List</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-flask"></i><span data-localize="nav_man">Border</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/current_border" data-localize="nav_current_man">Current Border</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/previous_border" data-localize="nav_previous_man">Previous Border</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-cogs"></i><span data-localize="nav_booker">Booker</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/current_booker" data-localize="nav_current_booker">Current Booker</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/previous_booker" data-localize="nav_previous_booker">Previous Booker</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-separator" data-localize="nav_account">Accounts</li>
                                        <li><a href="javascript:;"><i class="fa fa-random"></i><span data-localize="nav_collection">Collection</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin2/monthlycollection" data-localize="nav_monthly_collection">
                                                        Monthly Collection</a>
                                                </li>
                                                <li>
                                                    <a href="../admin2/dailycollection" data-localize="nav_daily_collection">
                                                        Daily Collection</a>
                                                </li>
                                                <li>
                                                    <a href="../admin2/security_money_collection" data-localize="nav_security_money_collection">Security Money Collection</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-pencil"></i><span data-localize="nav_cost">Cost</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin2/add_cost_description" data-localize="nav_add_cost_description">Add Cost Description</a>
                                                </li>
                                                <!-- <li>
                                                     <a href="../admin2/add_cost_type">Add Cost Type</a>
                                                 </li>-->
                                                <li>
                                                    <a href="../admin2/add_cost" data-localize="nav_add_cost">Add Cost</a>
                                                </li>
                                                <li>
                                                    <a href="../admin2/dailycost" data-localize="nav_view_daily_cost">View Daily Cost</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-table"></i><span data-localize="nav_balance_sheet">Balance Sheet</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin2/balance_sheet1" data-localize="nav_balance_sheet1">Balance Sheet1</a>
                                                </li>
                                                <li>
                                                    <a href="../admin2/balance_sheet2" data-localize="nav_balance_sheet2">Balance Sheet2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-adjust"></i><span data-localize="nav_acc_head">Account Head</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../account/add_account_head" data-localize="nav_add_acc_head">Add Account Head</a>
                                                </li>
                                                <li>
                                                    <a href="../account/post_to_account" data-localize="nav_post_to_account">All Account Ledger</a>
                                                </li>

                                            </ul>
                                        </li>

                                        <li>
                                            <a href="../admin2/banking"><i class="fa fa-fw fa-yen"></i><span data-localize="nav_banking">Banking</span></a>
                                        </li>

                                        <li>
                                            <a href="../admin2/income"><i class="fa fa-fw fa-yen"></i> <span data-localize="nav_income">Income</span></a>
                                        </li>

                                        <li>
                                            <a href="../account/final_report"><i class="fa fa-fw fa-repeat"></i> <span data-localize="nav_final_report">Final Report</span></a>
                                        </li>

                                        <li>
                                            <a href="../admin2/monthly_security_deposite">
                                                <i class="fa fa-fw fa-yen"></i><span data-localize="navsecurity_money_deposit">Security Money Deposit</span></a>
                                        </li> 



                                    </ul>
                                </nav>
                            </div>


                            <!-- <div class="widget" id="widget-sparklines">
                                <div class="widget-heading">Sparklines</div>
                                <div class="widget-body p-md">
                                    <div class="clearfix pt-n pb-sm">
                                        <div class="pull-left"><h5 class="m-n small" style="font-weight: 400;">Total Visitors</h5></div>
                                        <div class="pull-right"><h5 class="m-n">1,785</h5></div>
                                    </div>
                                    <div class="spark-totalvisitors"></div>
                                    <div class="clearfix pt-lg pb-sm">
                                        <div class="pull-left"><h5 class="m-n small" style="font-weight: 400;">Total Earnings</h5></div>
                                        <div class="pull-right"><h5 class="m-n">$7,585</h5></div>
                                    </div>
                                    <div class="spark-totalearnings"></div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content" id="core_container">
                        <div class="page-content">

                            <div class="container-fluid">