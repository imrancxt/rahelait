<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin Panel</title>

        <!-- Bootstrap -->
        <link href="../theme-gentelella/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../metisMenu/dist/metisMenu.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../theme-gentelella/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../theme-gentelella/css/custom.css" rel="stylesheet">

        <!-- jQuery -->
        <script src="../theme-gentelella/js/jquery/dist/jquery.min.js"></script>
        <script src="../js/admin.js"></script>
        <script src="../js/customize.js"></script>
        <script src="../js/print.js"></script>
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">DPOS Software</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">


                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Change Theme
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#" class="change_theme" theme="avenger">
                                    <i class="fa fa-fw fa-weibo"></i><span data-localizem="theme_avenger">Avenger</span></a>
                            </li>
                            <li>
                                <a href="#" class="change_theme" theme="gentelella">
                                    <i class="fa fa-fw fa-circle-o"></i><span data-localize="theme_outline">Gentelella</span></a>
                            </li>
                        </ul>

                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Back-up Database
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="http://localhost/phpmyadmin/server_export.php?db=&table=&server=1&target=&token=448be83169a9a4315722c799c2bfbde1#PMAURL-0:server_export.php?db=&table=&server=1&target=&token=448be83169a9a4315722c799c2bfbde1">
                                    <i class="fa fa-fw fa-lock"></i><span data-localize="export_database">Export Database</span></a>
                            </li>
                            <li>
                                <a href="http://localhost/phpmyadmin/server_export.php?db=&table=&server=1&target=&token=448be83169a9a4315722c799c2bfbde1#PMAURL-1:server_import.php?db=&table=&server=1&target=&token=772c14893158307e7b4962807017cd59" attr="_blank">
                                    <i class="fa fa-fw fa-unlock"></i><span data-localize="import_database">Import Database</span></a>
                            </li>
                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <?php
                            if ($_SESSION['type'] == "Admin") {
                                $url = "../admin/profile";
                                echo"<li><a href='$url'><i class='fa fa-fw fa-user'></i> Profile</a></li><li class='divider'></li>";
                                echo"<li><a href='../admin2/user_management'><i class='fa fa-fw fa-user'></i> User Management</a></li><li class='divider'></li>";
                            }
                            ?>
                            <li>
                                <a href="<?php
                            if ($_SESSION['type'] == "Admin") {
                                $log_out = "../login/admin_log_out";
                            } else {
                                $log_out = "../login/user_log_out";
                            } echo "$log_out"
                            ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input id="search" type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- /input-group -->
                            </li>


                            <li><a href="../admin/dashboard"><i class="fa fa-home"></i> Dashboard</a></li>

                            <li><a href="#"><i class="fa fa-edit"></i> Supplier <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="../admin/addnewsupplier">Add New Supplier</a>
                                    </li>
                                    <li>
                                        <a href="../admin/currentsupplier">Current Supplier</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-desktop"></i> Customer <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">

                                    <li>
                                        <a href="../admin/customer_type">Customer Type</a>
                                    </li>
                                    <li>
                                        <a href="../admin/area">Area</a>
                                    </li>
                                    <li>
                                        <a href="../admin/addnewcustomer">Add New Customer</a>
                                    </li>
                                    <li>
                                        <a href="../admin/currentcustomer">Current Customer</a>
                                    </li>


                                </ul>
                            </li>

                            <li><a href="#"><i class="fa fa-table"></i> Stock <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="../admin/add_new_product_to_stock">Add New Product</a>
                                    </li>
                                    <li>
                                        <a href="../admin/product_in_stock">Current Stock</a>
                                    </li>
                                    <li>
                                        <a href="../admin/return_product">Add Return Product</a>
                                    </li>
                                    <li>
                                        <a href="../admin/return_product_list">All Returned Product</a>
                                    </li>
                                    <li>
                                        <a href="../admin/branch">Branch</a>
                                    </li>
                                    <li>
                                        <a href="../admin/branch_wise_stock">Branch Wise Stock </a>
                                    </li>
                                    <li>
                                        <a href="../admin2/stock_register">Stock Register</a>
                                    </li>
                                </ul>
                            </li>

                            <li><a href="#"><i class="fa fa-bug"></i> All Invoice <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="../admin/all_customer_receipt">Customer Receipt</a>
                                    </li>
                                    <li>
                                        <a href="../admin/all_supplier_receipt">Supplier Receipt</a>
                                    </li>
                                </ul>
                            </li>

                            <li><a href="#"><i class="fa fa-windows"></i> Products <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="../admin/producttype">Product Type</a>
                                    </li>
                                    <li>
                                        <a href="../admin/brand">Brand</a>
                                    </li>
                                    <li>
                                        <a href="../admin/measuring_scale">Measuring Scale</a>
                                    </li>
                                    <li>
                                        <a href="../admin/productname">Product Name</a>
                                    </li>
                                </ul>
                            </li>

                            <li><a href="#"><i class="fa fa-sitemap"></i> Reports <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="../admin/sales_report">Sales report1</a>
                                    </li>
                                    <li>
                                        <a href="../admin/sales_report2">Sales report2</a>
                                    </li>
                                    <li class="divider">

                                    </li>
                                    <li>
                                        <a href="../admin/customer_report">Customer report</a>
                                    </li>


                                </ul>
                            </li>
                            <li>
                                <a href="../admin/personal_dr_cr"><i class="fa fa-fw fa-phone"></i>Personal Debit-Credit</a>
                            </li>
                            <li class="ctive">
                                <a href="../admin/cashbook"><i class="fa fa-fw fa-volume-up"></i> Cash Book</a>
                            </li>


                            <li><a href="#"><i class="fa fa-weibo"></i> Account Head/Subhead <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="../account/">Account Head</a>
                                    </li>
                                    <li>
                                        <a href="../account/account_subhead">Account Sub-head</a>
                                    </li>

                                </ul>
                            </li>
                            <li>
                                <a href="../account/add_head_data"><i class="fa fa-fw fa-desktop"></i> Account head debit-credit</a>
                            </li>
                            
                            
                            <li><a href="#"><i class="fa fa-sitemap"></i> General Ledger<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="../account/acc_head_general_ledger">Account Head Ledger</a>
                                    </li>
                                    <li>
                                        <a href="../account/acc_subhead_general_ledger">Account Sub-head Ledger</a>
                                    </li>

                                </ul>
                            </li>


                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">