</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>

<!-- Bootstrap -->
<script src="../theme-gentelella/js/bootstrap.min.js"></script>
<!-- FastClick -->


<!-- Custom Theme Scripts -->
<script src="../theme-gentelella/js/custom.js"></script>
<script src="../theme-gentelella/metisMenu/dist/metisMenu.min.js"></script>
<script type="text/javascript" src="../js/footer_script.js"></script>
<style>
    .btn,.panel{
        border-radius: 0px !important;
    }
</style>
<script>
    $(document).ready(function(){
        $(".panel-primary").attr("class","panel panel-default");
        $(".panel-success").attr("class","panel panel-default");
        $(".panel-info").attr("class","panel panel-default");
        $(".panel-inverse").attr("class","panel panel-default");
        $(".change_theme").click(function(){
            theme=$(this).attr("theme");
            page="admin2/theme/"+theme;
            
            window.localStorage.clear();
            page_refresh(page);
        })
    })
</script>
</body>
</html>