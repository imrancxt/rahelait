</div> <!-- .container-fluid -->
</div> <!-- #page-content -->
</div>
<footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;"> &copy; 2015 rahelait</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
    </div>
</footer>
</div>
</div>
</div>




<!-- Switcher -->
<div class="demo-options">
    <div class="demo-options-icon"><i class="fa fa-spin fa-fw fa-smile-o"></i></div>
    <div class="demo-heading" data-localize="switcher_demo_settings">Demo Settings</div>

    <div class="demo-body">
        <div class="tabular">
            <div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_fixed_header">Fixed Header</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" checked data-size="mini" data-on-color="success" data-off-color="default" name="demo-fixedheader" data-on-text="I" data-off-text="O"></div>
            </div>
            <div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_boxed_layout">Boxed Layout</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-boxedlayout" data-on-text="I" data-off-text="O"></div>
            </div>
            <div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_collapse_leftbar">Collapse Leftbar</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-collapseleftbar" data-on-text="I" data-off-text="O"></div>
            </div>
            <!--<div class="tabular-row">
                <div class="tabular-cell" data-localize="switcher_collapse_rightbar">Collapse Rightbar</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" checked data-size="mini" data-on-color="success" data-off-color="default" name="demo-collapserightbar" data-on-text="I" data-off-text="O"></div>
            </div>-->
            <div class="tabular-row hide" id="demo-horizicon">
                <div class="tabular-cell" data-localize="switcher_horizontal_icons">Horizontal Icons</div>
                <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" checked data-size="mini" data-on-color="primary" data-off-color="warning" data-on-text="S" data-off-text="L" name="demo-horizicons" ></div>
            </div>
        </div>

    </div>

    <div class="demo-body">
        <div class="option-title" data-localize="switcher_header_color">Header Colors</div>
        <ul id="demo-header-color" class="demo-color-list">
            <li><span class="demo-white"></span></li>
            <li><span class="demo-black"></span></li>
            <li><span class="demo-midnightblue"></span></li>
            <li><span class="demo-primary"></span></li>
            <li><span class="demo-info"></span></li>
            <li><span class="demo-alizarin"></span></li>
            <li><span class="demo-green"></span></li>
            <li><span class="demo-violet"></span></li>                
            <li><span class="demo-indigo"></span></li> 
        </ul>
    </div>

    <div class="demo-body">
        <div class="option-title" data-localize="switcher_sidebar_color">Sidebar Colors</div>
        <ul id="demo-sidebar-color" class="demo-color-list">
            <li><span class="demo-white"></span></li>
            <li><span class="demo-black"></span></li>
            <li><span class="demo-midnightblue"></span></li>
            <li><span class="demo-primary"></span></li>
            <li><span class="demo-info"></span></li>
            <li><span class="demo-alizarin"></span></li>
            <li><span class="demo-green"></span></li>
            <li><span class="demo-violet"></span></li>                
            <li><span class="demo-indigo"></span></li> 
        </ul>
    </div>

    <div class="demo-body hide" id="demo-boxes">
        <div class="option-title" data-localize="switcher_boxed_layout_options">Boxed Layout Options</div>
        <ul id="demo-boxed-bg" class="demo-color-list">
            <li><span class="pattern-brickwall"></span></li>
            <li><span class="pattern-dark-stripes"></span></li>
            <li><span class="pattern-rockywall"></span></li>
            <li><span class="pattern-subtle-carbon"></span></li>
            <li><span class="pattern-tweed"></span></li>
            <li><span class="pattern-vertical-cloth"></span></li>
            <li><span class="pattern-grey_wash_wall"></span></li>
            <li><span class="pattern-pw_maze_black"></span></li>
            <li><span class="patther-wild_oliva"></span></li>
            <li><span class="pattern-stressed_linen"></span></li>
            <li><span class="pattern-sos"></span></li>
        </ul>
    </div>

</div>
<!-- /Switcher -->
<!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->



<!-- End loading site level scripts -->

<!-- Load page level scripts-->


<!-- End loading page level scripts-->
<script type="text/javascript" src="../theme-avenger/assets/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->


<script type="text/javascript" src="../theme-avenger/assets/plugins/easypiechart/jquery.easypiechart.js"></script> 		<!-- EasyPieChart-->
<script type="text/javascript" src="../theme-avenger/assets/plugins/sparklines/jquery.sparklines.min.js"></script>  		<!-- Sparkline -->
<script type="text/javascript" src="../theme-avenger/assets/plugins/jstree/dist/jstree.min.js"></script>  				<!-- jsTree -->

<script type="text/javascript" src="../theme-avenger/assets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
<script type="text/javascript" src="../theme-avenger/assets/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->

<script type="text/javascript" src="../theme-avenger/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="../theme-avenger/assets/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->

<script type="text/javascript" src="../theme-avenger/assets/js/enquire.min.js"></script> 									<!-- Enquire for Responsiveness -->

<script type="text/javascript" src="../theme-avenger/assets/plugins/bootbox/bootbox.js"></script>							<!-- Bootbox -->

<script type="text/javascript" src="../theme-avenger/assets/plugins/simpleWeather/jquery.simpleWeather.min.js"></script> <!-- Weather plugin-->

<script type="text/javascript" src="../theme-avenger/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

<script type="text/javascript" src="../theme-avenger/assets/plugins/jquery-mousewheel/jquery.mousewheel.min.js"></script> 	<!-- Mousewheel support needed for jScrollPane -->

<script type="text/javascript" src="../theme-avenger/assets/js/application.js"></script>
<script type="text/javascript" src="../theme-avenger/assets/demo/demo.js"></script>
<script type="text/javascript" src="../theme-avenger/assets/demo/demo-switcher.js"></script>

<script type="text/javascript" src="../js/footer_script.js"></script>
<!--<script type="text/javascript" src="../theme-avenger/assets/plugins/localisation/js/jquery.localize.min.js"></script>
<script type="text/javascript" src="../theme-avenger/assets/plugins/localisation/js/demo-localisation.js"></script>-->

<script>
    $(document).ready(function(){
        $(".panel-inverse").attr("class","panel panel-default");
        $(".change_theme").click(function(){
            theme=$(this).attr("theme");
            page="admin2/theme/"+theme;
            
            window.localStorage.clear();
            page_refresh(page);
        })
    })
</script>
<style>
    .btn{
        border-radius: 0px;
    }
</style>

</body>
</html>