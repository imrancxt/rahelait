
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Admin Panel</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="Avenger Admin Theme">
        <meta name="author" content="KaijuThemes">

        <link href='http://fonts.googleapis.com/css?family=RobotoDraft:300,400,400italic,500,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>

        <!--[if lt IE 10]>
            <script type="text/javascript" src="../theme-avenger/assets/js/media.match.min.js"></script>
            <script type="text/javascript" src="../theme-avenger/assets/js/placeholder.min.js"></script>
        <![endif]-->

        <link type="text/css" href="../theme-avenger/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">        <!-- Font Awesome -->
        <link type="text/css" href="../theme-avenger/assets/css/styles.css" rel="stylesheet">                                     <!-- Core CSS with all styles -->

        <link type="text/css" href="../theme-avenger/assets/plugins/jstree/dist/themes/avenger/style.min.css" rel="stylesheet">    <!-- jsTree -->
        <link type="text/css" href="../theme-avenger/assets/plugins/codeprettifier/prettify.css" rel="stylesheet">                <!-- Code Prettifier -->
        <link type="text/css" href="../theme-avenger/assets/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">              <!-- iCheck -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
        <!--[if lt IE 9]>
            <link type="text/css" href="../theme-avenger/assets/css/ie8.css" rel="stylesheet">
            <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
            <script type="text/javascript" src="../theme-avenger/assets/plugins/charts-flot/excanvas.min.js"></script>
            <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The following CSS are included as plugins and can be removed if unused-->



        <script type="text/javascript" src="../theme-avenger/assets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
        <script type="text/javascript" src="../theme-avenger/assets/js/jqueryui-1.9.2.min.js"></script> 							<!-- Load jQueryUI --> 

        <script src="../js/admin.js"></script>
        <script src="../js/customize.js"></script>
        <script src="../js/print.js"></script>


    </head>

    <body class="infobar-offcanvas">

        <div id="headerbar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6 col-sm-2">
                        <a href="#" class="shortcut-tile tile-brown">
                            <div class="tile-body">
                                <div class="pull-left"><i class="fa fa-pencil"></i></div>
                            </div>
                            <div class="tile-footer">
                                Create Post
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-2">
                        <a href="#" class="shortcut-tile tile-grape">
                            <div class="tile-body">
                                <div class="pull-left"><i class="fa fa-group"></i></div>
                                <div class="pull-right"><span class="badge">2</span></div>
                            </div>
                            <div class="tile-footer">
                                Contacts
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-2">
                        <a href="#" class="shortcut-tile tile-primary">
                            <div class="tile-body">
                                <div class="pull-left"><i class="fa fa-envelope-o"></i></div>
                                <div class="pull-right"><span class="badge">10</span></div>
                            </div>
                            <div class="tile-footer">
                                Messages
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-2">
                        <a href="#" class="shortcut-tile tile-inverse">
                            <div class="tile-body">
                                <div class="pull-left"><i class="fa fa-camera"></i></div>
                                <div class="pull-right"><span class="badge">3</span></div>
                            </div>
                            <div class="tile-footer">
                                Gallery
                            </div>
                        </a>
                    </div>

                    <div class="col-xs-6 col-sm-2">
                        <a href="#" class="shortcut-tile tile-midnightblue">
                            <div class="tile-body">
                                <div class="pull-left"><i class="fa fa-cog"></i></div>
                            </div>
                            <div class="tile-footer">
                                Settings
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-2">
                        <a href="#" class="shortcut-tile tile-orange">
                            <div class="tile-body">
                                <div class="pull-left"><i class="fa fa-wrench"></i></div>
                            </div>
                            <div class="tile-footer">
                                Plugins
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <header id="topnav" class="navbar navbar-info navbar-fixed-top clearfix" role="banner">

            <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
                <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
            </span>

            <a class="navbar-brand" href="#">Imran</a>

            <span id="trigger-infobar" class="toolbar-trigger toolbar-icon-bg">
                <a data-toggle="tooltips" data-placement="left" title="Toggle Infobar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
            </span>


            <div class="yamm navbar-left navbar-collapse collapse in">
                <ul class="nav navbar-nav">
                    <!-- <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Megamenu<span class="caret"></span></a>
                         <ul class="dropdown-menu" style="width: 900px;">
                             <li>
                                 <div class="yamm-content container-sm-height">
                                     <div class="row row-sm-height yamm-col-bordered">
                                         <div class="col-sm-3 col-sm-height yamm-col">
 
                                             <h3 class="yamm-category">Sidebar</h3>
                                             <ul class="list-unstyled mb20">
                                                 <li><a href="layout-fixed-sidebars.html">Stretch Sidebars</a></li>
                                                 <li><a href="layout-sidebar-scroll.html">Scroll Sidebar</a></li>
                                                 <li><a href="layout-static-leftbar.html">Static Sidebar</a></li>
                                                 <li><a href="layout-leftbar-widgets.html">Sidebar Widgets</a></li>   
                                             </ul>
 
                                             <h3 class="yamm-category">Infobar</h3>
                                             <ul class="list-unstyled">
                                                 <li><a href="layout-infobar-offcanvas.html">Offcanvas Infobar</a></li>
                                                 <li><a href="layout-infobar-overlay.html">Overlay Infobar</a></li>
                                                 <li><a href="layout-chatbar-overlay.html">Chatbar</a></li>
                                                 <li><a href="layout-rightbar-widgets.html">Infobar Widgets</a></li>   
                                             </ul>
 
                                         </div>
                                         <div class="col-sm-3 col-sm-height yamm-col">
 
                                             <h3 class="yamm-category">Page Content</h3>
                                             <ul class="list-unstyled mb20">
                                                 <li><a href="layout-breadcrumb-top.html">Breadcrumbs on Top</a></li>
                                                 <li><a href="layout-page-tabs.html">Page Tabs</a></li>
                                                 <li><a href="layout-fullheight-panel.html">Full-Height Panel</a></li>
                                                 <li><a href="layout-fullheight-content.html">Full-Height Content</a></li>
                                             </ul>
 
                                             <h3 class="yamm-category">Misc</h3>
                                             <ul class="list-unstyled">
                                                 <li><a href="layout-topnav-options.html">Topnav Options</a></li>
                                                 <li><a href="layout-horizontal-small.html">Horizontal Small</a></li>
                                                 <li><a href="layout-horizontal-large.html">Horizontal Large</a></li>
                                                 <li><a href="layout-boxed.html">Boxed</a></li>
                                             </ul>
 
                                         </div>
                                         <div class="col-sm-3 col-sm-height yamm-col">
 
                                             <h3 class="yamm-category">Analytics</h3>
                                             <ul class="list-unstyled mb20">
                                                 <li><a href="charts-flot.html">Flot</a></li>
                                                 <li><a href="charts-sparklines.html">Sparklines</a></li>
                                                 <li><a href="charts-morris.html">Morris</a></li>
                                                 <li><a href="charts-easypiechart.html">Easy Pie Charts</a></li>
                                             </ul>
 
                                             <h3 class="yamm-category">Components</h3>
                                             <ul class="list-unstyled">
                                                 <li><a href="ui-tiles.html">Tiles</a></li>
                                                 <li><a href="custom-knob.html">jQuery Knob</a></li>
                                                 <li><a href="custom-jqueryui.html">jQuery Slider</a></li>
                                                 <li><a href="custom-ionrange.html">Ion Range Slider</a></li>
                                             </ul>
 
                                         </div>
                                         <div class="col-sm-3 col-sm-height yamm-col">
                                             <h3 class="yamm-category">Rem</h3>
                                             <img src="../theme-avenger/assets/demo/stockphoto/communication_12_carousel.jpg" class="mb20 img-responsive" style="width: 100%;">
                                             <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. totam rem aperiam eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                         </div>
                                     </div>
                                 </div>
                             </li>
                         </ul>
                     </li>-->
                    <li class="dropdown" id="widget-classicmenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-localize="backup_database">Backup Database</span><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="http://localhost/phpmyadmin/#PMAURL-3:db_export.php?db=mess&table=&server=1&target=&token=3f5bdc4975ccdf009e6572c87188a69e">
                                    <i class="fa fa-fw fa-lock"></i><span data-localize="export_database">Export Database</span></a>
                            </li>
                            <li>
                                <a href="http://localhost/phpmyadmin/db_export.php?db=mess&table=&server=1&target=&token=3f5bdc4975ccdf009e6572c87188a69e#PMAURL-1:db_import.php?db=mess&table=&server=1&target=&token=4b812e06a5eec956e69985dea8203ea1" attr="_blank">
                                    <i class="fa fa-fw fa-unlock"></i><span data-localize="import_database">Import Database</span></a>
                            </li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-localize="change_theme">Change Theme</span><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a class="change_theme" theme="avenger">
                                    <i class="fa fa-fw fa-weibo"></i><span data-localize="theme_avenger">Avenger</span></a>
                            </li>
                            <li>
                                <a class="change_theme" theme="gentelella">
                                    <i class="fa fa-fw fa-circle-o"></i><span data-localize="theme_outline">Gentelella</span></a>
                            </li>

                        </ul>
                    </li>
                </ul>
            </div>

            <ul class="nav navbar-nav toolbar pull-right">
                <li class="dropdown toolbar-icon-bg">
                    <a href="#" id="navbar-links-toggle" data-toggle="collapse" data-target="header>.navbar-collapse">
                        <span class="icon-bg">
                            <i class="fa fa-fw fa-ellipsis-h"></i>
                        </span>
                    </a>
                </li>

                <li class="dropdown toolbar-icon-bg demo-search-hidden">
                    <a href="#" class="dropdown-toggle tooltips" data-toggle="dropdown"><span class="icon-bg"><i class="fa fa-fw fa-search"></i></span></a>

                    <div class="dropdown-menu dropdown-alternate arrow search dropdown-menu-form">
                        <!--<div class="dd-header">
                            <span>Search</span>
                            <span><a href="#">Advanced search</a></span>
                        </div>-->
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="" id="search">

                            <span class="input-group-btn">

                                <a class="btn btn-primary" href="#" data-localize="search">Search</a>
                            </span>
                        </div>
                    </div>
                </li>

                <!-- <li class="dropdown" id="sample-dropdown">
                 <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                     <span class="icon"><i class="ion-android-globe"></i></span>
                 </a>
                 <ul class="dropdown-menu">
                     <li><span class="category-heading" data-localize="topnav_localisation">Localisation</span></li>
                     <li class="en"><a href="#" class="select_local" lang="en"><i class="ion-android-create"></i><span>English</span></a></li>
                     <li class="fr"><a href="#" class="select_local" lang="fr"><i class="ion-settings"></i><span>French</span></a></li>
                     <li class="ar"><a href="#" class="select_local" lang="ar"><i class="ion-android-create"></i><span>Arabic</span></a></li>
                     <li class="ch"><a href="#" class="select_local" lang="ch"><i class="ion-android-create"></i><span>Chinese</span></a></li>
                 </ul>
             </li>-->

                <li class="dropdown toolbar-icon-bg">
                    <a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-globe"></i></span></a>
                    <ul class="dropdown-menu userinfo arrow">
                        <li class="en"><a href="#" class="select_local" lang="en"><span class="pull-left">English</span><i class="pull-right fa fa-cog"></i></a></li>
                        <li class="bn"><a href="#" class="select_local" lang="bn"><span class="pull-left">বাংলা</span><i class="pull-right fa fa-cog"></i></a></li>
                    </ul>
                </li>



                <li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
                    <a href="#" class="toggle-fullscreen"><span class="icon-bg"><i class="fa fa-fw fa-arrows-alt"></i></span></i></a>
                </li>





                <!-- <li class="dropdown toolbar-icon-bg">
                     <a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-gamepad"></i></span></a>
                     <ul class="dropdown-menu userinfo arrow">
                         <li>
                             <a href="javascript:have_fun(1)">
                                 <i class="fa fa-fw fa-smile-o" data-locali></i><span data-localize="nav_fun_snake">Snake</span>
                             </a>
                         </li>
                         <li>
                             <a href="javascript:have_fun(6)"><i class="fa fa-fw fa-smile-o"></i><span data-localize="nav_fun_pyramid">Pyramid</span></a>
                         </li>
                         <li>
                             <a href="javascript:have_fun(7)"><i class="fa fa-fw fa-smile-o"></i><span data-localize="nav_fun_bricks">Break-Bricks</span></a>
                         </li>
                         <li>
                             <a href="javascript:have_fun(8)"><i class="fa fa-fw fa-smile-o"></i><span data-localize="nav_fun_ping_pong">Ping-Pong</span> </a>
                         </li>
                     </ul>
                 </li>-->

                <!--<li class="dropdown toolbar-icon-bg hidden-xs">
                    <a href="#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-envelope"></i></span></a>
                    <div class="dropdown-menu dropdown-alternate messages arrow">
                        <div class="dd-header">
                            <span>Messages</span>
                            <span><a href="#">Settings</a></span>
                        </div>

                        <div class="scrollthis scroll-pane">
                            <ul class="scroll-content">
                              
                            </ul>
                        </div>

                        <div class="dd-footer"><a href="#">View all messages</a></div>
                    </div>
                </li>-->



                <li class="dropdown toolbar-icon-bg">
                    <a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-user"></i></span></a>
                    <ul class="dropdown-menu userinfo arrow">
                        <?php
                        if ($_SESSION['type'] == "Admin") {
                            $url = "../admin/profile";
                            echo"<li><a href='$url'><i class='fa fa-fw fa-user'></i> Profile</a></li>";
                            echo"<li><a href='../admin2/user_management'><i class='fa fa-fw fa-user'></i> User Management</a></li>";
                        }
                        ?>
                        <li>
                            <a href="<?php
                        if ($_SESSION['type'] == "Admin") {
                            $log_out = "login/admin_log_out";
                        } else {
                            $log_out = "login/user_log_out";
                        } echo "../$log_out"
                        ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>

            </ul>

        </header>

        <div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-info">
                    <div class="static-sidebar">
                        <div class="sidebar">
                            <div class="widget stay-on-collapse" id="widget-welcomebox">
                                <div class="widget-body welcome-box tabular">
                                    <div class="tabular-row">
                                        <div class="tabular-cell welcome-avatar">
                                            <a href="#"><img src="../theme-avenger/assets/demo/avatar/avatar_02.png" class="avatar"></a>
                                        </div>
                                        <div class="tabular-cell welcome-options">
                                            <span class="welcome-text" data-localize="nav_welcome">Welcome</span>
                                            <a href="#" class="name">Admin</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget stay-on-collapse" id="widget-sidebar">
                                <nav role="navigation" class="widget-body">
                                    <ul class="acc-menu">
                                        <li class="nav-separator" data-localize="nav_explore">Explore</li>
                                        <li><a href="../admin/dashboard"><i class="fa fa-home"></i><span data-localize="nav_dashboard">Dashboard</span></a></li>

                                        <li><a href="javascript:;"><i class="fa fa-columns"></i><span data-localize="nav_decoration">Supplier</span><span class="badge badge-primary">2</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/addnewsupplier">Add New Supplier</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/currentsupplier">Current Supplier</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-flask"></i><span data-localize="nav_man">Customer</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/customer_type">Customer Type</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/area">Area</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/addnewcustomer">Add New Customer</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/currentcustomer">Current Customer</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-cogs"></i><span data-localize="nav_booker">Stock</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/add_new_product_to_stock">Add New Product</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/product_in_stock">Current Stock</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/return_product">Add Return Product</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/return_product_list">All Returned Product</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/branch">Branch</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/branch_wise_stock">Branch Wise Stock </a>
                                                </li>
                                                <li>
                                        <a href="../admin2/stock_register">Stock Register</a>
                                    </li>
                                               
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-cogs"></i><span data-localize="nav_booker">All Invoice Receipt</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/all_customer_receipt">Customer Receipt</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/all_supplier_receipt">Supplier Receipt</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-columns"></i><span data-localize="nav_booker">Products</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/producttype">Product Type</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/brand">Brand</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/measuring_scale">Measuring Scale</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/productname">Product Name</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-separator" data-localize="nav_account">Accounts</li>
                                        <li><a href="javascript:;"><i class="fa fa-random"></i><span data-localize="nav_collection">Reports</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../admin/sales_report">Sales report1</a>
                                                </li>
                                                <li>
                                                    <a href="../admin/sales_report2">Sales report2</a>
                                                </li>
                                                <li class="divider">

                                                </li>
                                                <li>
                                                    <a href="../admin/customer_report">Customer report</a>
                                                </li>
                                                  <li>
                                        <a href="../admin2/stock_history">Stock History</a>
                                    </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="../admin/personal_dr_cr"><i class="fa fa-fw fa-phone"></i>Personal Debit-Credit</a>
                                        </li>
                                        <li class="ctive">
                                            <a href="../admin/cashbook"><i class="fa fa-fw fa-phone"></i>Cash Book</a>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-weibo"></i> Account Head/Subhead <span class="fa arrow"></span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../account/">Account Head</a>
                                                </li>
                                                <li>
                                                    <a href="../account/account_subhead">Account Sub-head</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="../account/add_head_data"><i class="fa fa-fw fa-desktop"></i> Account head debit-credit</a>
                                        </li>

                                        <li><a href="javascript:;"><i class="fa fa-sitemap"></i> General Ledger<span class="fa arrow"></span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a href="../account/acc_head_general_ledger">Account Head Ledger</a>
                                                </li>
                                                <li>
                                                    <a href="../account/acc_subhead_general_ledger">Account Sub-head Ledger</a>
                                                </li>

                                            </ul>
                                        </li>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content" id="core_container">
                        <div class="page-content">
                            <div class="container-fluid">