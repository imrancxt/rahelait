<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of account
 *
 * @author imran.v2v
 */
session_start();
if (isset($_SESSION['type'])) {

    class account extends CI_Controller {

        //put your code here
        public function __construct() {
            parent::__construct();

            $this->load->library('view_page');
            $this->load->model('madmin');
            $this->admin = 1;
        }

        public function index() {
            $query = "SELECT * FROM `account_head`";
            $data = $this->madmin->get_data($query, array('serial', 'type', 'acc_head'), array('serial', 'acc_type', 'acc_head'));
            //print_r($data);
            $this->view_page->admin_page('v_add_account_type', $data);
        }

        public function update_account_head() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $this->db->update('account_head', array('acc_head' => $data['acc_head']), array('serial' => $data['serial']));
            echo"Account Head Updated!";
        }

        public function account_subhead() {
            $query1 = "SELECT * FROM `account_head`";
            $data1 = $this->madmin->get_data($query1, array('acc_head_serial', 'acc_head'), array('serial', 'acc_head'));
            $data2 = array();
            $query2 = "SELECT * FROM `acc_subhead`";
            $data2 = $this->madmin->get_data($query2, array('acc_subhead_serial', 'acc_subhead', 'acc_head2'), array('serial', 'acc_subhead', 'acc_head'));

            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_acc_subhead', $data);
        }

        public function get_subhead() {
            $head = $_GET["head"];
            $query = "SELECT * FROM `acc_subhead` where acc_head='$head'";
            $data = $this->madmin->get_data($query, array('acc_subhead'), array('acc_subhead'));
            if (isset($data)) {
                foreach ($data['acc_subhead'] as $option) {
                    echo"<option>$option</option>";
                }
            }
        }

        public function add_acc_type() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            //echo"n";
            if ($this->db->insert('account_head', $data)) {
                echo "Account Head Added Successfully!";
                //echo"Account Head Added Successfully!";
            } else {
                echo "Failed!";
            }
        }

        public function add_acc_subhead() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if ($this->db->insert('acc_subhead', $data)) {
                //echo"Account Head Added Successfully!";
                echo "New Account Sub-Head Created Successfully";
            } else {
                echo "Error Encountered!";
            }
        }

        public function add_head_data() {
            $query1 = "SELECT distinct(acc_head) FROM `acc_subhead`";
            $data1 = $this->madmin->get_data($query1, array('acc_head'), array('acc_head'));
            $data2 = array();
            if (isset($data1)) {
                $query2 = "SELECT * FROM `acc_subhead` where acc_head='{$data1['acc_head'][0]}'";
                $data2 = $this->madmin->get_data($query2, array('acc_subhead'), array('acc_subhead'));
            }
            $data = array_merge($data1, $data2);
            //$this->load->view('vadmin/ajax_code/v_open_supplementary2', $data);
            $this->view_page->admin_page('v_add_head_data', $data);
            //print_r($data);
        }

        public function others_account_ledger() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('date' => $data["date"], 'acc_head' => $data["acc_head"], 'acc_subhead' => $data["acc_subhead"], $data['drcr'] => $data['amount'],
                'description' => $data['comment'], 'admin' => $this->admin);
            if ($this->db->insert('others_account_ledger', $info)) {
                echo"Transaction Completed!";
            }
        }

        public function acc_head_general_ledger() {

            $query1 = "SELECT distinct(acc_head) FROM `others_account_ledger`";
            $data1 = $this->madmin->get_data($query1, array('acc_head'), array('acc_head'));
            $date = date("Y-m-d");

            $data3 = $this->get_acc_head_ledger_data($data1['acc_head'][0], $date);
            $data = array_merge($data1, $data3);
            $this->view_page->admin_page('v_acc_head_data', $data);
        }

        private function get_acc_head_ledger_data($acc_head, $date) {
            $query2 = "SELECT (sum(cr)-sum(dr)) as last_date_cash_in_hand FROM `others_account_ledger` where date<'$date' and acc_head='$acc_head'";
            $data2['last_date_balance'] = $this->madmin->get_cash_in_hand($query2);

            $query3 = "SELECT * FROM `others_account_ledger` where acc_head='$acc_head' and date='$date'";
            $data3 = $this->madmin->get_data($query3, array('serial', 'description', 'dr', 'cr', 'date'), array('serial', 'description', 'dr', 'cr', 'date'));
            return array_merge($data2, $data3);
        }

        public function filter_acc_head_data() {
            $acc_head = $_GET["acc_head"];
            $date = $_GET["date"];
            $data = $this->get_acc_head_ledger_data($acc_head, $date);
            //print_r($data);
            $this->load->view('ajax_code/v_filter_acc_head_data', $data);
        }

        public function acc_subhead_general_ledger() {
            $date = date("Y-m-d");
            $query1 = "SELECT distinct(acc_head) FROM `others_account_ledger`";
            $data1 = $this->madmin->get_data($query1, array('acc_head'), array('acc_head'));

            $query2 = "SELECT distinct(acc_subhead) FROM `others_account_ledger` where acc_head='{$data1['acc_head'][0]}'";
            $data2 = $this->madmin->get_data($query2, array('acc_subhead'), array("acc_subhead"));

            $data3 = $this->get_acc_subhead_ledger_data($data1["acc_head"][0], $data2["acc_subhead"][0], $date);

            $data = array_merge($data1, $data2, $data3);
            $this->view_page->admin_page('v_acc_subhead_data', $data);
        }

        private function get_acc_subhead_ledger_data($acc_head, $acc_subhead, $date) {
            $query2 = "SELECT (sum(cr)-sum(dr)) as last_date_cash_in_hand FROM `others_account_ledger` where date<'$date' and acc_head='$acc_head' and acc_subhead='$acc_subhead'";
            $data2['last_date_balance'] = $this->madmin->get_cash_in_hand($query2);

            $query3 = "SELECT * FROM `others_account_ledger` where acc_head='$acc_head' and acc_subhead='$acc_subhead' and date='$date'";
            $data3 = $this->madmin->get_data($query3, array('serial', 'description', 'dr', 'cr', 'date'), array('serial', 'description', 'dr', 'cr', 'date'));
            return array_merge($data2, $data3);
        }

        public function filter_acc_subhead_data() {
            $acc_head = $_GET["acc_head"];
            $acc_subhead = $_GET["acc_subhead"];
            $date = $_GET["date"];
            $data = $this->get_acc_subhead_ledger_data($acc_head, $acc_subhead, $date);
            //print_r($data);
            $this->load->view('ajax_code/v_filter_acc_head_data', $data);
        }

    }

}
?>
