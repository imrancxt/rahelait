<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author imran
 */
class login extends CI_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->asset_url="http://localhost/rahelait/";
        session_start();
    }
    public function admin_login(){
        $data['asset_url']=  $this->asset_url;
        $this->load->view('v_admin_login',$data);
    }
    public function user_login(){
        $data['asset_url']=  $this->asset_url;
        $this->load->view('v_user_login',$data);
    
    }
    public function check_admin(){
       $this->input->post(NULL, TRUE);
       $data = $this->input->post();
       $query="SELECT * FROM `user` where type='admin' and  name={$this->db->escape($data['admin'])} and password={$this->db->escape($data['password'])}";
       $rs=$this->db->query($query);
       if($rs->num_rows()>0){
          foreach($rs->result() as $row){
              $_SESSION['type']='Admin';
              $_SESSION['shop_name']=$row->shop_name;
              $_SESSION['name']=$row->name;
              $_SESSION['serial']=$row->serial;
              break;
          }
          echo"success";
       }
       else{
           echo"unknown user";
       }
    }
     public function check_user(){
       $this->input->post(NULL, TRUE);
       $data = $this->input->post();
       $query="SELECT * FROM `user` where type='user' and  name={$this->db->escape($data['user'])} and password={$this->db->escape($data['password'])}";
       $rs=$this->db->query($query);
       if($rs->num_rows()>0){
          foreach($rs->result() as $row){
              $_SESSION['type']='User';
              $_SESSION['shop_name']=$row->shop_name;
              $_SESSION['name']=$row->name;
              $_SESSION['serial']=$row->serial;
              break;
          }
          echo"success";
       }
       else{
           echo"unknown user";
       }
    }
    
    public function admin_log_out(){
        unset($_SESSION['type']);
        unset($_SESSION['shop_name']);
        unset($_SESSION['top_side_color']);
        unset($_SESSION['top_side_color']);
        $url="../login/admin_login";
        header("Location: $url");
    }
     public function user_log_out(){
        unset($_SESSION['type']);
        unset($_SESSION['shop_name']);
        unset($_SESSION['top_side_color']);
        unset($_SESSION['top_side_color']);
        $url="../login/user_login";
        header("Location: $url");
    }
}

?>
