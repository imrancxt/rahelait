<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author imran
 */
session_start();
if (isset($_SESSION['type'])) {

    class admin extends CI_Controller {

//put your code here
        function __construct() {
            parent::__construct();
            $this->load->library('view_page');
            $this->load->model('madmin');
            $this->admin = 1;
            $this->asset_url = "http://localhost/rahelait/";
        }

        public function renew_database() {
            $table = array('personal_dr_cr', 'product_ledger', 'customer_ledger', 'customer_ledger2', 'sold_product_tbl', 'stock_branch_supplier', 'stock_product', 'supplier_ledger', 'supplier_ledger2');
            $query = array();
            for ($i = 0; $i < count($table); $i++) {
                $query[$i] = "TRUNCATE TABLE `$table[$i]`";
            }
            $this->db->trans_start();
            foreach ($query as $query) {
                $this->db->query($query);
            }
            $this->db->trans_complete();
        }

        function index() {
            /* $query = "SELECT * FROM `product_type` where admin=1";
              $data = $this->madmin->get_product_type($query); */
            $this->view_page->admin_page('test', "");
        }

        public function ajax_update_product_type($serial = "") {
            $query = "SELECT * FROM `product_type` where admin=$this->admin && serial=$serial";
            $data = $this->madmin->get_product_type($query);
            $this->load->view('ajax_code/v_ ajax_update_product_type', $data);
        }

        public function update_product_type() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['type_serial'])) {
                $info = array('product_type' => $data['product_type']);
                if ($this->db->update('product_type', $info, array('serial' => $data['type_serial'], 'admin' => $this->admin))) {
                    $this->alert_message("PRODUCT TYPE UPDATED SUCCESSFULY!");
                }
            }
        }

        public function add_new_product_type() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('admin' => $this->admin, 'product_type' => $data['product_type']);
            if ($this->db->insert('product_type', $info)) {
                $this->alert_message("NEW PRODUCT TYPE SUCCESSFULLY INSERTED!");
            }
        }

        private function alert_message($message) {
            echo"<div class='alert alert-warning'>$message
                            <button type ='button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'><span aria-hidden = 'true'>&times;
                                </span>
                            </button>
                        </div>";
        }

        public function ajax_get_product_name($serial = "") {
            $query = "SELECT t1.*,t2.product_type FROM `product_name` as t1,product_type as t2
                where t1.product_type=t2.serial and t1.product_type=$serial and t1.admin=t2.admin and t1.admin=$this->admin";
            $data = $this->madmin->get_product_name($query);
            $this->load->view('ajax_code/v_ajax_get_product_name', $data);
        }

        public function get_product_name_to_edit($serial) {
            $query = "select* from product_name where serial=$serial";
            $data = $this->madmin->get_product_name($query);
            $this->load->view('ajax_code/v_get_product_name_to_edit', $data);
//print_r($data);
        }

        /* public function update_product_name() {
          $this->input->post(NULL, TRUE);
          $data = $this->input->post();
          if (isset($data['serial'])) {
          $info = array('product_name' => $data['product_name'], 'date' => $data['date'], 'description' => $data['description'], 'product_brand' => $data['brand']);
          if ($this->db->update('product_name', $info, array('serial' => $data['serial'], 'admin' => $this->admin))) {
          $this->alert_message("PRODUCT NAME UPDATED!");
          }
          }
          } */

        public function addnewsupplier() {
            $this->view_page->admin_page('v_addnewsupplier', "");
        }

        public function p_add_new_supplier() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('admin' => $this->admin, 'name' => $data['name'], 'phone' => $data['phone'], 'sales_person' => $data['sales_person'], 'address' => $data['address'], 'date' => $data['date']);
            if ($this->db->insert('supplier', $info)) {
                $this->alert_message("NEW SUPPLIER INSERTED SUCCESSFULLY!");
            }
        }

        public function currentsupplier() {
            $query = "SELECT * FROM `supplier` where admin=$this->admin and access='yes'";
            $data = $this->madmin->get_supplier($query);
            $this->view_page->admin_page('v_current_supplier', $data);
        }

        public function addproducttype() {
            $this->view_page->admin_page('v_addproducttype', "");
        }

        public function producttype() {
            $query = "SELECT * FROM `product_type` where admin=$this->admin";
            $data = $this->madmin->get_product_type($query);
            $this->view_page->admin_page('v_producttype', $data);
        }

        public function addnewproductname() {
            $query = "SELECT * FROM `product_type` where admin=$this->admin";
            $data = $this->madmin->get_product_type($query);
            $this->view_page->admin_page('v_addnewproductname', $data);
        }

        public function p_addnewproductname() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['product_type'])) {
                $query = "SELECT * FROM `product_type` where admin=$this->admin && product_type='{$data['product_type']}'";
                $type_serial = $this->madmin->get_product_type($query);
                if (isset($type_serial['type_serial'][0])) {
                    $info = array('admin' => $this->admin, 'product_type' => $type_serial['type_serial'][0], 'product_name' => $data['product_name'], 'product_brand' => $data['brand'],
                        'date' => $data['date'], 'description' => $data['description']);
                    if ($this->db->insert('product_name', $info)) {
                        $this->alert_message("NEW PRODUCT NAME SUCCESSFULY INSERTED!");
                    }
                }
            }
        }

        public function currentproductname() {
            $query = "SELECT t1.*,t2.product_type FROM `product_name` as t1,product_type as t2
                where t1.product_type=t2.serial  and t1.admin=t2.admin";
            $data = $this->madmin->get_product_name($query);
            $this->view_page->admin_page('v_currentproductname', $data);
        }

        public function add_new_product_to_stock() {
            $query1 = "SELECT * FROM `supplier` where admin=$this->admin and access='yes'";
            $data1 = $this->madmin->get_supplier($query1);
            $query2 = "SELECT * FROM `branch_list` where admin=$this->admin";
            $data2 = $this->madmin->get_branch_list($query2);
            $query3 = "SELECT * FROM `product_type` where admin=$this->admin";
            $data3 = $this->madmin->get_product_type($query3);
            $query4 = "SELECT (invoice_no+1) as invoice FROM `supplier_ledger` where admin=$this->admin order by serial desc limit 1 ";
            $data4 = $this->madmin->get_only_one_invoice_no($query4);
            $data = array_merge($data1, $data2, $data3, $data4);

            $this->view_page->admin_page('v_add_new_product_to_stock', $data);
// print_r($data);
        }

        public function get_product_list_to_stock($type = "") {
//$query="SELECT * FROM `product_name` where product_type=(select serial from product_type where admin=$this->admin and product_type='$type')";

            $query1 = "SELECT * FROM `product_name_list` where admin=$this->admin and product_type='$type'";
            $data1 = $this->madmin->get_broduct_name_list($query1);
            $data2 = array();
            if (isset($data1['pn_type'])) {
                $query2 = "SELECT * FROM `product_type` where admin=$this->admin and product_type='{$data1['pn_type'][0]}'";
                $data2 = $this->madmin->get_product_type($query2);
                $data = array_merge($data1, $data2);
                /* if ($data1['product_type'][0] == 'ROD') {
                  $this->load->view('ajax_code/v_stock_rod', $data);
                  } */
                $this->show_product_to_be_stocked($data1['pn_type'][0], $data);
            }
        }

        private function show_product_to_be_stocked($product_type, $data) {
            switch ($product_type) {
                case 'ROD':
                    $this->load->view('ajax_code/v_stock_rod', $data);
                    break;
                case 'CEMENT':
                    $this->load->view('ajax_code/v_stock_cement', $data);
                    break;
                case 'TIN':
                    $this->load->view('ajax_code/v_stock_tin', $data);
//print_r($data);
                    break;
                default:
                    break;
            }
        }

        public function readynewproducttostock() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $insertion_date = date("Y-m-d h:i:s");
            $q_supplier_id = "SELECT serial FROM `supplier` where admin=$this->admin and name='{$data['supplier']}'";
            $supplier_id = $this->madmin->get_only_serial($q_supplier_id, 'supplier_id', 'serial');
            $stock_supplier_info = $this->get_stock_supplier_stock($data['branch_name'], $data['supplier'], $data['date'], $data['invoice_no'], $data['comment'], $data['product_type'], $insertion_date);
            $product_ledger = array();
            $z = 0; //product_ledger_counter;

            if (isset($data['product_name_id']) && isset($supplier_id['supplier_id'][0])) {
                if ($data['product_type'] == "ROD" || $data['product_type'] == "CEMENT") {
                    $total_amount = 0;
                    $scale = $data['scale'];
                    $measurement = $data['measurement'];
                    $stock_product_info = array();
                    for ($i = 0; $i < count($data['product_name_id']); $i++) {
                        $product_name_id = $data["product_name_id"][$i];
                        $quantity = $data["quantity$product_name_id"];
                        $purchase_price = $data["purchase_price$product_name_id"];
                        $sales_price = $data["sales_price$product_name_id"];
                        $amount = $quantity * $purchase_price;
                        $total_amount+=$amount;
                        $product_ledger[$z] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'dr' => $amount, 'cr' => 0, 'date' => $data['date'], 'description' => "STOCK");
                        $stock_product_info[$i] = array('admin' => $this->admin, 'supplier_id' => $supplier_id['supplier_id'][0], 'product_name_id' => $product_name_id, 'invoice_no' => $data['invoice_no'], 'quantity' => $quantity,
                            'purchase_price' => $purchase_price, 'sales_price' => $sales_price, 'total_amount' => $amount, 'insertion_date' => $insertion_date,
                            'product_type' => $data['product_type'], 'scale' => $scale, 'measurement' => $measurement, 'measurement2' => 1, 'branch_id' => $stock_supplier_info['branch_id'], 'date' => $data['date'], 'quantity2' => $quantity);
                        $z++;
                    }
                    $total_paid = $data['total_paid'];
                    $suplier_ledger_info = $this->supplier_ledger($data['invoice_no'], $data['date'], $insertion_date, array("PAID", "BOUGHT PRODUCT"), $total_paid, $total_amount, $supplier_id['supplier_id'][0]);
                    $this->db->trans_start();
                    $this->db->insert('stock_branch_supplier', $stock_supplier_info);
                    $this->db->insert_batch('stock_product', $stock_product_info);
                    $this->db->insert_batch('supplier_ledger', $suplier_ledger_info);
                    $this->db->insert_batch('product_ledger', $product_ledger);
                    echo"INSERTION COMPLETED!";
                    $this->db->trans_complete();
                } else {
                    if ($data['product_type'] == "TIN") {
                        $total_price_amount = 0;
                        for ($i = 0; $i < count($data['product_name_id']); $i++) {
                            $product_name_id = $data["product_name_id"][$i];
                            $scale = explode(" ", $data["scale$product_name_id"]);
                            $new_scale = $scale[count($scale) - 1];
                            $quantity = $data["quantity$product_name_id"]; //in bundle
                            $purchase_price = $data["purchase_price$product_name_id"]; //price per bundle
                            $salse_price = $data["sales_price$product_name_id"]; //price per bundle
                            $measurement = $data["measurement$product_name_id"];
                            if ($new_scale == "bundle") {
                                $price_per_tin = $purchase_price / $measurement;
                                $total_tin = $quantity * $measurement;
                                $amount = $price_per_tin * $total_tin;
                                $sales_price_per_tin = $salse_price / $measurement;
                                $total_price_amount+=$amount;
                                $product_ledger[$z] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'dr' => $amount, 'cr' => 0, 'date' => $data['date'], 'description' => "STOCK");
                                $stock_product_info[$i] = array('admin' => $this->admin, 'supplier_id' => $supplier_id['supplier_id'][0], 'product_name_id' => $product_name_id, 'invoice_no' => $data['invoice_no'], 'quantity' => $total_tin,
                                    'purchase_price' => $price_per_tin, 'sales_price' => $sales_price_per_tin, 'total_amount' => $amount, 'insertion_date' => $insertion_date,
                                    'product_type' => $data['product_type'], 'scale' => $data["scale$product_name_id"], 'measurement' => $measurement, 'measurement2' => 1, 'branch_id' => $stock_supplier_info['branch_id'], 'date' => $data['date'], 'quantity2' => $total_tin);
                                $z++;
                            } else {
                                if ($new_scale == "fiber") {
                                    $purchase_price = $data["purchase_price$product_name_id"];
                                    $amount = $measurement * $purchase_price * $quantity;
                                    $total_price_amount+=$amount;
                                    $product_ledger[$z] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'dr' => $amount, 'cr' => 0, 'date' => $data['date'], 'description' => "STOCK");
                                    $stock_product_info[$i] = array('admin' => $this->admin, 'supplier_id' => $supplier_id['supplier_id'][0], 'product_name_id' => $product_name_id, 'invoice_no' => $data['invoice_no'], 'quantity' => $quantity,
                                        'purchase_price' => $purchase_price, 'sales_price' => $salse_price, 'total_amount' => $amount, 'insertion_date' => $insertion_date,
                                        'product_type' => $data['product_type'], 'scale' => $data["scale$product_name_id"], 'measurement' => $measurement, 'measurement2' => $measurement, 'branch_id' => $stock_supplier_info['branch_id'], 'date' => $data['date'], 'quantity2' => $quantity);
                                    $z++;
                                }
                            }
                        }
                        $total_paid = $data['total_paid'];
                        $suplier_ledger_info = $this->supplier_ledger($data['invoice_no'], $data['date'], $insertion_date, array("PAID", "BOUGHT PRODUCT"), $total_paid, $total_price_amount, $supplier_id['supplier_id'][0]);
                        $this->db->trans_start();
                        $this->db->insert('stock_branch_supplier', $stock_supplier_info);
                        $this->db->insert_batch('stock_product', $stock_product_info);
                        $this->db->insert_batch('supplier_ledger', $suplier_ledger_info);
                        $this->db->insert_batch('product_ledger', $product_ledger);
                        echo"INSERTION COMPLETED!";
                        $this->db->trans_complete();
//print_r($product_ledger);
                    }
                }
            }
        }

        private function supplier_ledger($invoice_no, $date, $insertion_date, $description, $dr, $cr, $supplier_id) {
            $info = array();
            //$info[0] = array('admin' => $this->admin, 'supplier_id' => $supplier_id, 'invoice_no' => $invoice_no, 'description' => $description[0], 'dr' => $dr, 'cr' => 0, 'date' => $date, 'insertion_date' => $insertion_date);
            $info[0] = array('admin' => $this->admin, 'supplier_id' => $supplier_id, 'invoice_no' => $invoice_no, 'description' => $description[1], 'cr' => $cr, 'dr' => 0, 'date' => $date, 'insertion_date' => $insertion_date);
            return $info;
        }

        private function get_stock_supplier_stock($branch_name, $supplier, $date, $invoice_no, $comment, $product_type, $insertion_date) {
            $stock_supplier_info = array();
            $query1 = "SELECT serial FROM `branch_list` where admin=$this->admin and branch='$branch_name'";
            $data1 = $this->madmin->get_only_serial($query1, 'branch_id', 'serial');

            if (isset($data1['branch_id'])) {
                $branch_id = $data1['branch_id'][0];
//$query2 = "SELECT serial FROM `product_type` where admin=$this->admin and product_type='$product_type'";

                /* $data2 = $this->madmin->get_only_serial($query2, 'product_type_id', 'serial');
                  $product_type_id = $data2['product_type_id'][0]; */

                $query3 = "SELECT serial FROM `supplier` where admin=$this->admin and name='$supplier'";
                $data3 = $this->madmin->get_only_serial($query3, 'supplier_id', 'serial');
                $supplier_id = $data3['supplier_id'][0];
                $stock_supplier_info = array('admin' => $this->admin, 'branch_id' => $branch_id, 'product_type' => $product_type, 'supplier_id' => $supplier_id,
                    'invoice_no' => $invoice_no, 'date' => $date, 'insertion_date' => $insertion_date, 'comment' => $comment);
            }
            return $stock_supplier_info;
        }

        public function product_in_stock() {
            $query1 = "SELECT t2.brand,  t2.product_name,t1.product_type,sum(t1.quantity) as quantity,sum(t1.purchase_price) as amount FROM `stock_product` as t1,product_name_list as t2 
                  where t1.product_name_id=t2.serial and t1.admin=t2.admin and t1.admin=1 and t1.quantity>0 group by  t1.product_name_id order by t1.product_type";
            $data1 = $this->madmin->get_stock_product($query1);
            $query2 = "SELECT t1.product_type as o_product_type, sum(t1.quantity) as o_quantity,sum(t1.total_amount) as o_amount FROM 
                 `stock_product` as t1 where t1.quantity>0 group by t1.product_type";
            $data2 = $this->madmin->get_overall_sales_report($query2);
            $data = array_merge($data1, $data2);
//print_r($data2);
            $this->view_page->admin_page('v_stock_product', $data);
        }

        public function filter_stock_product($branch = "", $product_type = "") {
            if ($branch == "ALL" && $product_type == "ALL") {
                $query1 = "SELECT sum(t1.product_name_id) as quantity,t1.product_name_id,t1.scale,t1.product_type,t2.product_name,t2.product_brand FROM `stock_product` as t1,product_name as t2,branch_list as t3
                    where t1.product_name_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin group by t1.product_name_id";
            } else {
                if ($branch == "ALL" && $product_type != "ALL") {
                    $query1 = "SELECT sum(t1.product_name_id) as quantity,t1.product_name_id,t1.scale,t1.product_type,t2.product_name,t2.product_brand FROM `stock_product` as t1,product_name as t2,branch_list as t3
                where t1.product_name_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin and t1.product_type='$product_type' group by t1.product_name_id";
                } else {
                    $branch1 = $this->madmin->get_only_serial("SELECT serial FROM `branch_list` where admin=$this->admin and branch='$branch'", 'branch_id', 'serial');
                    $branch_id = $branch1['branch_id'][0];
                    if ($product_type == "ALL" && $branch != "") {
                        $query1 = "SELECT sum(t1.product_name_id) as quantity,t1.product_name_id,t1.scale,t1.product_type,t2.product_name,t2.product_brand FROM `stock_product` as t1,product_name as t2,branch_list as t3
                    where t1.product_name_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin and t1.branch_id=$branch_id group by t1.product_name_id";
                    } else {
                        $query1 = "SELECT sum(t1.product_name_id) as quantity,t1.product_name_id,t1.scale,t1.product_type,t2.product_name,t2.product_brand FROM `stock_product` as t1,product_name as t2,branch_list as t3
                    where t1.product_name_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin and t1.branch_id=$branch_id and t1.product_type='$product_type' group by t1.product_name_id";
                    }
                }
            }
            $data1 = $this->madmin->get_stock_product($query1);
            $data1['asset_url'] = $this->asset_url;
            $this->load->view('ajax_code/v_filter_stock_product', $data1);
        }

        public function supplier_ledger_account() {
            $supplier_id = $_GET['id'];
            $query1 = "SELECT invoice_no,date,description, sum(dr) as dr,sum(cr) as cr FROM `supplier_ledger` where supplier_id=$supplier_id and admin=$this->admin group by invoice_no order by date";
            $data1 = $this->madmin->get_ledger_account_data($query1, array('invoice_no', 'description', 'dr', 'cr', 'date'), array('invoice_no', 'description', 'dr', 'cr', 'date'));

            $query2 = "SELECT * FROM `supplier_ledger2` where supplier_id=$supplier_id and admin=$this->admin order by date";
            $data2 = $this->madmin->get_supplier_ledger2($query2);

            $query3 = "SELECT * FROM `supplier` where admin=$this->admin and serial=$supplier_id";
            $data3 = $this->madmin->get_supplier($query3);

            $data = array_merge($data1, $data2, $data3);
            $data['supplier_id'] = $supplier_id;

            $query4="SELECT   (sum(cr)-sum(dr)) as current_balance FROM `supplier_ledger` where supplier_id=$supplier_id and admin=1";
            $data["current_balance"]=$this->madmin->get_single_data($query4,'current_balance');
            $this->view_page->admin_page('v_supplier_ledger', $data);
        }

        public function filter_supplier_trans_history(){
            $date1=$_GET["date1"];
            $date2=$_GET["date2"];
            $supplier_id=$_GET["supplier_id"];
            $query1 = "SELECT invoice_no,date,description, sum(dr) as dr,sum(cr) as cr FROM `supplier_ledger` where supplier_id=$supplier_id and admin=$this->admin and date between '$date1' and '$date2' group by invoice_no order by date";
            $data1 = $this->madmin->get_ledger_account_data($query1, array('invoice_no', 'description', 'dr', 'cr', 'date'), array('invoice_no', 'description', 'dr', 'cr', 'date'));
            //print_r($data1);
            //echo $query1;
            $data1["supplier_id"]=$supplier_id;

            $query2="SELECT   (sum(cr)-sum(dr)) as current_balance FROM `supplier_ledger` where supplier_id=1 and admin=1 and `date`<'$date1'";
            $data1["current_balance"]=$this->madmin->get_single_data($query2,'current_balance');


            $query3 = "SELECT * FROM `supplier_ledger2` where supplier_id=$supplier_id and admin=$this->admin and date between '$date1' and '$date2'  order by date";
            $data3 = $this->madmin->get_supplier_ledger2($query2);

            //echo $data;
            $this->load->view("ajax_code/v_ajax_supplier_ledger",$data1);
        }


       


        public function post_supplier_ledger_2() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['supplier_id'])) {
                $info = array('admin' => $this->admin, 'supplier_id' => $data['supplier_id'], 'description' => $data['description'], 'invoice_no' => "no", 'dr' => $data['dr'], 'cr' => $data['cr'], 'date' => $data['date']);
//print_r($info);
                if ($this->db->insert('supplier_ledger2', $info)) {
                    $this->alert_message("DATA INSERTED SUCCESSFULLY!");
                }
            }
        }

        public function supplier_id() {
            $supplier_id = $_GET['id'];
            $query1 = "SELECT * FROM `supplier` where admin=$this->admin and serial=$supplier_id";
            $data1 = $this->madmin->get_supplier($query1);

            $query2 = "SELECT t1.product_name_id,t1.invoice_no,t1.quantity,t1.purchase_price,t1.total_amount,t1.insertion_date,t2.product_name,t2.brand as product_brand,t2.product_type FROM 
                `stock_product` as t1,product_name_list as t2 where  t1.product_name_id=t2.serial and t1.admin=t2.admin and supplier_id=$supplier_id and t1.admin=$this->admin";
            $data2 = $this->madmin->get_supplied_product_by_supplier($query2);

            $query3 = "SELECT sum(quantity) as total_quantity,sum(total_amount) as total_amount  FROM `stock_product` where supplier_id=$supplier_id and admin=$this->admin";
            $data3 = $this->madmin->get_total_supplied_product($query3);
            $data = array_merge($data1, $data2, $data3);
            $data['supplier_id'] = $supplier_id;
            $this->view_page->admin_page('v_supplier_details', $data);
        }

        public function brand() {
            $query = "SELECT * FROM `brand_list` where admin=$this->admin";
            $data = $this->madmin->name_serial($query, array('serial', 'brand'), array('serial', 'brand'));
//print_r($data);
            $this->view_page->admin_page('v_brand', $data);
        }

        public function add_brand() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if ($this->db->insert('brand_list', array('admin' => $this->admin, 'brand' => $data['brand_name']))) {
                $this->alert_message("NEW BRAND ADDED SUCCESSFULLY!");
            }
        }

        public function update_brand($serial = "") {
            $query = "SELECT * FROM `brand_list` where admin=$this->admin and serial=$serial";
            $data = $this->madmin->name_serial($query, array('serial', 'brand'), array('serial', 'brand'));
            $this->load->view('ajax_code/v_update_brand', $data);
        }

        public function f_update_brand() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('serial' => $data['serial'], 'brand' => $data['brand']);
            if ($this->db->update('brand_list', $info, array('serial' => $data['serial'], 'admin' => $this->admin))) {
                $this->alert_message("BRAND UPDATED SUCCESSFULLY!");
            }
        }

        public function measuring_scale() {
            $query = "SELECT * FROM `measuring_scale` where admin=$this->admin";
            $data = $this->madmin->get_measurment_scale($query);
            $this->view_page->admin_page('v_measuring_scale', $data);
        }

        public function add_measuring_scale() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if ($this->db->insert('measuring_scale', array('admin' => $this->admin, 'scale' => $data['scale'], 'measurement' => $data['measurement']))) {
                $this->alert_message("NEW MEASUREMENT SCALE ADDED SUCCESSFULLY!");
            }
        }

        public function update_measuring_scale($serial = "") {
            $query = "SELECT * FROM `measuring_scale` where admin=$this->admin and serial=$serial";
            $data = $this->madmin->get_measurment_scale($query);
            $this->load->view('ajax_code/v_update_measuring_scale', $data);
        }

        public function f_update_measuring_scale() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('serial' => $data['serial'], 'scale' => $data['scale'], 'measurement' => $data['measurement']);
            if ($this->db->update('measuring_scale', $info, array('serial' => $data['serial'], 'admin' => $this->admin))) {
                $this->alert_message("MEASUREMENT SCALE UPDATED SUCCESSFULLY!");
            }
        }

        public function branch() {
            $query = "SELECT * FROM `branch_list` where admin=$this->admin";
            $data = $this->madmin->name_serial($query, array('serial', 'branch'), array('serial', 'branch'));
//print_r($data);
            $this->view_page->admin_page('v_branch', $data);
        }

        public function update_branch($serial) {
            $query = "SELECT * FROM `branch_list` where admin=$this->admin and serial=$serial";
            $data = $this->madmin->name_serial($query, array('serial', 'branch'), array('serial', 'branch'));
            $this->load->view('ajax_code/v_update_branch', $data);
        }

        public function f_update_branch() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('serial' => $data['serial'], 'branch' => $data['branch']);
            if ($this->db->update('branch_list', $info, array('serial' => $data['serial'], 'admin' => $this->admin))) {
                $this->alert_message("BRANCH UPDATED SUCCESSFULLY!");
            }
        }

        public function add_branch() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if ($this->db->insert('branch_list', array('admin' => $this->admin, 'branch' => $data['branch_name']))) {
                $this->alert_message("NEW BRANCH ADDED SUCCESSFULLY!");
            }
        }

        public function productname() {
            $query1 = "SELECT * FROM `brand_list` where admin=$this->admin";
            $data1 = $this->madmin->name_serial($query1, array('brand_serial', 'brand'), array('serial', 'brand'));
            $query2 = "SELECT * FROM `product_type` where admin=$this->admin";
            $data2 = $this->madmin->name_serial($query2, array('product_type_serial', 'product_type'), array('serial', 'product_type'));
            $query3 = "SELECT * FROM `measuring_scale` where admin=$this->admin";
            $data3 = $this->madmin->get_measurment_scale($query3);
            $query4 = "SELECT * FROM `product_name_list` where admin=$this->admin";
            $data4 = $this->madmin->get_broduct_name_list($query4);
            $data = array_merge($data1, $data2, $data3, $data4);
//print_r($data);
            $this->view_page->admin_page('v_product_name', $data);
        }

        public function add_new_product_name() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('admin' => $this->admin, 'product_type' => $data['product_type'], 'brand' => $data['product_brand'], 'scale' => $data['scale'],
                'measurement' => $data['measurement'], 'product_name' => $data['product_name']);
            if ($this->db->insert('product_name_list', $info)) {
                $this->alert_message("NEW PRODUCT NAME ADDED SUCCESSFULLY!");
            }
        }

        public function update_product_name($serial) {

            $query1 = "SELECT * FROM `brand_list` where admin=$this->admin";
            $data1 = $this->madmin->name_serial($query1, array('brand_serial', 'brand'), array('serial', 'brand'));
            $query2 = "SELECT * FROM `product_type` where admin=$this->admin";
            $data2 = $this->madmin->name_serial($query2, array('product_type_serial', 'product_type'), array('serial', 'product_type'));
            $query3 = "SELECT * FROM `measuring_scale` where admin=$this->admin";
            $data3 = $this->madmin->get_measurment_scale($query3);
            $query4 = "SELECT * FROM `product_name_list` where admin=$this->admin and serial=$serial";
            $data4 = $this->madmin->get_broduct_name_list($query4);
            $data = array_merge($data1, $data2, $data3, $data4);
//print_r($data);
            $this->load->view('ajax_code/v_update_product_name', $data);
        }

        public function f_update_product_name() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
// print_r($data);
            $info = array('serial' => $data['serial'], 'product_type' => $data['product_type'], 'brand' => $data['product_brand'], 'scale' => $data['scale'],
                'measurement' => $data['measurement'], 'product_name' => $data['product_name']);
            if ($this->db->update('product_name_list', $info, array('admin' => $this->admin, 'serial' => $data['serial']))) {
                $this->alert_message("PRODUCT NAME UPDATED SUCCESSFULLY!");
            }
        }

        public function get_supplier_invoice_data($supplier_invoice) {
            $info = explode("-", $supplier_invoice);
//echo "$supplier_invoice";
//print_r($info);
            $query = "SELECT t1.product_name_id,t2.product_name,t2.brand,t1.quantity2 as quantity,t1.purchase_price,t1.sales_price,t1.total_amount,t1.insertion_date FROM `stock_product` as t1,product_name_list as t2 where
                t1.admin=t2.admin and t1.product_name_id=t2.serial and t1.supplier_id=$info[0] and t1.invoice_no=$info[1] and t1.admin=$this->admin";
            $data = $this->madmin->get_supplier_invoice_info($query);
            $data['invoice_no'] = $info[1];
            $this->load->view('ajax_code/v_ajax_supplier_invoice', $data);
//print_r($data);
        }

        public function customer_type() {
            $data = $this->madmin->get_customer_type("SELECT * FROM `customer_type` where admin=$this->admin");
            $this->view_page->admin_page('v_customer_type', $data);
        }

        public function add_customer_type() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('type' => $data['customer_type_post'], 'admin' => $this->admin);
            // print_r($info);
            if ($this->db->insert('customer_type', $info)) {
                $this->alert_message("CUSTOMER TYPE ADDED SUCCESSFULLY");
            }
        }

        public function area() {
            $query = "select * from area where admin=$this->admin";
            $data = $this->madmin->name_serial($query, array('serial', 'area'), array('serial', 'area'));
            $this->view_page->admin_page('v_area', $data);
        }

        public function add_customer_area() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('area' => $data['customer_area'], 'admin' => $this->admin);
            if ($this->db->insert('area', $info)) {
                $this->alert_message("CUSTOMER AREA ADDED SUCCESSFULLY");
            }
        }

        public function get_specific_area($serial) {
            $query = "select * from area where admin=$this->admin and serial=$serial";
            $data = $this->madmin->name_serial($query, array('serial', 'area'), array('serial', 'area'));
            $this->load->view('ajax_code/v_update_area', $data);
        }

        public function f_update_area() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('area' => $data['customer_area']);
            if ($this->db->update('area', $info, array('serial' => $data['serial'], 'admin' => $this->admin))) {
                $this->alert_message("CUSTOMER AREA UPDARED SUCCESSFULLY");
            }
        }

        public function addnewcustomer() {
            $query1 = "select * from area where admin=$this->admin";
            $data1 = $this->madmin->name_serial($query1, array('area_serial', 'area'), array('serial', 'area'));
            $query2 = "select * from customer_type where admin=$this->admin";
            $data2 = $this->madmin->name_serial($query2, array('type_serial', 'type'), array('serial', 'type'));
            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_addnewcustomer', $data);
        }

        public function add_new_customer() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('admin' => $this->admin, 'area' => $data['area'], 'type' => $data['type'], 'shop_name' => $data['shop_name'], 'customer_name' => $data['customer_name']
                , 'address' => $data['customer_address'], 'mobile_no' => $data['mobile_no']);
            if ($this->db->insert('customer', $info)) {
                $this->alert_message("NEW CUSTOMER ADDED SUCCESSFULLY");
            }
        }

        public function currentcustomer() {
            $query = "SELECT * FROM `customer` where admin=$this->admin";
            $data = $this->madmin->get_customer($query);
            $this->view_page->admin_page('v_current_customer', $data);
        }

        public function get_customer_info($serial) {
            $query1 = "select * from area where admin=$this->admin";
            $data1 = $this->madmin->name_serial($query1, array('area_serial', 'a_area'), array('serial', 'area'));
            $query2 = "select * from customer_type where admin=$this->admin";
            $data2 = $this->madmin->name_serial($query2, array('type_serial', 't_type'), array('serial', 'type'));
            $query3 = "SELECT * FROM `customer` where admin=$this->admin and serial=$serial";
            $data3 = $this->madmin->get_customer($query3);
            $data = array_merge($data1, $data2, $data3);
            $this->load->view('ajax_code/v_update_customer_info', $data);
        }

        public function f_update_customer_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('type' => $data['type'], 'area' => $data['area'], 'shop_name' => $data['shop_name'], 'customer_name' => $data['customer_name'], 'address' => $data['address'],
                'mobile_no' => $data['mobile_no']);
            if ($this->db->update('customer', $info, array('serial' => $data['serial'], 'admin' => $this->admin))) {
                $this->alert_message("CUSTOMER INFO UPDARED SUCCESSFULLY");
            }
        }

        public function sales_product() {
            $query1 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,(t1.quantity/t1.measurement) as bundle,t1.measurement,
               t1.purchase_price,t1.sales_price,t1.measurement,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
               where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin and t1.quantity>0 and 
               right(t1.scale,6)='bundle'";
            $data1 = $this->madmin->get_tin_bundle_info_for_sale($query1);

            $query2 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,t1.measurement,t1.scale,
               t1.purchase_price,t1.sales_price,t1.measurement,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
               where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin and t1.quantity>0 and 
               right(t1.scale,5)='fiber'";
            $data2 = $this->madmin->get_other_product_info_for_sale(array('fiber_tin_stock_product_serial', 'fiber_tin_product_name_id', 'fiber_tin_product_name', 'fiber_tin_quantity', 'fiber_tin_purchase_price', 'fiber_tin_sales_price', 'fiber_tin_measurement', 'fiber_tin_branch', 'fiber_tin_scale'), $query2);


            $query3 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,t1.measurement,t1.scale,
               t1.purchase_price,t1.sales_price,t1.measurement,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
               where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin and t1.quantity>0 and t1.product_type!='TIN'";
            $data3 = $this->madmin->get_other_product_info_for_sale(array('other_stock_product_serial', 'other_product_name_id', 'other_product_name', 'other_quantity', 'other_purchase_price', 'other_sales_price', 'other_measurement', 'other_branch', 'other_scale'), $query3);

            $data = array_merge($data1, $data2, $data3);
            $this->view_page->admin_page('v_sales_product', $data);
        }

        public function check_product() {
// header("Location:http://localhost/rahelait/admin/get_customer_invoice_data/1-1");
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $z = 0; //insert_sold_product_counter and update_stock_product_counter
            $y = 0; //product_ledger_counter;
            $product_ledger = array();
            if ($data['action'] == "submit") {
                if (isset($data['customer_id']) && isset($data['invoice_no'])) {
                    $insert_sold_product_data = array();
                    $update_stock_product_data = array();
                    $customer_id = $data['customer_id'];
                    $date = $data['date'];
                    $laber_bill = $data['laber_bill'];
                    $disscount = $data['disscount'];
                    $invoice_no = $data['invoice_no'];
                    $paid = $data['paid'];
                    $total_sold_amount = 0;
                    if (isset($data['bundle_tin_id'])) {
                        $j = 0;
                        for ($i = 0; $i < count($data['bundle_tin_id']); $i++) {
                            $product_serial = $data['bundle_tin_id'][$i];
                            $product_name_id = $data["bundle_tin_name_id$product_serial"];
                            $measurement = $data["bundle_tin_measurement$product_serial"];
                            $quantity = ($data["bundle_sales_tin_bundle$product_serial"] * $measurement) + $data["bundle_sales_tin_pices$product_serial"];
                            $avl_quantity = $this->get_product_id_which_are_avail($quantity, $product_serial);
                            $product_name = $data["bundle_tin_name$product_serial"];
                            if ($avl_quantity != "not found") {
                                $price_per_pices = $data["bundle_tin_sales_price$product_serial"] / $measurement;
                                $total_amount = $quantity * $price_per_pices;
                                $quantity2 = $data["bundle_sales_tin_bundle$product_serial"] . " bundle " . $data["bundle_sales_tin_pices$product_serial"] . " piece";

                                $insert_sold_product_data[$z] = array('admin' => $this->admin, 'invoice_no' => $invoice_no, 'customer_id' => $customer_id, 'stock_product_serial' => $product_serial, 'sales_price' => $price_per_pices,
                                    'quantity' => $quantity, 'total_amount' => $total_amount, 'date' => $date, 'product_name' => $product_name);
                                $update_quantity = $avl_quantity - $quantity;
                                $update_stock_product_data[$z] = array('serial' => $product_serial, 'quantity' => $update_quantity);
                                $total_sold_amount+=$total_amount;
                                $product_ledger[$y] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'cr' => $total_amount, 'dr' => 0, 'date' => $date, 'description' => 'SOLD');
                                $j++;
                                $z++;
                                $y++;
                            }
                        }
                    }


                    if (isset($data['fiber_tin_id'])) {
                        for ($i = 0; $i < count($data['fiber_tin_id']); $i++) {
                            $stock_product_serial = $data['fiber_tin_id'][$i];
                            $sales_price = $data["fiber_tin_sales_price$stock_product_serial"];
                            $quantity = $data["fiber_sales_tin_pices$stock_product_serial"];
                            $measurement = $data["fiber_tin_measurement$stock_product_serial"];
                            $total_amount = $quantity * $sales_price * $measurement;
                            $product_name = $data["fiber_tin_name$stock_product_serial"];
                            $product_name_id = $data["fiber_tin_name_id$stock_product_serial"];
                            $insert_sold_product_data[$z] = array('admin' => $this->admin, 'invoice_no' => $invoice_no, 'customer_id' => $customer_id, 'stock_product_serial' => $stock_product_serial, 'sales_price' => $sales_price,
                                'quantity' => $quantity, 'total_amount' => $total_amount, 'date' => $date, 'product_name' => $product_name);

                            $avl_quantity = $this->get_product_id_which_are_avail($quantity, $stock_product_serial);
                            $update_quantity = $avl_quantity - $quantity;
                            $update_stock_product_data[$z] = array('serial' => $stock_product_serial, 'quantity' => $update_quantity);
                            //$product_ledger[$y] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'cr' => $total_amount, 'dr' => 0, 'date' => $date, 'description' => 'SOLD');
                            $product_ledger[$y] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'cr' => $total_amount, 'dr' => 0, 'date' => $date, 'description' => 'SOLD');
                            $total_sold_amount+=$total_amount;
                            $z++;
                            $y++;
                        }
                    }


                    if (isset($data['other_id'])) {
                        for ($i = 0; $i < count($data['other_id']); $i++) {
                            $stock_product_serial = $data['other_id'][$i];
                            $sales_price = $data["other_sales_price$stock_product_serial"];
                            $quantity = $data["other_sales_quantity$stock_product_serial"];
                            $measurement = $data["other_measurement$stock_product_serial"];
                            $total_amount = $quantity * $sales_price;
                            $product_name = $data["other_name$stock_product_serial"];
                            $product_name_id = $data["other_name_id$stock_product_serial"];
                            $insert_sold_product_data[$z] = array('admin' => $this->admin, 'invoice_no' => $invoice_no, 'customer_id' => $customer_id, 'stock_product_serial' => $stock_product_serial, 'sales_price' => $sales_price,
                                'quantity' => $quantity, 'total_amount' => $total_amount, 'date' => $date, 'product_name' => $product_name);
                            $avl_quantity = $this->get_product_id_which_are_avail($quantity, $stock_product_serial);
                            $update_quantity = $avl_quantity - $quantity;
                            $update_stock_product_data[$z] = array('serial' => $stock_product_serial, 'quantity' => $update_quantity);
                            $total_sold_amount+=$total_amount;
                            $product_ledger[$y] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'cr' => $total_amount, 'dr' => 0, 'date' => $date, 'description' => 'SOLD');
                            $y++;
                            $z++;
                        }
                    }
                    $customer_ldger_info = $this->create_customer_ledger($customer_id, $invoice_no, array('SOLD PRODUCT', 'LABER BILL', 'DISSCOUNT', 'PAID BY CUSTOMER'), array($total_sold_amount, $laber_bill, $disscount, $paid), '2014-12-12');
                    $this->db->trans_start();
                    $this->db->insert_batch('sold_product_tbl', $insert_sold_product_data);
                    $this->db->update_batch('stock_product', $update_stock_product_data, 'serial');
                    $this->db->insert_batch('customer_ledger', $customer_ldger_info);
                    $this->db->insert_batch('product_ledger', $product_ledger);
                    $this->db->trans_complete();
                    $url ="../admin/get_customer_invoice_data/$customer_id-$invoice_no";
                    header("Location:$url");
                    // print_r($product_ledger);
                } else {
                    $url ="../admin/currentcustomer";
                    header("Location:$url");
                }
            } else {
                if ($data['action'] == "check_amount") {
                    $this->check_amount($data);
                }
            }
        }

        private function check_amount($data) {
            $total_amount = 0;
            $not_avaiable = "";
            $product_name_list = array();
            $p = 0;
            $product_quantity_list = array();
            $q = 0;
            $product_sales_price_list = array();
            $a = 0;
            $product_amount_list = array();
            if (isset($data['other_id'])) {
                for ($i = 0; $i < count($data['other_id']); $i++) {
                    $stock_product_serial = $data['other_id'][$i];
                    $product_name = $data["other_name$stock_product_serial"];
                    $sales_price = $data["other_sales_price$stock_product_serial"];
                    $quantity = $data["other_sales_quantity$stock_product_serial"];
                    $total_amount+=$quantity * $sales_price;

                    $product_name_list[$p] = $product_name;
                    $product_quantity_list[$p] = $quantity;
                    $product_sales_price_list[$p] = $sales_price;
                    $product_amount_list[$p] = $product_quantity_list[$p] * $product_sales_price_list[$p];
                    $p++;
                }
            }
            if (isset($data['bundle_tin_id'])) {
                for ($i = 0; $i < count($data['bundle_tin_id']); $i++) {
                    $product_serial = $data['bundle_tin_id'][$i];
                    $measurement = $data["bundle_tin_measurement$product_serial"];
                    $product_name = $data["bundle_tin_name$product_serial"];
                    $quantity = ($data["bundle_sales_tin_bundle$product_serial"] * $measurement) + $data["bundle_sales_tin_pices$product_serial"];
                    $avl_quantity = $this->get_product_id_which_are_avail($quantity, $product_serial);
                    if ($avl_quantity != "not found") {
                        $price_per_pices = $data["bundle_tin_sales_price$product_serial"] / $measurement;
                        $total_amount = $total_amount + $quantity * $price_per_pices;

                        $product_name_list[$p] = $product_name;
                        $product_quantity_list[$p] = $quantity;
                        $product_sales_price_list[$p] = $price_per_pices;
                        $product_amount_list[$p] = $quantity * $price_per_pices;
                        $p++;
                    } else {
                        $not_avaiable.="$product_name,";
                    }
                }
            }
            if (isset($data['fiber_tin_id'])) {
                for ($i = 0; $i < count($data['fiber_tin_id']); $i++) {
                    $stock_product_serial = $data['fiber_tin_id'][$i];
                    $sales_price = $data["fiber_tin_sales_price$stock_product_serial"];
                    $quantity = $data["fiber_sales_tin_pices$stock_product_serial"];
                    $measurement = $data["fiber_tin_measurement$stock_product_serial"];
                    $amount = $quantity * $sales_price * $measurement;
                    $total_amount+=$amount;
                    $product_name = $data["fiber_tin_name$stock_product_serial"];

                    $product_name_list[$p] = $product_name;
                    $product_quantity_list[$p] = $quantity;
                    $product_sales_price_list[$p] = $sales_price;
                    $product_amount_list[$p] = $amount;
                    $p++;
                }
            }
            // echo $total_amount;

            if (isset($data['customer_id']) && isset($data['invoice_no']) && count($product_name_list) > 0) {
                $paid = number_format($data['paid'], 2, ".", "");
                $laber_bill = number_format($data['laber_bill'], 2, ".", "");
                $disscount = number_format($data['disscount'], 2, ".", "");
                $total_price = $total_amount + $laber_bill - $disscount - $paid;
                $total_amount = number_format($total_amount, 2, ".", "");
                $total_price = number_format($total_price, 2, ".", "");
                echo"<strong>PREPARED INVOICE SHEET</strong>";
                echo"<table class='table table-bordered table-hover'>";
                echo"<thead><tr class='info'><th>SERIAL</th><th>PRODUCT NAME</th><th>QUANTITY</th><th>SALES PRICE</th><th>AMOUNT</th></tr></thead>";
                for ($i = 0; $i < count($product_name_list); $i++) {
                    echo"<tr class='warning'><td>$i</td><td>$product_name_list[$i]</td><td>$product_quantity_list[$i]</td><td> $product_sales_price_list[$i]</td>
                    <td>$product_amount_list[$i]</td></tr>";
                }
                echo"</table>";
                echo"<table class='table table-bordered table-hover'>";
                echo"<tr class='info'><td>PRODUCT PRICE</td><td>{$total_amount}</td></tr>";
                echo"<tr class='info'><td>LABOUR BILL</td><td>(+)$laber_bill</td></tr>";
                echo"<tr class='info'><td>DISCOUNT</td><td>(-)$disscount</td></tr>";
                echo"<tr class='info'><td>PAID</td><td>(-)$paid</td></tr>";
                echo"<tr class='info'><td>TOTAL</td><td>$total_price</td></tr>";
                echo"</table>";
            }

            if ($not_avaiable != "") {
                echo"<strong>NOT AVAILABE:$not_avaiable</strong>";
            }
        }

        private function create_customer_ledger($customer_id, $invoice_no, $description, $amount, $date) {
            $info[0] = array('admin' => $this->admin, 'customer_id' => $customer_id, 'invoice_no' => $invoice_no, 'description' => $description[0], 'dr' => $amount[0], 'cr' => 0, 'date' => $date);
            $info[1] = array('admin' => $this->admin, 'customer_id' => $customer_id, 'invoice_no' => $invoice_no, 'description' => $description[1], 'dr' => $amount[1], 'cr' => 0, 'date' => $date);
            $info[2] = array('admin' => $this->admin, 'customer_id' => $customer_id, 'invoice_no' => $invoice_no, 'description' => $description[2], 'cr' => $amount[2], 'dr' => 0, 'date' => $date);
            $info[3] = array('admin' => $this->admin, 'customer_id' => $customer_id, 'invoice_no' => $invoice_no, 'description' => $description[3], 'cr' => $amount[3], 'dr' => 0, 'date' => $date);
            return $info;
        }

        private function get_product_id_which_are_avail($quantity, $serial) {
            $query = "select quantity from stock_product where serial=$serial and quantity>=$quantity and admin=$this->admin";
            $rs = $this->db->query($query);
            if ($rs->num_rows() > 0) {
                foreach ($rs->result() as $row) {
                    $quantity = $row->quantity;
                    return $quantity;
                }
            } else {
                return "not found";
            }
        }

        public function get_customer_invoice_data($info) {
            $info = explode("-", $info);
            $customer_id = $info[0];
            $invoice_no = $info[1];
            $query1 = "SELECT t1.product_name,sum(t1.quantity) as quantity,sum(t1.total_amount) as amount,t1.sales_price FROM `sold_product_tbl` as t1 
                where t1.invoice_no='$invoice_no' and t1.customer_id=$customer_id group by t1.product_name";
            $query2 = "SELECT t1.description,(sum(t1.cr)+sum(t1.dr)) as total_amount  FROM `customer_ledger` as t1 where t1.invoice_no='$invoice_no' and customer_id=$customer_id group by t1.description";
            $data1 = $this->madmin->get_customer_product_invoice_data($query1);
            $data2 = $this->madmin->get_customer_product_ledger_data($query2);

            $query3 = "SELECT * FROM `customer` where admin=$this->admin and serial=$customer_id";
            $data3 = $this->madmin->get_customer($query3);

            $data = array_merge($data1, $data2, $data3);
            $data['invoice_no'] = $invoice_no;
            $this->load->view('vadmin/v_customer_invoice_receipt', $data);
            //$this->view_page->admin_page('v_customer_invoice_receipt', $data);
            //
        //
//print_r($data);
        }

        public function sale_product_to_customer() {
            $serial = $_GET['id'];
            if ($_SESSION['type'] == "Admin") {
                $query1 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,(t1.quantity/t1.measurement) as bundle,t1.measurement,
               t1.purchase_price,t1.sales_price,t1.measurement,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
               where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin and t1.quantity>0 and 
               right(t1.scale,6)='bundle'";
                $data1 = $this->madmin->get_tin_bundle_info_for_sale($query1);

                $query2 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,t1.measurement,t1.scale,
               t1.purchase_price,t1.sales_price,t1.measurement,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
               where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin and t1.quantity>0 and 
               right(t1.scale,5)='fiber'";
                $data2 = $this->madmin->get_other_product_info_for_sale(array('fiber_tin_stock_product_serial', 'fiber_tin_product_name_id', 'fiber_tin_product_name', 'fiber_tin_quantity', 'fiber_tin_purchase_price', 'fiber_tin_sales_price', 'fiber_tin_measurement', 'fiber_tin_branch', 'fiber_tin_scale'), $query2);


                $query3 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,t1.measurement,t1.scale,
               t1.purchase_price,t1.sales_price,t1.measurement,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
               where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin and t1.quantity>0 and t1.product_type!='TIN'";
                $data3 = $this->madmin->get_other_product_info_for_sale(array('other_stock_product_serial', 'other_product_name_id', 'other_product_name', 'other_quantity', 'other_purchase_price', 'other_sales_price', 'other_measurement', 'other_branch', 'other_scale'), $query3);

                $query4 = "SELECT * FROM `customer` where admin=$this->admin and serial=$serial";
                $data4 = $this->madmin->get_customer($query4);
                $query5 = "SELECT (invoice_no+1) as invoice FROM `customer_ledger` where admin=$this->admin order by serial desc limit 1 ";
                $data5 = $this->madmin->get_only_one_invoice_no($query5);
                $data = array_merge($data1, $data2, $data3, $data4, $data5);
                $data['customer_serial'] = $serial;
                $this->view_page->admin_page('v_sales_product', $data);
            } else {
                $this->view_page->admin_page('page_not_found', "");
            }
        }

        public function customer_ledger_account() {
            $customer_id = $_GET['id'];
            $query1 = "SELECT invoice_no,date,description, sum(dr) as dr,sum(cr) as cr FROM `customer_ledger` where customer_id=$customer_id and admin=$this->admin group by invoice_no";
            $data1 = $this->madmin->get_ledger_account_data($query1, array('invoice_no', 'description', 'dr', 'cr', 'date'), array('invoice_no', 'description', 'dr', 'cr', 'date'));

            $query2 = "SELECT * FROM `customer_ledger2` where customer_id=$customer_id and admin=$this->admin";
            $data2 = $this->madmin->get_customer_ledger2($query2);

            $query3 = "SELECT * FROM `customer` where admin=$this->admin and serial=$customer_id";
            $data3 = $this->madmin->get_customer($query3);


            $data = array_merge($data1, $data2, $data3);
            $data['customer_id'] = $customer_id;

            $this->view_page->admin_page('v_customer_ledger', $data);
        }

        public function post_customer_ledger_2() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('customer_id' => $data['customer_id'], 'admin' => $this->admin, 'invoice_no' => 'INV', 'description' => $data['description'], 'dr' => $data['dr'], 'cr' => $data['cr'], 'date' => $data['date']);
            if ($this->db->insert('customer_ledger2', $info)) {
                $this->alert_message("NEW CUSTOMER LEDGER DATA INSERTED SUCCESSFULLY!");
            }
        }

        public function sales_report() {
            $query1 = "SELECT t2.date, t1.customer_name,t1.area,t1.type,t2.product_name,t2.quantity,t2.total_amount,t3.product_type FROM 
               `customer` as t1,sold_product_tbl as t2,stock_product as t3  where t1.serial=t2.customer_id and t2.stock_product_serial=t3.serial
               order by t3.product_type";
            $query2 = "SELECT sum(t2.quantity) as o_quantity,sum(t2.total_amount) as o_amount,t3.product_type as o_product_type FROM 
                sold_product_tbl as t2,stock_product as t3  where t2.stock_product_serial=t3.serial group by t3.product_type";

            $data1 = $this->madmin->get_sales_report_data($query1);
            $data2 = $this->madmin->get_overall_sales_report($query2);
            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_sales_report', $data);
        }

        public function filter_sales_report($date1, $date2) {
            $query1 = "SELECT t2.date, t1.customer_name,t1.area,t1.type,t2.product_name,t2.quantity,t2.total_amount,t3.product_type FROM 
               `customer` as t1,sold_product_tbl as t2,stock_product as t3  where t1.serial=t2.customer_id and t2.stock_product_serial=t3.serial
                and (t2.date between '$date1' and '$date2') order by t3.product_type";
            $query2 = "SELECT sum(t2.quantity) as o_quantity,sum(t2.total_amount) as o_amount,t3.product_type as o_product_type FROM 
                sold_product_tbl as t2,stock_product as t3  where t2.stock_product_serial=t3.serial  and (t2.date between '$date1' and '$date2') group by t3.product_type";

            $data1 = $this->madmin->get_sales_report_data($query1);
            $data2 = $this->madmin->get_overall_sales_report($query2);
            $data = array_merge($data1, $data2);
            //print_r($data);
            $this->load->view('ajax_code/v_filter_sales_report', $data);
//$data=$this->madmin->get_sales_report_data($query);
//print_r($data);
        }

        public function branch_wise_stock() {
            $query1 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,t1.purchase_price,t1.scale,sum(t1.quantity*t1.purchase_price*t1.measurement2) as amount,
                  t1.product_type,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
                 where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin group by t1.serial order by t1.product_type";
            $data1 = $this->madmin->get_stock_history($query1);
            $query2 = "SELECT t1.product_type as o_product_type, sum(t1.quantity) as o_quantity,sum(t1.quantity*t1.purchase_price*t1.measurement2) as o_amount FROM 
                 `stock_product` as t1 group by t1.product_type";
            $data2 = $this->madmin->get_overall_sales_report($query2);
            $query3 = "SELECT t1.branch_id,t2.branch,sum(t1.quantity*t1.purchase_price*t1.measurement2) as total_amount,sum(t1.quantity) as quantity,t1.product_type FROM `stock_product` as t1,branch_list as t2 
                     where t1.branch_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin  group by t1.branch_id,t1.product_type";
            $data3 = $this->madmin->get_only_branch_product($query3);
            $query4 = "SELECT t1.branch_id,t2.branch,sum(t1.quantity*t1.purchase_price*t1.measurement2) as total_amount FROM `stock_product` as t1,branch_list as t2 
                     where t1.branch_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin group by t1.branch_id";
            $data4 = $this->madmin->get_only_branch_amount($query4);
            $data = array_merge($data1, $data2, $data3, $data4);
            //print_r($data3);
            $this->view_page->admin_page('v_branch_wise_stock', $data);
        }

        public function filter_branch_wise_stock($date1, $date2) {
            $query1 = "SELECT t1.serial, t1.product_name_id,t2.product_name,t1.quantity,t1.purchase_price,t1.scale,sum(t1.quantity*t1.purchase_price*t1.measurement2) as amount,
                  t1.product_type,t3.branch FROM `stock_product` as t1,product_name_list as t2,branch_list as t3 
                 where t1.product_name_id=t2.serial and t3.serial=t1.branch_id and t1.admin=t2.admin and t2.admin=t3.admin and t1.date between '$date1' and '$date2' order by t1.product_type";
            $data1 = $this->madmin->get_stock_history($query1);
            $query2 = "SELECT t1.product_type as o_product_type, sum(t1.quantity) as o_quantity,sum(t1.quantity*t1.purchase_price*t1.measurement2) as o_amount FROM 
                 `stock_product` as t1 where t1.insertion_date between '$date1' and '$date2' group by t1.product_type order by t1.product_type";
            $data2 = $this->madmin->get_overall_sales_report($query2);
            $query3 = "SELECT t1.branch_id,t2.branch,sum(t1.quantity*t1.purchase_price*t1.measurement2) as total_amount,sum(t1.quantity) as quantity,t1.product_type FROM `stock_product` as t1,branch_list as t2 
                     where t1.branch_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin and t1.insertion_date between '$date1' and '$date2'  group by t1.branch_id,t1.product_type";
            $data3 = $this->madmin->get_only_branch_product($query3);
            $query4 = "SELECT t1.branch_id,t2.branch,sum(t1.quantity*t1.purchase_price*t1.measurement2) as total_amount FROM `stock_product` as t1,branch_list as t2 
                     where t1.branch_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin and t1.insertion_date between '$date1' and '$date2' group by t1.branch_id";
            $data4 = $this->madmin->get_only_branch_amount($query4);
            $data = array_merge($data1, $data2, $data3, $data4);
            $this->load->view('ajax_code/v_filter_branch_wise_stock', $data);
        }

        public function filter_product_in_stock($date1, $date2) {
            $query1 = "SELECT t2.brand, t2.product_name,t1.product_type,sum(t1.quantity) as quantity,sum(t1.total_amount) as amount FROM `stock_product` as t1,product_name_list as t2 
                  where t1.product_name_id=t2.serial and t1.admin=t2.admin and t1.quantity>0 and t1.admin=$this->admin and t1.insertion_date between '$date1' and '$date2'  group by  t1.product_name_id order by t1.product_type";
            $data1 = $this->madmin->get_stock_product($query1);
            $query2 = "SELECT t1.product_type as o_product_type, sum(t1.quantity) as o_quantity,sum(t1.total_amount) as o_amount FROM 
                 `stock_product` as t1 where t1.quantity>0 and t1.insertion_date between '$date1' and '$date2' group by t1.product_type order by t1.product_type";
            $data2 = $this->madmin->get_overall_sales_report($query2);
            $data = array_merge($data1, $data2);
            $data['asset_url'] = $this->asset_url;
//print_r($data1);
            $this->load->view('ajax_code/v_filter_product_in_stock', $data);
        }

        public function customer_report() {
            $query = "SELECT t1.customer_id,t3.customer_name,t3.area,t3.type,
                  COALESCE(sum(t1.dr),0)+(select COALESCE(sum(t2.dr),0) from customer_ledger2 as t2 where t1.customer_id=t2.customer_id) as dr,
                  COALESCE(sum(t1.cr),0)+(select COALESCE(sum(t2.cr),0) from customer_ledger2 as t2 where t1.customer_id=t2.customer_id) as cr
                  FROM `customer_ledger` as t1,customer as t3 where t3.serial=t1.customer_id group by t1.customer_id";
            $data = $this->madmin->get_customer_report_data($query);
            $this->view_page->admin_page('v_customer_report', $data);
        }

        public function product_ledger() {
            $product_name_id = $_GET["id"];
            $query1 = "SELECT * FROM `product_ledger` where product_name_id=$product_name_id and admin=$this->admin";
            $data1 = $this->madmin->get_product_ledger($query1);
            $query2 = "SELECT * FROM `product_name_list` where serial=$product_name_id and admin=$this->admin";
            $data2 = $this->madmin->get_broduct_name_list($query2);
            $data = array_merge($data1, $data2);
            $data['product_name_id'] = $product_name_id;
            $this->view_page->admin_page('v_product_ledger', $data);
        }

        public function filter_product_ledger($product_name_id = "", $date1 = "", $date2 = "") {
            $query = "SELECT * FROM `product_ledger` where product_name_id=$product_name_id and date between '$date1' and '$date2' and admin=$this->admin";
            $data = $this->madmin->get_product_ledger($query);
            //print_r($data);
            $this->load->view('ajax_code/v_filter_product_ledger', $data);
        }

        public function dashboard() {
            $query1 = "SELECT sum(quantity) as quantity,product_type FROM `stock_product` where admin=$this->admin group by product_type"; //product type vs quantity:
            $query2 = "SELECT sum(total_amount) as amount,substring(date,1,7) as month FROM `stock_product` where admin=$this->admin group by substring(date,1,7)"; //month vs total_stock_product_amount:
            $query3 = "SELECT sum(total_amount) as amount,substring(date,1,7) as month FROM `sold_product_tbl` where admin=$this->admin group by substring(date,1,7)"; //month vs total_sold_product_amount:
            $query4 = "SELECT sum(total_amount) as amount,date FROM `sold_product_tbl` where admin=$this->admin group by date";
            $query5 = "SELECT sum(t1.quantity) as quantity,t2.product_name FROM
                `stock_product` as t1,product_name_list as t2 where t1.product_name_id=t2.serial and t1.admin=t2.admin and t1.admin=$this->admin group by t1.product_name_id";
            $json = array();
            $json['avl_product'] = $this->madmin->dash_bar_chart($query1, "", array('product_type', 'quantity'), array('label', 'value'));
            $json['stock_product'] = $this->madmin->dash_bar_chart($query2, "", array('month', 'amount'), array('device', 'geekbench'));
            $json['sold_product'] = $this->madmin->dash_bar_chart($query3, "MONTH :", array('month', 'amount'), array('label', 'value'));
            $json['sold_product_by_date'] = $this->madmin->dash_bar_chart($query4, "", array('date', 'amount'), array('d', 'visits'));
            $json['avl_product_product_name'] = $this->madmin->dash_bar_chart($query5, "", array('product_name', 'quantity'), array('label', 'value'));

            $this->view_page->admin_page('v_dashboard', $json);
        }

        public function check_price() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            print_r($data);
        }

        public function all_customer_receipt() {
            $query = "SELECT t1.customer_id,t1.invoice_no, sum(t1.dr) as dr,sum(t1.cr) as cr,t2.customer_name,t2.mobile_no,t1.date FROM 
                `customer_ledger` as t1,customer as t2 where t2.serial=t1.customer_id group by t1.invoice_no";
            $data = $this->madmin->get_all_customer_receipt($query);
            $this->view_page->admin_page('v_all_customer_receipt', $data);
        }

        public function all_supplier_receipt() {
            $query = "SELECT t1.supplier_id,t1.invoice_no, sum(t1.dr) as dr,sum(t1.cr) as cr,t2.name,t2.phone,t1.date FROM 
                `supplier_ledger` as t1,supplier as t2 where t2.serial=t1.supplier_id group by t1.invoice_no";
            $data = $this->madmin->get_all_supplier_receipt($query);
            $this->view_page->admin_page('v_all_supplier_receipt', $data);
        }

        public function cashbook() {
            $date = date("Y-m-d");
            $data = $this->get_cash_book_data($date);
            $this->view_page->admin_page('v_cash_book', $data);
        }

        public function filter_cash_book($date = "no") {
            // echo"$date";
            $todate = date('Y-m-d', strtotime($date . " - 1 day"));
            $data = $this->get_cash_book_data($date);
            //print_r($data);

            $this->load->view('ajax_code/v_filter_cash_book', $data);
        }

        private function get_cash_book_data($date) {
            $query1 = "SELECT t1.description,t1.dr,t1.cr FROM `supplier_ledger` as t1 where t1.date='$date'"; //supplier_ledger s1
            $query2 = "SELECT t1.description,t1.dr,t1.cr FROM `supplier_ledger2` as t1 where t1.date='$date'"; //supplier_ledger2 s2
            $query3 = "SELECT t1.description,t1.dr as dr,t1.cr as cr FROM `customer_ledger` as t1 where t1.date='$date'"; //customer_ledger c1
            $query4 = "SELECT t1.description,t1.dr as dr,t1.cr as cr FROM `customer_ledger2` as t1 where t1.date='$date'"; //customer_ledger c2
            $query5 = "SELECT t1.description,t1.dr,t1.cr FROM `personal_dr_cr` as t1 where t1.date='$date'"; //persinal_dr_cr p1
            $data1 = $this->madmin->get_only_dr_cr($query1, array('s1_description', 's1_dr', 's1_cr'), array('description', 'dr', 'cr'));
            $data2 = $this->madmin->get_only_dr_cr($query2, array('s2_description', 's2_dr', 's2_cr'), array('description', 'dr', 'cr'));
            $data3 = $this->madmin->get_only_dr_cr($query3, array('c1_description', 'c1_dr', 'c1_cr'), array('description', 'dr', 'cr'));
            $data4 = $this->madmin->get_only_dr_cr($query4, array('c2_description', 'c2_dr', 'c2_cr'), array('description', 'dr', 'cr'));
            $data5 = $this->madmin->get_only_dr_cr($query5, array('p1_description', 'p1_dr', 'p1_cr'), array('description', 'dr', 'cr'));
            $data = array_merge($data1, $data2, $data3, $data4, $data5);
            $todate = date('Y-m-d', strtotime($date . " - 1 day"));
            $data['previous_day_credit'] = $this->get_all_dr_cr($todate);
            return $data;
        }

        public function get_all_dr_cr($todate) {
            $fromdate = "2010-01-01";
            $query[0] = "SELECT (sum(cr)-sum(dr)) as amount FROM `supplier_ledger` as t1 where t1.date between '$fromdate' and '$todate'"; //supplier_ledger s1
            $query[1] = "SELECT (sum(cr)-sum(dr)) as amount  FROM `supplier_ledger2` as t1 where t1.date between '$fromdate' and '$todate'"; //supplier_ledger2 s2
            $query[2] = "SELECT (sum(cr)-sum(dr)) as amount  FROM `customer_ledger` as t1 where t1.date between '$fromdate' and '$todate'"; //customer_ledger c1
            $query[3] = "SELECT (sum(cr)-sum(dr)) as amount  FROM `customer_ledger2` as t1 where t1.date between '$fromdate' and '$todate'"; //customer_ledger c2
            $query[4] = "SELECT (sum(cr)-sum(dr)) as amount  FROM `personal_dr_cr` as t1 where t1.date between '$fromdate' and '$todate'"; //persinal_dr_cr p1
            $amount = 0;
            for ($i = 0; $i < count($query); $i++) {
                $amount+=$this->get_only_dr_cr($query[$i], 'amount');
            }
            return number_format($amount, 2, ".", "");
        }

        private function get_only_dr_cr($query, $col) {
            $rs = $this->db->query($query);
            $amount = 0;
            if ($rs->num_rows() > 0) {
                foreach ($rs->result() as $row) {
                    $amount = $row->$col;
                }
            }
            return $amount;
        }

        public function profile() {
            if ($_SESSION['type'] == "Admin") {
                $query1 = "SELECT * FROM `user` where type='admin' and serial={$_SESSION['serial']}";
                $data1 = $this->madmin->get_user_info($query1, array('admin_name', 'admin_password', 'admin_shop_name'), array('name', 'password', 'shop_name'));
                $this->view_page->admin_page('v_profile', $data1);
                //print_r($data1);
            }
        }

        public function update_admin() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $query1 = "SELECT * FROM `user` where type='admin' and serial={$_SESSION['serial']} and password='{$data['admin_old_password']}'";
            $data1 = $this->madmin->get_user_info($query1, array('admin_name', 'admin_password', 'admin_shop_name'), array('name', 'password', 'shop_name'));
            if (isset($data1['admin_name'])) {
                $info = array('name' => $data['admin_name'], 'password' => $data['admin_new_password'], 'shop_name' => $data['admin_shop_name']);
                if ($this->db->update('user', $info, array('type' => 'admin'))) {
                    $_SESSION['shop_name'] = $data['admin_shop_name'];
                    $this->alert_message("ADMIN INFO UPDATED SUCCESSFULLY");
                }
            } else {
                $this->alert_message("INCORRECT ADMIN PASSWORD!");
            }
        }

        public function update_user() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $query1 = "SELECT * FROM `user` where type='admin' and password='{$data['admin_password']}'";
            $data1 = $this->madmin->get_user_info($query1, array('admin_name', 'admin_password', 'admin_shop_name'), array('name', 'password', 'shop_name'));
            if (isset($data1['admin_name'])) {
                $info = array('name' => $data['user_name'], 'password' => $data['user_new_password'], 'shop_name' => $data['user_shop_name']);
                if ($this->db->update('user', $info, array('type' => 'user'))) {
                    $this->alert_message("USER INFO UPDATED SUCCESSFULLY");
                }
            }
        }

        public function customer_id($serial) {
            $query = "SELECT product_name,sales_price,quantity,total_amount,date  FROM `sold_product_tbl` where customer_id=$serial";
        }

        public function personal_dr_cr() {
            $query = "SELECT * FROM `personal_dr_cr` where admin=$this->admin";
            $data = $this->madmin->get_product_ledger($query); //get debit-credit
            $this->view_page->admin_page('v_personal_dr_cr', $data);
        }

        public function filter_personal_dr_cr($date1 = "", $date2 = "") {
            $query = "SELECT * FROM `personal_dr_cr` where admin=$this->admin and date between '$date1' and '$date2'";
            $data = $this->madmin->get_product_ledger($query);
            $this->load->view('ajax_code/v_filter_personal_dr_cr', $data);
        }

        public function post_personal_dr_cr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('admin' => $this->admin, 'description' => $data['description'], 'dr' => $data['dr'], 'cr' => $data['cr'], 'date' => $data['date']);
            if ($this->db->insert('personal_dr_cr', $info)) {
                $this->alert_message("TRNSACTION INSERTED SUCCESSFULLY!");
            }
        }

        public function customer_product() {
            $customer_id = $_GET['id'];
            $query1 = "SELECT t2.product_type,t2.product_type, t1.product_name,t1.sales_price,t1.quantity,t1.total_amount,t1.date FROM 
                   `sold_product_tbl` as t1,stock_product as t2 where t1.customer_id=$customer_id and t2.serial=t1.stock_product_serial and t1.admin=t2.admin and t1.admin=$this->admin";
            $query2 = "SELECT * FROM `customer` where admin=$this->admin and serial=$customer_id";

            $data1 = $this->madmin->get_customer_product($query1);
            $data2 = $this->madmin->get_customer($query2);
            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_customer_product', $data);
        }

        public function update_data_of_supplier_info($supplier_id = "1") {
            $query1 = "SELECT * FROM `supplier` where admin=$this->admin and serial=$supplier_id";
            $data1 = $this->madmin->get_supplier($query1);
            $this->load->view('ajax_code/v_update_supplier_info', $data1);
        }
        

        public function update_color() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('top_side_color' => $data['top_side'], 'body_color' => $data['body']);
            // print_r($info);
            $_SESSION['top_side_color'] = $data['top_side'];
            $_SESSION['body_color'] = $data['body'];
            $this->db->update('user', $info);
            echo"recovered";
        }

        /* public function sales_report2(){
          $query="SELECT t1.customer_id,t2.customer_name,t2.mobile_no,t2.shop_name,sum(t1.total_amount) as amount,t3.product_type FROM `sold_product_tbl` as t1,customer as t2,product_name_list as t3
          where t1.customer_id=t2.serial and t1.product_name=t3.product_name group by t3.product_type,t1.customer_id order by t1.customer_id";
          $data= $this->madmin->get_sales_report2($query);
          $this->view_page->admin_page('v_sales_report2',$data);
          } */

        public function sales_report2() {
            $query = "SELECT DISTINCT(customer_id) FROM `sold_product_tbl`";
            $data = $this->madmin->get_only_serial($query, 'customer_id', 'customer_id');
            $info = array();
            if (isset($data['customer_id'])) {
                $i = 0;
                foreach ($data['customer_id'] as $customer_id) {
                    $info['data'][$i] = $this->madmin->get_sales_report2($customer_id);
                    $i++;
                }
                //print_r($info['data']);
                //print_r($info['customer_name'][0]);
                $this->view_page->admin_page('v_sales_report2', $info);
            } else {
                $this->view_page->admin_page('page_not_found', "");
            }
        }

        public function return_product() {
            $query = "SELECT distinct(invoice_no) FROM `sold_product_tbl`";
            $data = $this->madmin->get_only_serial($query, "invoice_no", "invoice_no");
            //print_r($data);
            $this->view_page->admin_page('v_return_product', $data);
        }

        public function return_product2($invoice_no) {
            $query = "SELECT t1.serial, t1.stock_product_serial,t1.product_name,t1.sales_price,t1.quantity,t2.branch_id,t3.branch,t2.scale,t1.invoice_no,t1.total_amount,t2.product_name_id,t2.quantity as stock_quantity,t2.serial as stock_serial  FROM `sold_product_tbl` as t1,stock_product as t2,branch_list as t3 
            where t1.invoice_no=$invoice_no and t1.stock_product_serial=t2.serial and t2.branch_id=t3.serial";
            $data = $this->madmin->get_return_a_product($query);
            $this->load->view('ajax_code/v_return_product_data', $data);
        }

        public function update_sales_product() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $sold_product_info = array();
            if (isset($data['sold_serial'])) {
                $date = $data['date'];
                $invoice_no = $data['invoice_no'];
                $total = 0;
                for ($i = 0; $i < count($data['sold_serial']); $i++) {
                    $sold_serial = $data['sold_serial'][$i];
                    $product_name = $data["product_name_$sold_serial"];
                    $product_name_id = $data["product_name_id_$sold_serial"];
                    $quantity = $data["quantity_$sold_serial"];
                    $sales_price = $data["sales_price_$sold_serial"];
                    $stock_product_serial = $data["stock_product_serial_$sold_serial"];
                    $previous_quantity = $data["orginal_quantity_$sold_serial"];
                    $previous_amount = $data["total_amount_$sold_serial"];
                    $stock_quantity = $data["stock_quantity_$sold_serial"];
                    $stock_serial = $data["stock_serial_$sold_serial"];
                    $update_quantity = $previous_quantity - $quantity;
                    $update_amount = $previous_amount - ($quantity * $sales_price);
                    $product_dr = $quantity * $sales_price;

                    $curr_stock = $stock_quantity + $quantity;
                    $total+=$product_dr;
                    $current_stock_product[$i] = array('serial' => $stock_serial, 'quantity' => $curr_stock);
                    $sold_product_info[$i] = array('serial' => $sold_serial, 'quantity' => $update_quantity, 'total_amount' => $update_amount);
                    $product_ledger_info[$i] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'date' => $date, 'dr' => $product_dr, 'cr' => 0, 'description' => 'RETURN');
                    $returned_product_info[$i] = array('admin' => $this->admin, 'product_name_id' => $product_name_id, 'invoice_no' => $invoice_no, 'quantity' => $quantity, 'total_amount' => $product_dr, 'date' => $date);
                }
                $query = "update `customer_ledger` set dr=dr-$total where invoice_no='$invoice_no' and description='SOLD PRODUCT'";
                $this->db->trans_start();
                $this->db->update_batch('sold_product_tbl', $sold_product_info, 'serial');
                $this->db->insert_batch('product_ledger', $product_ledger_info);
                $this->db->insert_batch('returned_product_list', $returned_product_info);
                $this->db->update_batch('stock_product', $current_stock_product, 'serial');
                $this->db->query($query);
                $this->alert_message("UPDATED SUCCESSFULLY!");
                $this->db->trans_complete();
            }
        }

        public function return_product_list() {
            $query = "SELECT t1.product_name_id,t2.product_name,t2.product_type,t1.date,t1.quantity,t1.total_amount,t2.scale FROM `returned_product_list` as t1,product_name_list as t2
                    where t1.product_name_id=t2.serial";
            $data = $this->madmin->get_return_product_list($query);
            $this->view_page->admin_page('v_return_product_list', $data);
        }

        public function theme_test() {
            $this->view_page->theme_test();
        }

    }

}
?>
