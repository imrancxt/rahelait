<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin2
 *
 * @author imran.v2v
 */
session_start();
if (isset($_SESSION['type'])) {

    class admin2 extends CI_Controller {
        //put your code here
        function __construct() {
            parent::__construct();
            $this->load->library('view_page');
            $this->load->model('madmin');
            $this->admin = 1;

        }
         private function alert_message($message) {
            echo"<div class='alert alert-warning'>$message
                            <button type ='button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'><span aria-hidden = 'true'>&times;
                                </span>
                            </button>
                        </div>";
        }
        public function theme($theme){
            $_SESSION['dpos_theme']=$theme;
            echo"refresh";
        }
        public function user_management(){
            $query="select * from user";
            $data=$this->madmin->get_data($query,array('serial','type','name','password'),array('serial','type','name','password'));
            //print_r($data);
            $this->view_page->admin_page('v_user_management',$data);
        }
        public function add_user_info(){
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if($this->db->insert('user',$data)){
                echo "refresh";
            }
        }
        public function update_user_info(){
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if($this->db->update('user',$data,array('serial'=>$data["serial"]))){
                echo"refresh";
            }
        }
        public function delete_user(){
            $serial=$_GET["serial"];
            if($this->db->delete('user',array('serial'=>$serial))){
                echo"refresh";
            }
        }
        public function f_update_supplier_info(){
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            //print_r($data);
            if($this->db->update('supplier',$data,array('serial'=>$data['serial']))){
                $this->alert_message("Info Updated!");
            }
        }

        public function stock_register() {
           /* $query1 = "SELECT * FROM `supplier` where admin=$this->admin and access='yes'";
            $data1 = $this->madmin->get_supplier($query1);
            $query2 = "SELECT * FROM `branch_list` where admin=$this->admin";
            $data2 = $this->madmin->get_branch_list($query2);
            
            $query3 = "SELECT (invoice_no+1) as invoice FROM `supplier_ledger` where admin=$this->admin order by serial desc limit 1 ";
            $data3 = $this->madmin->get_only_one_invoice_no($query3);*/

            $query4 = "SELECT * FROM `product_name_list` where admin=$this->admin";
            $data4 = $this->madmin->get_broduct_name_list($query4);
            //$data = array_merge($data1, $data2, $data3, $data4);
            //print_r($data);
            //$this->view_page->admin_page('v_product_name', $data);


            $this->view_page->admin_page('v_stock_register',$data4);

        }
        public function stock_register_product(){
            $id=$_GET["p_id"];
            $type=$_GET["p_type"];



            $query1 = "SELECT * FROM `product_name_list` where admin=$this->admin and product_type='$type' and serial=$id";
            $data1 = $this->madmin->get_broduct_name_list($query1);
            $data2 = array();


            $query3 = "SELECT * FROM `supplier` where admin=$this->admin and access='yes'";
            $data3 = $this->madmin->get_supplier($query3);
            $query4 = "SELECT * FROM `branch_list` where admin=$this->admin";
            $data4 = $this->madmin->get_branch_list($query4);
            
            $query5 = "SELECT (invoice_no+1) as invoice FROM `supplier_ledger` where admin=$this->admin order by serial desc limit 1 ";
            $data5 = $this->madmin->get_only_one_invoice_no($query5);

            if (isset($data1['pn_type'])) {
                $query2 = "SELECT * FROM `product_type` where admin=$this->admin and product_type='$type'";
                $data2 = $this->madmin->get_product_type($query2);
                $data = array_merge($data1, $data2,$data3,$data4,$data5);
                $data["stock_type"]=$type;
                $this->view_page->admin_page('v_add_new_product_to_stock', $data);
                //print_r($data1);
               // $this->show_product_to_be_stocked($data1['pn_type'][0], $data);
            }


        }


        public function stock_history(){

            $product_type=$_GET["product_type"];
            $query="SELECT t1.date,t1.invoice_no,sum(t1.quantity) as available,sum(t1.quantity2) as stock,(sum(t1.quantity2)-sum(t1.quantity)) as sold FROM `stock_product` as t1 where t1.product_type='$product_type' group by invoice_no";
            $data=$this->madmin->get_data($query,array('date','invoice_no','stock','sold'),array('date','invoice_no','stock','sold'));
            //print_r($data);

            $data["product_type"]=$product_type;
            $this->view_page->admin_page('v_stock_history', $data);
            

        }

    }

}
?>
