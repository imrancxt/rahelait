<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of madmin
 *
 * @author imran
 */
class madmin extends CI_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function get_product_type($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['type_serial'][$i] = $row->serial;
                $info['product_type'][$i] = $row->product_type;
                $i++;
            }
        }
        return $info;
    }

    public function get_product_name($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['product_name_serial'][$i] = $row->serial;
                $info['product_name'][$i] = $row->product_name;
                $info['product_brand'][$i] = $row->product_brand;
                $info['product_type'][$i] = $row->product_type;
                $info['description'][$i] = $row->description;
                $info['date'][$i] = $row->date;
                $i++;
            }
        }
        return $info;
    }

    public function get_supplier($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['name'][$i] = $row->name;
                $info['phone'][$i] = $row->phone;
                $info['address'][$i] = $row->address;
                $info['sales_person'][$i] = $row->sales_person;
                $info['address'][$i] = $row->address;
                $info['add_date'][$i] = $row->insertion_date;
                $i++;
            }
        }
        return $info;
    }

    public function get_branch_list($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['branch_serial'][$i] = $row->serial;
                $info['branch_name'][$i] = $row->branch;
                $i++;
            }
        }
        return $info;
    }

    public function get_only_serial($query, $variable, $column) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$variable][$i] = $row->$column;
                $i++;
            }
        }
        return $info;
    }

    public function get_only_name($query, $variable, $column) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$variable][$i] = $row->$column;
                $i++;
            }
        }
        return $info;
    }

    public function get_stock_product($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['product_name'][$i] = $row->product_name;
                $info['product_type'][$i] = $row->product_type;
                $info['quantity'][$i] = $row->quantity;
                $info['brand'][$i] = $row->brand;
                $info['amount'][$i] = $row->amount;
                $i++;
            }
        }
        return $info;
    }

    public function get_ledger_account_data($query, $var, $column) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            $info['balance'][0] = 0;
            foreach ($rs->result() as $row) {
                $info[$var[0]][$i] = $row->$column[0];
                $info[$var[1]][$i] = $row->$column[1];
                $info[$var[2]][$i] = $row->$column[2];
                $info[$var[3]][$i] = $row->$column[3];
                $info[$var[4]][$i] = $row->$column[4];
                $i++;
            }
        }
        return $info;
    }

    public function get_supplied_product_by_supplier($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['product_name'][$i] = $row->product_name;
                $info['invoice_no'][$i] = $row->invoice_no;
                $info['quantity'][$i] = $row->quantity;
                $info['purchase_price'][$i] = number_format($row->purchase_price, 2, ".", "");
                $info['insertion_date'][$i] = $row->insertion_date;
                $info['product_name_id'][$i] = $row->product_name_id;
                $info['product_brand'][$i] = $row->product_brand;
                $info['product_type'][$i] = $row->product_type;
                $info['amount'][$i] = number_format($row->total_amount, 2, ".", "");
                $i++;
            }
        }
        return $info;
    }

    public function get_total_supplied_product($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['total_quantity'][$i] = $row->total_quantity;
                $info['total_amount'][$i] = $row->total_amount;
                $i++;
            }
        }
        return $info;
    }

    public function get_supplier_ledger2($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['suppler_ledger2_serial'][$i] = $row->serial;
                $info['description2'][$i] = $row->description;
                $info['dr2'][$i] = number_format($row->dr, 2, '.', '');
                $info['cr2'][$i] = number_format($row->cr, 2, '.', '');
                $info['date2'][$i] = $row->date;
                $i++;
            }
        }
        return $info;
    }

    public function get_customer_ledger2($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['customer_ledger2_serial'][$i] = $row->serial;
                $info['description2'][$i] = $row->description;
                $info['dr2'][$i] = number_format($row->dr, 2, '.', '');

                $info['cr2'][$i] = number_format($row->cr, 2, '.', '');
                $info['date2'][$i] = $row->insertion_date;
                $i++;
            }
        }
        return $info;
    }

    public function name_serial($query, $var, $col) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$var[0]][$i] = $row->$col[0];
                $info[$var[1]][$i] = $row->$col[1];
                $i++;
            }
        }
        return $info;
    }

    public function get_measurment_scale($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['scale'][$i] = $row->scale;
                $info['measurement'][$i] = $row->measurement;
                $i++;
            }
        }
        return $info;
    }

    public function get_broduct_name_list($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['pn_serial'][$i] = $row->serial;
                $info['pn_type'][$i] = $row->product_type;
                $info['pn_brand'][$i] = $row->brand;
                $info['pn_scale'][$i] = $row->scale;
                $info['pn_measurement'][$i] = $row->measurement;
                $info['pn_product_name'][$i] = $row->product_name;
                $info['insertion_date'][$i] = $row->insertion_date;
                $i++;
            }
        }
        return $info;
    }

    public function get_supplier_invoice_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['product_name_id'][$i] = $row->product_name_id;
                $info['product_name'][$i] = $row->product_name;
                $info['quantity'][$i] = $row->quantity;
                $info['purchase_price'][$i] = $row->purchase_price;
                $info['sales_price'][$i] = number_format($row->sales_price, 2);
                $info['total_amount'][$i] = $row->total_amount;
                $info['brand'][$i] = $row->brand;
                $info['insertion_date'][$i] = $row->insertion_date;
                $i++;
            }
        }
        return $info;
    }

    public function get_customer_type($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['customer_type'][$i] = $row->type;
                $i++;
            }
        }
        return $info;
    }

    public function get_customer($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['type'][$i] = $row->type;
                $info['area'][$i] = $row->area;
                $info['shop_name'][$i] = $row->shop_name;
                $info['customer_name'][$i] = $row->customer_name;
                $info['address'][$i] = $row->address;
                $info['mobile_no'][$i] = $row->mobile_no;
                $i++;
            }
        }
        return $info;
    }

    public function get_tin_bundle_info_for_sale($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['bundle_tin_stock_product_serial'][$i] = $row->serial;
                $info['bundle_tin_product_name_id'][$i] = $row->product_name_id;
                $info['bundle_tin_product_name'][$i] = $row->product_name;
                $info['bundle_tin_quantity'][$i] = $row->quantity;
                $info['bundle_tin_purchase_price'][$i] = $row->purchase_price;
                $info['bundle_tin_sales_price'][$i] = $row->sales_price;
                $info['bundle_tin_bundle'][$i] = $row->bundle;
                $info['bundle_tin_measurement'][$i] = $row->measurement;
                $info['bundle_tin_branch'][$i] = $row->branch;
                $i++;
            }
        }
        return $info;
    }

    public function get_other_product_info_for_sale($var, $query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$var[0]][$i] = $row->serial;
                $info[$var[1]][$i] = $row->product_name_id;
                $info[$var[2]][$i] = $row->product_name;
                $info[$var[3]][$i] = $row->quantity;
                $info[$var[4]][$i] = $row->purchase_price;
                $info[$var[5]][$i] = $row->sales_price;
                $info[$var[6]][$i] = $row->measurement;
                $info[$var[7]][$i] = $row->branch;
                $info[$var[8]][$i] = $row->scale;
                $i++;
            }
        }
        return $info;
    }

    public function get_customer_product_invoice_data($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['product_name'][$i] = $row->product_name;
                $info['sales_price'][$i] = number_format($row->sales_price, 2, ".", "");
                $info['quantity'][$i] = $row->quantity;
                $info['amount'][$i] = $row->amount;
                $i++;
            }
        }
        return $info;
    }

    public function get_customer_product_ledger_data($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['description'][$i] = $row->description;
                $info['total_amount'][$i] = $row->total_amount;
                $i++;
            }
        }
        return $info;
    }

    public function get_sales_report_data($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['customer_name'][$i] = $row->customer_name;
                $info['area'][$i] = $row->area;
                $info['type'][$i] = $row->type;
                $info['product_name'][$i] = $row->product_name;
                $info['quantity'][$i] = $row->quantity;
                $info['product_type'][$i] = $row->product_type;
                $info['sales_date'][$i] = $row->date;
                $info['total_amount'][$i] = number_format($row->total_amount, 2, ".", "");
                $i++;
            }
        }
        return $info;
    }

    public function get_overall_sales_report($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['o_quantity'][$i] = $row->o_quantity;
                $info['o_amount'][$i] = number_format($row->o_amount, 2, ".", "");
                $info['o_product_type'][$i] = $row->o_product_type;
                $i++;
            }
        }
        return $info;
    }

    public function get_stock_history($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['product_name'][$i] = $row->product_name;
                $info['quantity'][$i] = $row->quantity;
                $info['amount'][$i] = number_format($row->amount, 2, ".", "");
                $info['product_type'][$i] = $row->product_type;
                $info['branch'][$i] = $row->branch;
                $info['purchase_price'][$i] = number_format($row->purchase_price, 2, ".", "");
                $info['scale'][$i] = $row->scale;
                $i++;
            }
        }
        return $info;
    }

    public function get_customer_report_data($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['customer_id'][$i] = $row->customer_id;
                $info['customer_name'][$i] = $row->customer_name;
                $info['area'][$i] = $row->area;
                $info['type'][$i] = $row->type;
                $info['dr'][$i] = number_format($row->dr, 2, ".", "");
                $info['cr'][$i] = number_format($row->cr, 2, ".", "");
                $balance = $info['cr'][$i] - $info['dr'][$i];
                $info['balance'][$i] = number_format($balance, 2, ".", "");
                $i++;
            }
        }
        return $info;
    }

    public function get_product_ledger($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['date'][$i] = $row->date;
                $info['description'][$i] = $row->description;
                $info['dr'][$i] = number_format($row->dr, 2, ".", "");
                $info['cr'][$i] = number_format($row->cr, 2, ".", "");
                $i++;
            }
        }
        return $info;
    }

    public function get_all_customer_receipt($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['customer_id'][$i] = $row->customer_id;
                $info['invoice_no'][$i] = $row->invoice_no;
                $info['dr'][$i] = number_format($row->dr, 2, ".", "");
                $info['cr'][$i] = number_format($row->cr, 2, ".", "");
                $info['customer_name'][$i] = $row->customer_name;
                $info['mobile_no'][$i] = $row->mobile_no;
                $info['date'][$i] = $row->date;
                $i++;
            }
        }
        return $info;
    }

    public function get_all_supplier_receipt($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['supplier_id'][$i] = $row->supplier_id;
                $info['invoice_no'][$i] = $row->invoice_no;
                $info['dr'][$i] = number_format($row->dr, 2, ".", "");
                $info['cr'][$i] = number_format($row->cr, 2, ".", "");
                $info['supplier_name'][$i] = $row->name;
                $info['mobile_no'][$i] = $row->phone;
                $info['date'][$i] = $row->date;
                $i++;
            }
        }
        return $info;
    }

    public function dash_bar_chart($query, $var, $col, $type) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                //array_push($info, array('device' => "YEAR:$row->product_type", 'geekbench' =>"$row->quantity"));
                $x = $row->$col[0];
                $y = (int) $row->$col[1];
                array_push($info, array("$type[0]" => "$var $x", "$type[1]" => "$y")); //X VS Y
            }
        }
        return json_encode($info, JSON_NUMERIC_CHECK);
    }

    public function get_only_dr_cr($query, $var, $col) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$var[0]][$i] = $row->$col[0];
                $info[$var[1]][$i] = number_format($row->$col[1], 2, ".", "");
                $info[$var[2]][$i] = number_format($row->$col[2], 2, ".", "");
                $i++;
            }
        }
        return $info;
    }

    public function get_user_info($query, $var, $col) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$var[0]][$i] = $row->$col[0];
                $info[$var[1]][$i] = $row->$col[1];
                $info[$var[2]][$i] = $row->$col[2];
                $i++;
            }
        }
        return $info;
    }

    public function get_customer_product($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['product_type'][$i] = $row->product_type;
                $info['product_name'][$i] = $row->product_name;
                $info['sales_price'][$i] = number_format($row->sales_price, 2, ".", "");
                $info['quantity'][$i] = $row->quantity;
                $info['total_amount'][$i] = number_format($row->total_amount, 2, ".", "");
                $info['date'][$i] = $row->date;
                $i++;
            }
        }
        return $info;
    }

    public function get_only_one_invoice_no($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                $info['invoice_no'] = $row->invoice;
            }
        }
        return $info;
    }

    public function get_only_branch_product($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['o1_branch_id'][$i] = $row->branch_id;
                $info['o1_branch_name'][$i] = $row->branch;
                $info['o1_product_type'][$i] = $row->product_type;
                $info['o1_total_quantity'][$i] = $row->quantity;
                $info['o1_total_amount'][$i] = number_format($row->total_amount, 2, ".", "");
                $i++;
            }
        }
        return $info;
    }

    public function get_only_branch_amount($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['o2_branch_id'][$i] = $row->branch_id;
                $info['o2_branch_name'][$i] = $row->branch;
                $info['o2_total_amount'][$i] = number_format($row->total_amount, 2, ".", "");
                $i++;
            }
        }
        return $info;
    }

    /* public function get_sales_report2($query) {
      $info = array();
      $rs = $this->db->query($query);
      if ($rs->num_rows() > 0) {
      $i = 0;
      foreach ($rs->result() as $row) {
      $info['customer_id'][$i] = $row->customer_id;
      $info['customer_name'][$i] = $row->customer_name;
      $info['mobile_no'][$i] = $row->mobile_no;
      $info['shop_name'][$i] = $row->shop_name;
      $info['product_type'][$i] = $row->product_type;
      $info['amount'][$i] = number_format($row->amount,2,".","");
      $i++;
      }
      }
      return $info;
      } */

    public function get_sales_report2($customer_id) {
        $query = "SELECT customer_name,shop_name,address FROM `customer` where serial=$customer_id";
        $data['customer_info'] = $this->get_user_info($query, array('customer_name', 'shop_name', 'address'), array('customer_name', 'shop_name', 'address'));

        $info['customer_name'] = $data['customer_info']['customer_name'][0];
        $info['shop_name'] = $data['customer_info']['shop_name'][0];
        $info['address'] = $data['customer_info']['address'][0];

        $info['rod_amount'] = $this->get_product_report($customer_id, "ROD");
        $info['tin_amount'] = $this->get_product_report($customer_id, "TIN");
        $info['cement_amount'] = $this->get_product_report($customer_id, "CEMENT");
        $info['total'] = $info['rod_amount'] + $info['tin_amount'] + $info['cement_amount'];
        return $info;
    }

    private function get_product_report($customer_id, $product_type) {
        $query = "SELECT  COALESCE(sum(t1.total_amount),0) as amount,t3.product_type FROM `sold_product_tbl` as t1,customer as t2,product_name_list as t3 where
                t1.customer_id=t2.serial and t1.customer_id=$customer_id and t1.product_name=t3.product_name and  t3.product_type='$product_type'";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                return number_format($row->amount, 2, ".", "");
            }
        } else {
            return 0;
        }
    }

    public function get_return_a_product($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['stock_product_serial'][$i] = $row->stock_product_serial;
                $info['product_name'][$i] = $row->product_name;
                $info['quantity'][$i] = $row->quantity;
                $info['sales_price'][$i] = number_format($row->sales_price, 2, ".", "");
                $info['branch_id'][$i] = $row->branch_id;
                $info['branch'][$i] = $row->branch;
                $info['invoice_no'][$i] = $row->invoice_no;
                $info['total_amount'][$i] = $row->total_amount;
                $info['product_name_id'][$i] = $row->product_name_id;
                $info['stock_quantity'][$i] = $row->stock_quantity;
                $info['stock_serial'][$i] = $row->stock_serial;
                $scale = strtolower($row->scale);
                if ($scale == "bag" || $scale == "kg") {
                    $info['scale'][$i] = $scale;
                } else {
                    $info['scale'][$i] = "pices";
                }
                $i++;
            }
        }
        return $info;
    }

    public function get_return_product_list($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['product_name'][$i] = $row->product_name;
                $info['product_type'][$i] = $row->product_type;
                $info['date'][$i] = $row->date;
                $info['quantity'][$i] = $row->quantity;
                $info['total_amount'][$i] = $row->total_amount;
                $scale = strtolower($row->scale);
                if ($scale == "bag" || $scale == "kg") {
                    $info['scale'][$i] = $scale;
                } else {
                    $info['scale'][$i] = "pices";
                }
                $i++;
            }
        }
        return $info;;
    }
    
    public function get_data($query, $var, $col) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                for ($j = 0; $j < count($var); $j++) {
                    $info[$var[$j]][$i] = $row->$col[$j];
                }
                $i++;
            }
        }
        return $info;
    }
    public function get_single_data($query,$col) {
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                return $row->$col;
            }
        } else {
            return 0;
        }
    }
    public function get_cash_in_hand($query) {
       // $query = "SELECT (sum(t1.cr)-sum(t1.dr)) as cash_in_hand FROM `general_ledger` as t1 where  t1.date<'$date'";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                if ($row->last_date_cash_in_hand != NULL) {
                    return $row->last_date_cash_in_hand;
                } else {
                    return 0;
                }
            }
        }
    }

    /* $query="SELECT t1.product_name_id,t2.product_name,t2.product_type,t1.date,t1.quantity,t1.total_amount FROM `returned_product_list` as t1,product_name_list as t2
      where t1.product_name_id=t2.serial"; */
}

?>
