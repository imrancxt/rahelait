$(document).ready(function(){
   
    var base_url="http://localhost/pos/";

    var mf={
        change_content:function(content,page){
            $.ajax({
                url:base_url+page,
                success: function(data){
                    if(data!=""){
                        $(content).html(data);
                    }
                }
            })
        },
        change_alert:function(page){
            $.ajax({
                url:base_url+page,
                success: function(data){
                    if(data!=""){
                        alert(data);
                    }
                }
            })
        }
    };
    
    $(document).on('submit','.alert_ajax_form',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                if(data!=""){
                    if(data=="refresh"){
                       nl1=$(location).attr('href');
                        nl2=nl1.replace("#","");
                        window.location=nl2;
                    }
                    else{
                        alert(data);
                    }
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    $(document).on('submit','.change_content_by_form',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        var content=$(this).attr('content');
       
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                if(data!=""){
                    if(data=='trashed'||data=="recovered"){
                        nl1=$(location).attr('href');
                        nl2=nl1.replace("#","");
                        window.location=nl2;
                    }
                    else{
                        $(content).html(data);
                        setTimeout(function(){
                            $(content).html("");
                        },3000);
                    }
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    $(document).on('submit','.append_by_form',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        var content=$(this).attr('content');
        //alert(content);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                if(data!=""){
                    $(content).append(data);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    
     $(document).on('submit','.change_content_by_refresh',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                
                        nl1=$(location).attr('href');
                        nl2=nl1.replace("#","");
                        window.location=nl2;
                    
                
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    $(document).on('submit','.change_content_by_alert',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        //var content=$(this).attr('content');
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                 alert(data);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    
});