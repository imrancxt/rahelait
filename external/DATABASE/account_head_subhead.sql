-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2016 at 05:46 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_head`
--

CREATE TABLE IF NOT EXISTS `account_head` (
`serial` int(11) NOT NULL,
  `acc_type` varchar(100) NOT NULL,
  `acc_head` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_head`
--

INSERT INTO `account_head` (`serial`, `acc_type`, `acc_head`) VALUES
(1, 'asset', 'Income A/C'),
(27, 'liability', 'Expense'),
(28, 'asset', 'me');

-- --------------------------------------------------------

--
-- Table structure for table `acc_subhead`
--

CREATE TABLE IF NOT EXISTS `acc_subhead` (
`serial` int(11) NOT NULL,
  `acc_head` varchar(50) NOT NULL,
  `acc_subhead` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_subhead`
--

INSERT INTO `acc_subhead` (`serial`, `acc_head`, `acc_subhead`) VALUES
(1, 'Income A/C', 'interest'),
(2, 'Income A/C', 'service charge'),
(5, 'Expense', 'Rent'),
(6, 'Expense', 'Salary'),
(7, 'me', 'edew');

-- --------------------------------------------------------

--
-- Table structure for table `others_account_ledger`
--

CREATE TABLE IF NOT EXISTS `others_account_ledger` (
`serial` int(11) NOT NULL,
  `admin` varchar(20) NOT NULL,
  `acc_head` varchar(50) NOT NULL,
  `acc_subhead` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `dr` double NOT NULL DEFAULT '0',
  `cr` double NOT NULL DEFAULT '0',
  `date` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `others_account_ledger`
--

INSERT INTO `others_account_ledger` (`serial`, `admin`, `acc_head`, `acc_subhead`, `description`, `dr`, `cr`, `date`) VALUES
(1, '1', 'Income A/C', 'interest', 'no', 900, 0, '2016-05-13'),
(2, '1', 'Income A/C', 'interest', 'no', 0, 1000, '2016-05-13'),
(3, '1', 'Expense', 'Rent', 'no', 0, 1000, '2016-05-13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_head`
--
ALTER TABLE `account_head`
 ADD PRIMARY KEY (`serial`), ADD UNIQUE KEY `acc_head` (`acc_head`);

--
-- Indexes for table `acc_subhead`
--
ALTER TABLE `acc_subhead`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `others_account_ledger`
--
ALTER TABLE `others_account_ledger`
 ADD PRIMARY KEY (`serial`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_head`
--
ALTER TABLE `account_head`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `acc_subhead`
--
ALTER TABLE `acc_subhead`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `others_account_ledger`
--
ALTER TABLE `others_account_ledger`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
